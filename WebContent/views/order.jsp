<%@page import="model.bean.HoaDonBean"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

</head>
<body>
	<%@ include file="./common/header.jsp"%>

	<main>
		<!-- Latest Products Start -->
		<section class="popular-items pt-20 pb-100 confirmation_part ">
			<div class="container">
				<div class="row product-btn justify-content-between">
					<div class="properties__button col-3">
						<!--Nav Button  -->
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="new-product"
									data-toggle="tab" href="#nav-home" role="tab"
									aria-controls="nav-home" aria-selected="true"
									onclick="filterProduct(this.id)">Lịch sử mua hàng</a>
							</div>
						</nav>
						<!--End Nav Button  -->
					</div>
					<div class="col-6 form-group p_star form-group-search">
						<input oninput="searchByName(this)" type="text"
							class="form-control" id="name" name="name-product" value=""
							placeholder="Nhập vào tên sản phẩm">
						<div class="nav-search search-switch wrap-search-icon">
							<span> <i class="fa fa-search"></i>
							</span>
						</div>
					</div>
					<!-- Select items -->
					<div class="select_filter col-3">
						<form action="#">
							<div class="select-itms">
								<select name="select" id="view-product"
									onchange="filterViewProduct(this, ${sessionScope['auth'].getMaKH()})">
									<option value="6">6 đơn hàng</option>
									<option value="12">12 đơn hàng</option>
									<option value="18">18 đơn hàng</option>
									<option value="24">24 đơn hàng</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<table class="table table table-bordered">
							<thead>
								<tr>
									<th scope="col">Đơn hàng</th>
									<th scope="col">Thời gian</th>
									<th scope="col">Số lượng</th>
									<th scope="col">Trạng thái</th>
									<th scope="col">Tác vụ</th>
									<th scope="col">Xem chi tiết</th>
								</tr>
							</thead>
							<tbody id="order_list">

								<%
								ArrayList<HoaDonBean> orderHistory = (ArrayList<HoaDonBean>) request.getAttribute("orderHistory");
								for (HoaDonBean od : orderHistory) {
								%>
								<tr>
									<td><%=od.getMaHoaDon()%></td>
									<td><%=od.getThoiGianMua()%></td>
									<td><%=od.getSoLuongMua()%></td>
									<%
									if (od.getTrangThai() == 0) {
									%>
									<td>Đang chờ xác nhận từ người bán</td>
									<%
									} else if (od.getTrangThai() == 1) {
									%>
									<td>Đang giao hàng</td>
									<%
									} else if (od.getTrangThai() == 2) {
									%>
									<td>Đã thanh toán</td>
									<%
									}
									if (od.getTrangThai() == 0) {
									%>
									<td>
										<div class="col d-flex align-items-center">
											<a class="btn btn-danger min-width"
												href="delete-order-cus?mhd=<%=od.getMaHoaDon()%>">Huỷ
												đặt hàng</a>
										</div>
									</td>

									<%
									} else if (od.getTrangThai() == 1) {
									%>
									<td>
										<div class="col d-flex align-items-center">
											<a class="btn btn-success min-width ml-2"
												href="update-order-cus?mhd=<%=od.getMaHoaDon()%>&tt=<%=od.getTrangThai()%>">Đã
												nhận hàng</a>
										</div>
									</td>
									<%
									} else if (od.getTrangThai() == 2) {
									%>
									<td>
										<div class="col d-flex align-items-center">
											<a class="btn btn-success min-width ml-2"
												href="delete-order-cus?mhd=<%=od.getMaHoaDon()%>">Đánh
												giá</a>
										</div>
									</td>
									<%
									}
									%>
									<td><a href="order-detail?mhd=<%=od.getMaHoaDon()%>"
										class="btn btn-primary">Xem chi tiết</a></td>
								</tr>
								<%
								}
								%>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</section>
		<!-- Latest Products End -->

		<%@ include file="./common/services.jsp"%>

	</main>
	<!-- Footer HTML -->
	<%@ include file="./common/footer.jsp"%>

	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script type="text/javascript">

		function filterViewProduct(selectElem, customerId) {
			let quanityOrderShow = selectElem.options[selectElem.selectedIndex].value
			
			console.log(quanityOrderShow, customerId)
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					quanityOrderShow,
					customerId
				},
				success : function(data) {
					console.log(data)
					var row = document.getElementById("order_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function searchByName(param) {
			let nameProductOrder = param.value;
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/search",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					nameProductOrder,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;

				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});

		}
	</script>
</body>
</html>