<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">

<link rel="stylesheet" href="css/animate.css" />

<link rel="stylesheet" href="css/owl.carousel.min.css" />

<link rel="stylesheet" href="css/ionicons.min.css" />

<link rel="stylesheet" href="css/style.css" />

<link rel="stylesheet" href="css/new_style.css" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>


	<c:choose>
		<c:when test="${not empty listgiohang}">
			<div aria-live="polite" aria-atomic="true"">
				<!-- Position it -->
				<div style="position: fixed; top: 10%; right: 0; z-index: 999">
					<!-- Then put toasts within -->
					<div class="toast-remove" role="alert" aria-live="assertive"
						aria-atomic="true" data-delay="1000">
						<div class="toast-header">
							<strong class="mr-auto">Thông báo</strong>
							<button type="button" class="ml-2 mb-1 close"
								data-dismiss="toast" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="toast-body">
							<div class="alert alert-success" role="alert">Xoá sản phẩm
								khỏi giỏ hàng thành công</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ftco-animate">
					<div class="cart-list">
						<table class="table">
							<thead class="thead-primary">
								<tr class="text-center">
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th>Sản phẩm</th>
									<th>Giá bán</th>
									<th>Số lượng</th>
									<th>Tổng tiền</th>
								</tr>
							</thead>
							<tbody id="wrap_product">
								<c:forEach items="${listgiohang}" var="kb">
									<tr class="text-center" id="content_product">
										<td class="product-remove"><a
											onclick="removeProduct('${kb.getMaBanPhim()}')"><i
												class="fa fa-times"></i></a></td>

										<td class="image-prod"><div class="img"
												style="background-image: url(./assets/img/gallery/${kb.getAnh()});"></div></td>

										<td class="product-name">
											<h3>${kb.getTenBanPhim()}</h3>
										</td>

										<td class="price"><fmt:formatNumber
												pattern="###,###,###₫" value="${kb.getGiaBan()}"
												type="currency" /></td>

										<td class="quantity">
											<div class="input-group mb-3">
												<input type="number" id="${kb.getMaBanPhim()}"
													oninput="updateQuantity(this.value, this.id)"
													class="quantity form-control input-number"
													value="${kb.getSoLuong()}" min="1">
											</div>
										</td>
										<td class="total"><fmt:formatNumber
												pattern="###,###,###₫" value="${kb.getThanhTien()}"
												type="currency" /></td>
									</tr>
								</c:forEach>
								<!-- END TR-->
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row justify-content-start">
				<div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
					<div id="content_total">
						<div class="cart-total mb-3 ">
							<h3>Cart Totals</h3>
							<p class="d-flex ">
								<span>Tạm tính</span> <span><fmt:formatNumber
										pattern="###,###,###₫" value="${giohang.getTongTien()}"
										type="currency" /> </span>
							</p>
							<p class="d-flex load">
								<span>Phí vận chuyển</span> <span><fmt:formatNumber
										pattern="###,###,###₫" value="0" type="currency" /> </span>
							</p>
							<p class="d-flex ">
								<span>Giảm giá</span> <span><fmt:formatNumber
										pattern="###,###,###₫" value="0" type="currency" /> </span>
							</p>
							<hr>
							<p class="d-flex total-price ">
								<span>Thành tiền</span><span><fmt:formatNumber
										pattern="###,###,###₫" value="${giohang.getTongTien()}"
										type="currency" /> </span>
							</p>
						</div>
					</div>
					<p class="text-center ">
						<a href="checkout" class="btn btn-primary py-3 px-4">Tiến hành
							đạt hàng</a>
					</p>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="slider-area ">
				<div class="single-slider slider-height2 d-flex align-items-center">
					<div class="container">
						<div class="row">
							<div class="col-xl-12">
								<div class="hero-cap text-center">
									<h2>Giỏ hàng trống</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>

	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>

	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>

	<script src="js/aos.js"></script>

	<script src="js/scrollax.min.js"></script>
	<script src="js/main.js"></script>

	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>


	<script type="text/javascript">
		function updateQuantity(quantity, maBanPhim) {
			$
					.ajax({
						url : "/JavaProject/cart",
						type : "get", //send it through get method
						timeout : 100000,
						data : {
							upKeyboardId : maBanPhim,
							upQuantity : quantity
						},
						success : function(data) {
							$("#wrap_product").load(
									location.href + " #content_product");
							$("#content_total").load(
									location.href + " #content_total");
						},
						error : function(xhr) {
							//Do Something to handle error
						}
					});

		}

		function removeProduct(maBanPhim) {
			$.ajax({
						url : "/JavaProject/cart",
						type : "get", //send it through get method
						timeout : 100000,
						data : {
							delKeyboardId : maBanPhim
						},
						success : function(data) {
							$(document).ready(function() {
								$('.toast-remove').toast('show');
							})
							$("#wrap_product").load(
									location.href + " #content_product");
							$("#content_total").load(
									location.href + " #content_total");
						},
						error : function(xhr) {
							//Do Something to handle error
						}
					});
		}
	</script>

</body>
</html>