<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="model.bean.KhachHangBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="lib/css/productdetail.css" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
	integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
	crossorigin="anonymous" />


</head>
<body>

	<%@ include file="./common/header.jsp"%>


	<div class="container">
		<div class="product__detail">

			<div class="row product__detail-row">
				<div class="col-lg-6 col-12 daonguoc">
					<div class="img-product">
						<ul class="all-img">
							<li class="img-item"><img
								src="https://images.unsplash.com/photo-1639049915980-f5a5ed0469d6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw3fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60"
								class="small-img" alt="anh 1" onclick="changeImg('one')"
								id="one"></li>
							<li class="img-item"><img
								src="https://images.unsplash.com/photo-1639080921697-73cbd55eb294?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw5fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60"
								class="small-img" alt="anh 2" onclick="changeImg('two')"
								id="two"></li>
							<li class="img-item"><img
								src="https://images.unsplash.com/photo-1639076527644-d48b605e30e5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxM3x8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60"
								class="small-img" alt="anh 3" onclick="changeImg('three')"
								id="three"></li>
							<li class="img-item"><img
								src="https://images.unsplash.com/photo-1639008941242-472faadf14ea?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyNXx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60"
								class="small-img" alt="anh 4" onclick="changeImg('four')"
								id="four"></li>

						</ul>
					</div>
					<div id="main-img" style="cursor: pointer;">
						<img src="${product.getAnh()}" class="big-img" alt="ảnh chính"
							id="img-main" style="visibility: visible;">
					</div>
				</div>

				<div class="col-lg-6 col-12">
					<div class="product__wrapper">
						<div class="product__name">
							<h2>
								<c:out value="${product.getTensach()}" />
							</h2>
						</div>
						<div class="status-product">
							Trạng thái: <b>Còn hàng</b>
						</div>
						<div class="infor-oder">
							Loại sản phẩm: <b> <c:out
									value="${product_loai.getTenloai()}"></c:out>
							</b>
						</div>
						<div class="product__price">
							<h5>Giá bán: ${product.getGia()}đ</h5>
						</div>
						<div class="d-flex align-items-center">
							<div
								class="d-flex align-items-center justify-content-between p-1 me-2">
								<label for="" class="mr-3 ml-1">Số lượng</label>
								<div class="number-input">
									<button
										onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
										class="minus"></button>
									<input class="quantity" min="1" name="quantity" value="1"
										type="number">
									<button
										onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
										class="plus"></button>
								</div>
							</div>
						</div>
						<div class="product__shopnow">
							<button class="add-cart"
								onclick="addToCart('${product.getMasach()}')">Thêm vào
								giỏ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="product__relateto">
		<div class="container">
			<h3 class="product__relateto-heading">Sản phẩm liên quan</h3>
			<div class="row" id="content">
				<c:forEach items="${product_related}" var="s">
					<div class="col-lg-3 col-md-6 col-sm-12 mb-20 productt">
						<div class="product__new-item">
							<div class="card" style="width: 100%">
								<div>
									<img class="card-img-top" src="${s.getAnh()}"
										alt="Card image cap">
								</div>
								<a>
									<div class="card-body">
										<h5 class="card-title custom__name-product">${s.getTensach()}</h5>
										<div class="product__price">
											<p class="card-text price-color product__price-new">${s.getGia()}
												đ</p>
										</div>
										<div class="home-product-item__action">
											<span class="home-product-item__sold">79 đã bán</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="seemore">
				<button onclick="loadMore()">Tải thêm</button>
			</div>
		</div>
	</div>

	<div class="container-fuild">
		<!-- Footer HTML -->
		<%@ include file="./common/footer.jsp"%>
	</div>



	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script type="text/javascript">
		function addToCart(maSach) {
			var quantity = document.querySelector(".quantity").value;
			$.ajax({
				url : "/JavaCourse/cart",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					ms : maSach,
					sl : quantity
				},
				success : function(data) {
					$(document).ready(
							function() {
								$('.toast').toast('show');
								$("#navbarSupportedContent").load(
										location.href
												+ " #navbarSupportedContent");
							});

				},
				error : function(xhr) {
					console.log(xhr)
				}
			});
		}

		function loadMore() {
			var amount = document.getElementsByClassName("productt").length;
			console.log(amount);
			$.ajax({
				url : "/JavaCourse/load",
				type : "get", //send it through get method
				data : {
					productDetail : amount
				},
				success : function(data) {
					var row = document.getElementById("content");
					row.innerHTML += data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}
	</script>
</body>
</html>
