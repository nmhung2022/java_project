<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.BanPhimBean"%>
<%@page import="model.bean.AdminBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Admin | Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">


</head>
<body>
	<style>
.form-v7-content {
	margin: 30px 0;
	font-family: 'Open Sans', sans-serif;
	position: relative;
	display: flex;
	justify-content: center;
	display: -webkit-flex;
}

.form-v7-content img {
	object-fit: cover;
	width: 100%;
	height: 100%;
}

.form-v7-content .form-left {
	height: 500px;
	width: 55%;
	padding: 30px;
	width: 55%;
}

.form-v7-content .form-detail {
	padding: 73px 80px 41px;
	position: relative;
	width: 100%;
	background: #fff;
	box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%);
	-o-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-ms-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
	-webkit-box-shadow: 0px 8px 20px 0px rgb(0 0 0/ 15%)
}

.form-v7-content .form-detail .form-row {
	width: 100%;
	position: relative;
	align-items: center;
	margin-bottom: 20px;
}

.form-v7-content .form-detail .form-row label {
	color: #666;
	font-weight: 600;
	font-size: 13px;
}

.form-v7-content .form-detail .form-row label {
	margin-bottom: 0px;
}

.popular-items .nice-select span {
	font-weight: 400;
}

.form-v7-content label {
	margin-right: 15px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5 px 15 px 10 px 15 px;
	border: 2 px solid transparent;
	border-bottom: 2 px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

.form-row-last {
	margin-top: 35px;
}

.form-v7-content .form-detail .form-row-last input {
	padding: 15px;
}

.form-v7-content .form-detail .register {
	background: #373be3;
	border-radius: 4px;
	-o-border-radius: 4px;
	-ms-border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	width: 180px;
	border: none;
	cursor: pointer;
	color: #fff;
	font-weight: 700;
	font-size: 15px;
}

.form-v7-content .form-detail input {
	width: 100%;
	padding: 5px 15px 10px 15px;
	border: 2px solid transparent;
	border-bottom: 2px solid #e5e5e5;
	appearance: unset;
	-moz-appearance: unset;
	-webkit-appearance: unset;
	-o-appearance: unset;
	-ms-appearance: unset;
	outline: none;
	-moz-outline: none;
	-webkit-outline: none;
	-o-outline: none;
	-ms-outline: none;
	font-family: 'Open Sans', sans-serif;
	font-size: 16px;
	font-weight: 700;
	color: #333;
	box-sizing: border-box;
	-o-box-sizing: border-box;
	-ms-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
</style>


	<c:if test="${not empty isSignin}">
		<script>
			$(document).ready(function() {
				$("#loginModal").modal('show');
			});
		</script>
	</c:if>


	<%@ include file="./common/header.jsp"%>

	<main>

		<!-- Latest Products Start -->
		<section class="popular-items pt-20 pb-30">
			<div class="container">
				<h3 class="text-heading mb-0">Cập nhất bàn phím</h3>
				<div class="row">
					<div class="col">
						<div class="form-v7-content">
							<div class="form-left">
								<img alt="" src="assets/img/gallery/${keyboardEdit.getAnh()}">
							</div>

							<form class="form-detail" action="edit-product" method="post"
								enctype="multipart/form-data">

								<div class="form-row">
									<input type="text" name="maBanPhim" class="input-text" hidden
										value="${keyboardEdit != null ? keyboardEdit.getMaBanPhim() : ''}">
								</div>

								<div class="form-row">
									<label>Tên bàn phím</label> <input type="text"
										name="tenBanPhim" class="input-text" data-focused="true"
										value="${keyboardEdit != null ? keyboardEdit.getTenBanPhim() : ''}">
								</div>

								<div class="form-row">
									<label>Loại bàn phím</label> <select
										class="default-select custom-selectt" name="maLoai">
										<c:forEach items="${dsLoai}" var="l">
											<option value="${l.getMaLoai()}"
												${l.getMaLoai().equals(sachEdit.getMaLoai()) ? 'selected' : ''}>${l.getTenLoai()}</option>
										</c:forEach>
									</select>
								</div>
								<div class="form-row">
									<label>Loại switch</label> <select
										class="default-select custom-selectt" name="maSwitch">
										<c:forEach items="${dsSwitch}" var="l">
											<option value="${l.getMaSwitch()}"
												${l.getMaSwitch().equals(sachEdit.getMaSwitch()) ? 'selected' : ''}>${l.getTenSwitch()}</option>
										</c:forEach>
									</select>
								</div>

								<div class="form-row">
									<label>Giá bán</label> <input class="input-text"
										aria-required="true" name="giaBan"
										value="${keyboardEdit != null ? keyboardEdit.getGiaBan() : ''}">
								</div>

								<div class="form-row">
									<label>Số lượng tồn</label> <input class="input-text"
										aria-required="true" name="soLuong"
										value="${keyboardEdit != null ? keyboardEdit.getSoLuong() : ''}">
								</div>

								<div class="form-row">
									<label>Ngày nhập</label> <input class="input-text"
										aria-required="true" name="ngayNhap" type="date"
										value="${keyboardEditNgayNhap != null ? keyboardEditNgayNhap : ''}">
								</div>

								<div class="form-row">
									<label>Ảnh</label>
									<div class="custom-file">
										<input class="custom-file-input" name="anh" type="file">
										<label class="custom-file-label" for="customFile">
											${keyboardEdit != null ? keyboardEdit.getAnh() : "Chọn ảnh"}</label>
									</div>
								</div>
								<div class="form-row mt-3">
									<label>Mô tả</label>
									<textarea class="form-control" name="moTa" id="message"
										rows="12" cols="50" placeholder="Order Notes">${keyboardEdit.getMoTa()}</textarea>
								</div>

								<div class="form-row-last d-flex justify-content-center">
									<input type="submit" name="update" class="register"
										value="Cập nhật">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>



	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>



	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script>
		$('input[type="file"]').on("change", function() {
			let filenames = [];
			let files = this.files;
			if (files.length > 1) {
				filenames.push("Total Files (" + files.length + ")");
			} else {
				for ( let i in files) {
					if (files.hasOwnProperty(i)) {
						filenames.push(files[i].name);
					}
				}
			}
			$(this).next(".custom-file-label").html(filenames.join(","));
		});
	</script>

</body>
</html>