<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.BanPhimBean"%>
<%@page import="model.bean.AdminBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Admin | Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

</head>
<body>
	<%@ include file="./common/header.jsp"%>
	<main>
		<section class="pt-70 pb-70">
			<div class="container">
				<div class="row">
					<div class="col">
						<c:choose>
							<c:when test="${loai != null}">
								<h3>Cập nhật loại</h3>
							</c:when>
							<c:otherwise>
								<h3>Thêm mới loại</h3>
							</c:otherwise>
						</c:choose>


						<form class="row contact_form d-flex justify-content-center"
							action="${loai != null ? 'edit-loai' : 'add-loai'}" method="post" novalidate="novalidate">
							<div class="col-md-5 form-group p_star">

								<c:if test="${existsMaLoai != null }">
									<div class="alert alert-warning" role="alert">
										Mã loại sách <strong><c:out
												value="${existsMaLoai.getMaLoai()}"></c:out></strong> đã tồn tại, vui
										lòng thử lại với tên khác
									</div>
								</c:if>

								<input type="text" name="ml-old" hidden
									value="${loai != null ? loai.getMaLoai() : ''}">

								<c:if test="${existsMaLoaiNew != null }">
									<div class="alert alert-warning" role="alert">
										Mã loại sách <strong><c:out
												value="${existsMaLoai.getMaLoai()}"></c:out></strong> đã tồn tại, vui
										lòng thử lại với tên khác
									</div>
								</c:if>



								<div class="form-row mt-3">
									<label>Mã loại</label> <input type="text" class="form-control"
										id="name" name="ml-new"
										value="${loai != null ? loai.getMaLoai() : ''}"
										data-focused="true">
								</div>

								<div class="form-row mt-3">
									<label>Tên loại</label> <input type="text" class="form-control"
										id="name" name="tenLoai"
										value="${loai != null ? loai.getTenLoai() : ''}"
										data-focused="true">
								</div>
								<c:choose>
									<c:when test="${loai != null}">
										<div class="form-group mt-5 d-flex justify-content-center">
											<button type="submit" value="submit" class="btn_3">Cập
												nhật</button>
										</div>

									</c:when>
									<c:otherwise>
										<div class="form-group mt-5 d-flex justify-content-center">
											<button type="submit" value="submit" class="btn_3">Thêm
												mới</button>
										</div>
									</c:otherwise>
								</c:choose>

							</div>


						</form>
					</div>
				</div>
			</div>
		</section>
	</main>

	<%@ include file="../common/footer.jsp"%>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>


</body>
</html>