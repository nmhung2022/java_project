<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="model.bean.AdminBean"%>
<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.BanPhimBean"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">
</head>
<body>
	<style>
td {
	text-align: center;
}
</style>


	<%@ include file="./common/header.jsp"%>

	<main>

		<!-- Latest Products Start -->
		<section class="popular-items pb-100">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="card card-1">
							<div class="card-header bg-white">
								<div
									class="media flex-sm-row flex-column-reverse justify-content-between">
									<div class="col my-auto">
										<h4 class="heading">
											<strong class="nav-item nav-link active">Danh sách
												đơn hàng</strong>
										</h4>
									</div>
								</div>
								<c:if test="${deleteSuccess != null }">
									<div class="col">
										<div
											class="alert alert-success alert-dismissible fade show mt-2"
											role="alert">
											Hoá đơn có mã <strong><c:out
													value="${deleteSuccess}"></c:out></strong> đã được xoá
											<p class="mb-0">
												Bấm vào <strong>đây</strong> để khôi phục.
											</p>
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
								</c:if>
							</div>
							<div class="card-body">
								<div class="row justify-content-between mb-3">
									<div class="col-auto"></div>
								</div>
								<div class="row product-btn justify-content-between">
									<div class="properties__button col-3">
										<nav>
											<div class="nav nav-tabs" id="nav-tab" role="tablist">
												<a class="nav-item nav-link active" id="new-product"
													data-toggle="tab" href="#nav-home" role="tab"
													aria-controls="nav-home" aria-selected="true"
													onclick="filterProduct(this.id)">Mới nhất</a>
											</div>
										</nav>
										<!--End Nav Button  -->
									</div>
									<div class="col-6 form-group p_star form-group-search">
										<input oninput="searchByOrderId(this)" type="text"
											class="form-control" id="name" name="name-product" value=""
											placeholder="Nhập vào mã đơn hàng">
										<div class="nav-search search-switch wrap-search-icon">
											<span> <i class="fa fa-search"></i>
											</span>
										</div>
									</div>
									<!-- Select items -->
									<div class="select_filter col-3">
										<form action="#">
											<div class="select-itms">
												<select name="select" id="view-product"
													onchange="filterViewAllProduct(this)"
													style="display: none;">
													<option value="6">6 đơn hàng</option>
													<option value="12">12 đơn hàng</option>
													<option value="18">18 đơn hàng</option>
													<option value="24">24 đơn hàng</option>
												</select>
												<div class="nice-select" tabindex="0">
													<span class="current">6 đơn hàng</span>
													<ul class="list">
														<li data-value="6" class="option selected">6 đơn hàng</li>
														<li data-value="12" class="option">12 đơn hàng</li>
														<li data-value="18" class="option">18 đơn hàng</li>
														<li data-value="24" class="option">24 đơn hàng</li>
													</ul>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row card-wrap">
									<div class="col">
										<table class="table table table-bordered">
											<thead>
												<tr>
													<th scope="col">Đơn hàng</th>
													<th scope="col">Thời gian mua</th>
													<th scope="col">Số lượng mua</th>
													<th scope="col">Trạng thái</th>
													<th scope="col">Tác vụ</th>
													<th scope="col">Chi tiết</th>
												</tr>
											</thead>

											<tbody id="order_all_list">
												<c:forEach items="${orderHistory}" var="od">
													<tr >
														<td>${od.getMaHoaDon()}</td>
														<td>${od.getThoiGianMua()}</td>
														<td>${od.getSoLuongMua()}</td>
														<c:choose>
															<c:when test="${od.getTrangThai() eq 0}">
																<td>Chưa thanh toán</td>
															</c:when>
															<c:when test="${od.getTrangThai() eq 1}">
																<td>Đang giao hàng</td>
															</c:when>
															<c:otherwise>
																<td>Đã thanh toán</td>
															</c:otherwise>
														</c:choose>

														<c:choose>

															<c:when test="${od.getTrangThai() eq 0}">
																<td>
																	<div class="col ">
																		<a class="genric-btn danger circle"
																			href="delete-order?mhd=${od.getMaHoaDon()}">Huỷ
																			đơn</a> <a class="genric-btn success circle ml-3"
																			href="update-order?mhd=${od.getMaHoaDon()}&tt=${od.getTrangThai()}">Chấp
																			nhận đơn</a>
																	</div>
																</td>
															</c:when>

															<c:when test="${od.getTrangThai() eq 1}">
																<td>
																	<div class="col">
																		<a class="genric-btn danger circle"
																			href="delete-order?mhd=${od.getMaHoaDon()}">Thay
																			đổi thông tin</a> <a class="genric-btn danger circle"
																			href="delete-order?mhd=${od.getMaHoaDon()}">Đã
																			nhận tiền</a>
																	</div>
																</td>
															</c:when>
															<c:otherwise>
																<td><a class="genric-btn danger circle"
																	href="delete-order?mhd=${od.getMaHoaDon()}">Xoá</a></td>
															</c:otherwise>
														</c:choose>
														<td><a class="genric-btn info circle"
															href="order-history?mhd=${od.getMaHoaDon()}&mkh=${od.getMaKhachHang()}">Xem
																chi tiết</a></td>

													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script type="text/javascript">

		function filterViewAllProduct(selectElem) {
			let quanityOrderAllShow = selectElem.options[selectElem.selectedIndex].value
			console.log(quanityOrderAllShow)
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					quanityOrderAllShow,
				},
				success : function(data) {
					console.log(data)
					var row = document.getElementById("order_all_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function searchByOrderId(param) {
			let orderId = param.value;
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/search",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					orderId,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;

				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});

		}
	</script>

</body>
</html>
