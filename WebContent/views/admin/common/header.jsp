<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="css/new_style.css">
</head>
<body>


	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="dashboard"><img src="assets/img/logo/logo.jpg"
								alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="product">QUẢN LÝ SẢN PHẨM</a></li>
									<li><a href="product">QUẢN LÝ MẶT HÀNG</a>
										<ul class="submenu">
											<li><a href="manage-loai">QUẢN LÝ LOẠI</a></li>
											<li><a href="blog-details.html">QUẢN LÝ SWITCH</a></li>
										</ul></li>
									<li><a href="manage-order">QUẢN LÝ HOÁ ĐƠN</a></li>
									<li><a href="blog.html">QUẢN LÝ BÀI VIẾT</a></li>
									<li><a href="#">QUẢN LÝ HỖ TRỢ</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><span> <i class="fa fa-user"> </i>
								</span>
									<ul class="submenu">
										<c:choose>
											<c:when test="${ not empty sessionScope['auth-admin']}">
												<li class="icon"><a href="signup-admin">Đăng ký</a></li>
												<li class="icon"><a href="signout">Đăng xuất</a></li>
											</c:when>
											<c:otherwise>
												<li class="icon"><a href="signin">Đăng nhập</a></li>
											</c:otherwise>
										</c:choose>
									</ul></li>
								<li>
									<div class="nav-search search-switch ">
										<span> <i class="fa fa-search"></i>
										</span>
									</div>
								</li>
								<li>
									<div class="nav-search search-switch ">
										<span> </span>
									</div>
								</li>
							</ul>
						</div>



					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
</body>
</html>