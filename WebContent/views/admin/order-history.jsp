<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page import="model.bo.HoaDonChiTietBo"%>
<%@page import="model.bean.HoaDonBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Admin | Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

</head>
<body class="skin-light-2">

	<%@ include file="./common/header.jsp"%>
	<main>
		<!-- Latest Products Start -->
		<section class="popular-items padding-all">
			<div class="container-fuild">
				<div class="row">
					<div class="col">
						<!-- Card -->
						<div class="card mb-4">
							<h5 class="p-2 shadow-2">Khách hàng</h5>
							<div class="card-body">
								<ul class="list-group list-group-flush">
									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
										Tên khách hàng <span>${customer.getHoTen()}</span>
									</li>

									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
										Số điện thoại <span><strong>${customer.getSoDienThoai()}
										</strong></span>
									</li>

									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
										Email <span>${customer.getEmail()}</span>
									</li>

									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
										<div>
											<strong>Địa chỉ</strong>
										</div> <span><strong>${customer.getDiaChi()} </strong></span>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!--Grid column-->
					<div class="col-lg-6">
						<!-- Card -->
						<div class="card wish-list mb-4">
							<div class="d-flex justify-content-between align-items-center">
								<h5 class="p-4 m-0">
									<span><strong>Hoá đơn:
											${listOrderDetailHistory.get(0).getMaHoaDon()}</strong></span>
								</h5>
								<h5 class="p-4 m-0">
									<span><strong>Số sản phẩm:
											${listOrderDetailHistory.size()}</strong></span>
								</h5>
							</div>


							<div class="card-body scroll">
								<c:forEach items="${listOrderDetailHistory}" var="odt">
									<div class="row mb-4">
										<div class="col-md-5 col-lg-3 col-xl-3">
											<div class=" mb-3 mb-md-0">
												<img alt="" src="assets/img/gallery/${odt.getAnh()}"
													class="rounded mx-auto d-block w-75 h-100">

											</div>
										</div>
										<div class="col-md-7 col-lg-9 col-xl-9">
											<div>
												<div class="d-flex justify-content-between">
													<div class="col">
														<h5>${odt.getTenBanPhim()}</h5>
													</div>
												</div>
												<div
													class="d-flex justify-content-between align-items-center">
													<p class="p-4 m-0">
														<span><strong>Giá bán: <fmt:formatNumber
																	pattern="###,###,###₫" value="${odt.getThanhTien()}"
																	type="currency" />
														</strong></span>
													</p>
													<p class="p-4 m-0">
														<span><strong>Số lượng:
																${odt.getSoLuong()}</strong></span>
													</p>

												</div>
											</div>
										</div>
									</div>
									<hr class="mb-4" />
								</c:forEach>
							</div>
						</div>

					</div>
					<!--Grid column-->

					<!--Grid column-->
					<div class="col-lg-3">
						<!-- Card -->
						<div class="card mb-4">
							<h5 class="p-2 shadow-2">Tổng tiền</h5>
							<div class="card-body">
								<ul class="list-group list-group-flush">
									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
										Số lượng <span>${listOrderDetailHistory.size()}</span>
									</li>
									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
										<div>
											<strong>Thành tiền</strong>
										</div> <span><strong> <fmt:formatNumber
													pattern="###,###,###₫"
													value="${orderDetailHistory.tongTien()}" type="currency" />
										</strong></span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>



	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>
</body>
</html>
