<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Admin | Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

</head>
<body>

	<%@ include file="./common/header.jsp"%>

	<main>

		<!-- Latest Products Start -->
		<section class="popular-items pt-70 pb-70">
			<div class="container">


				<c:if test="${deleteSuccess != null }">
					<div class="col">
						<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Sách có mã <strong><c:out value="${deleteSuccess}"></c:out></strong>
							đã được xoá
							<p class="mb-0">
								Bấm vào <strong>đây</strong> để khôi phục
							</p>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</c:if>

				<c:if test="${sachNew != null }">
					<div class="col">
						<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							<div class="alert alert-success" role="alert">

								<h4 class="alert-heading">Thêm sách thành công!</h4>
								<p>
									Sách có tên<strong> <c:out value="${sachNew()}"></c:out></strong>
									vừa mới được thêm vàogetTenBanPhim
								</p>
							</div>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</c:if>

				<div class="row product-btn justify-content-between mb-40">
					<div class="properties__button">
						<!--Nav Button  -->
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="new-product"
									data-toggle="tab" href="#nav-home" role="tab"
									aria-controls="nav-home" aria-selected="true"
									onclick="filterProduct(this.id)">Mới nhất </a>
							</div>
						</nav>
						<!--End Nav Button  -->
					</div>
					<div class="properties__button ml-3 mr-3">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a href="add-or-edit" class="genric-btn success circle arrow">Tạo
								mới sản phẩm<span class="lnr lnr-arrow-right"></span>
							</a>
						</div>
						<!--End Nav Button  -->
					</div>
					<div class="col form-group p_star form-group-search">
						<input oninput="searchByProduct(this)" type="text"
							class="form-control" id="name" name="name-product" value=""
							placeholder="Nhập vào tên sản phẩm">

						<div class="nav-search search-switch wrap-search-icon">
							<span> <i class="fa fa-search"></i>
							</span>
						</div>
					</div>
					<!-- Select items -->
					<div class="select_filter">
						<form action="#">
							<div class="select-itms">
								<select name="select" id="view-product-admin"
									onchange="filterViewProductAdmin(this.id)">
									<option value="6">6 sản phẩm</option>
									<option value="12">12 sản phẩm</option>
									<option value="18">18 sản phẩm</option>
									<option value="24">24 sản phẩm</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<!-- Nav Card -->
				<div class="tab-content" id="nav-tabContent">
					<!-- card one -->
					<div class="tab-pane fade show active" id="nav-home"
						role="tabpanel" aria-labelledby="nav-home-tab">

						<div class="row" id="product_list">

							<c:forEach items="${dsProduct}" var="s">
								<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
									<div class="single-popular-items mb-50 text-center">
										<div class="popular-img">
											<img src="assets/img/gallery/${s.getAnh()}">
											<div class="img-cap two">
												<span><a
													href="add-or-edit?keyboardId=${s.getMaBanPhim()}">Chỉnh
														sửa</a></span><span> <a
													href="delete-sach?ms=${s.getMaBanPhim()}&ml=${s.getMaLoai()}&ts=${s.getTenBanPhim()}
												&tg=${s.getTenBanPhim()}&gia=${s.getGiaBan()}
												&anh=${s.getAnh()}">Xoá</a></span>
											</div>
											<div class="favorit-items">
												<span class="flaticon-heart"></span>
											</div>
										</div>
										<div class="popular-caption">
											<h3>
												<a href="product_details.html"> <c:out
														value="${s.getTenBanPhim()}"></c:out>
												</a>
											</h3>
											<span><fmt:formatNumber value="${s.getGiaBan()}"
													type="currency" /></span>
										</div>
									</div>
								</div>
							</c:forEach>

						</div>
					</div>
				</div>
				<!-- End Nav Card -->
			</div>
		</section>
		<!-- Latest Products End -->
	</main>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script type="text/javascript">
	
	function searchByProduct(param) {
		let txtSearch = param.value;
		let selectElem = document.querySelector("#view-product")
		let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
		
		console.log(txtSearch)
		
		$.ajax({
			url : "/JavaProject/search",
			type : "get", //send it through get method
			timeout : 100000,
			data : {
				isDashboard: true
				txtSearch : txtSearch,
				quantityProductShow : quantityProductShow
			},
			success : function(data) {
				var row = document.getElementById("product_list");
				row.innerHTML = data;

			},
			error : function(xhr) {
				//Do Something to handle error
			}
		});
	}

		function filterProduct(filterName) {
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					filter : filterName,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function filterViewProduct(selectElem) {
			console.log(document.getElementById("view-product").value)			
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}
		
	</script>

</body>
</html>