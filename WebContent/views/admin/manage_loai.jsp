<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="model.bean.AdminBean"%>
<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.BanPhimBean"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Admin | Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">
</head>
<body>
	<%@ include file="./common/header.jsp"%>
	<main>
		<section>

			<div class="container">

				<div class="col">

					<div class="row product-btn justify-content-between mb-40">
						<div class="properties__button">
							<!--Nav Button  -->
							<nav>
								<div class="nav nav-tabs" id="nav-tab" role="tablist">
									<a class="nav-item nav-link active" id="new-product"
										data-toggle="tab" href="#nav-home" role="tab"
										aria-controls="nav-home" aria-selected="true"
										onclick="filterProduct(this.id)">Mới nhất </a>
								</div>
							</nav>
							<!--End Nav Button  -->
						</div>
						<div class="properties__button ml-3 mr-3">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a href="add-or-edit-loai" class="genric-btn success circle arrow">Tạo
									mới loại<span class="lnr lnr-arrow-right"></span>
								</a>
							</div>
							<!--End Nav Button  -->
						</div>
						<div class="col form-group p_star form-group-search">
							<input type="text" class="form-control" id="product"
								oninput="myFunction(this.value)" name="product" value=""
								placeholder="Nhập vào tên sản phẩm">

							<div class="nav-search search-switch wrap-search-icon">
								<span> <i class="fa fa-search"></i>
								</span>
							</div>
						</div>


						<!-- Select items -->
						<div class="select_filter">
							<form action="#">
								<div class="select-itms">
									<select name="select" id="view-product"
										onchange="filterViewProduct(this)">
										<option value="6">6 sản phẩm</option>
										<option value="12">12 sản phẩm</option>
										<option value="18">18 sản phẩm</option>
										<option value="24">24 sản phẩm</option>
									</select>
								</div>
							</form>
						</div>
					</div>
					<!-- Nav Card -->
					<div class="tab-content" id="nav-tabContent">
						<!-- card one -->
						<div class="tab-pane fade show active" id="nav-home"
							role="tabpanel" aria-labelledby="nav-home-tab">

							<div class="row" id="product_list">

								<c:forEach items="${dsLoai}" var="s">
									<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
										<div class="single-popular-items mb-50 text-center ">
											<div
												class="popular-caption box-shadown justify-content-around">
												<h3>
													<c:out value="${s.getTenLoai()}" />
												</h3>
												<div class="button-group-area">
													<a href="add-or-edit-loai?maLoai=${s.getMaLoai()}"
														class="genric-btn primary medium">Chỉnh sửa</a> <a
														href="delete-load?maLoa=${s.getMaLoai()}"
														class="genric-btn danger medium">Xoá</a>
												</div>
											</div>
										</div>
									</div>

								</c:forEach>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script type="text/javascript">

	$( document ).ready(function() {
	
	$("input[name=product]").on("input", function() {
		   alert($(this).val()); 
		});
	
		function filterProduct(filterName) {
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					filter : filterName,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function filterViewProduct(selectElem) {
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function searchByProduct(param) {
			let txtSearch = param.value;
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			
			console.log(txtSearch)
			
			$.ajax({
				url : "/JavaProject/search",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					isDashboard: true
					txtSearch : txtSearch,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;

				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}
	});
	</script>
</body>
</html>
