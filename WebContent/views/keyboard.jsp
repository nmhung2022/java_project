<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

</head>
<body>

	<%@ include file="./common/header.jsp"%>

	<main>
		<!-- Latest Products Start -->
		<section class="popular-items pb-100">
			<div class="container">
				<div aria-live="polite" aria-atomic="true">
					<!-- Position it -->
					<div style="position: fixed; top: 50%; right: 0; z-index: 999">
						<!-- Then put toasts within -->
						<div class="toast" role="alert" aria-live="assertive"
							aria-atomic="true" data-delay="1000">
							<div class="toast-header">
								<strong class="mr-auto">Thông báo</strong>
								<button type="button" class="ml-2 mb-1 close"
									data-dismiss="toast" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="toast-body">

								<div class="alert alert-success" role="alert">Thêm sản
									phẩm vào giỏ hàng thành công</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row product-btn justify-content-between mb-40">

					<div class="properties__button">
						<!--Nav Button  -->
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="new-product"
									data-toggle="tab" href="#nav-home" role="tab"
									aria-controls="nav-home" aria-selected="true"
									onclick="filterProduct(this.id)">Mới nhất </a> <a
									class="nav-item nav-link" id="price-asc" data-toggle="tab"
									role="tab" aria-controls="nav-profile" aria-selected="false"
									onclick="filterProduct(this.id)">Giá từ thấp đến cao</a> <a
									class="nav-item nav-link" id="price-desc" data-toggle="tab"
									href="#nav-contact" role="tab" aria-controls="nav-contact"
									aria-selected="false" onclick="filterProduct(this.id)">Giá
									từ cao đến thấp</a>
							</div>
						</nav>
						<!--End Nav Button  -->
					</div>
					<div class="col form-group p_star form-group-search">
						<input oninput="searchByName(this)" type="text"
							class="form-control" id="name" name="name-product" value=""
							placeholder="Nhập vào tên sản phẩm">
						<div class="nav-search search-switch wrap-search-icon">
							<span> <i class="fa fa-search"></i>
							</span>
						</div>
					</div>
					<!-- Select items -->
					<div class="select_filter">
						<div class="select-itms">
							<select name="select" id="view-product"
								onchange="filterViewProduct(this)">
								<option value="6">6 sản phẩm</option>
								<option value="12">12 sản phẩm</option>
								<option value="18">18 sản phẩm</option>
								<option value="24">24 sản phẩm</option>
							</select>
						</div>

					</div>
				</div>
				<!-- Nav Card -->
				<div class="tab-content" id="nav-tabContent">
					<!-- card one -->
					<div class="tab-pane fade show active" id="nav-home"
						role="tabpanel" aria-labelledby="nav-home-tab">

						<div class="row" id="product_list">
							<c:forEach items="${listKeyBoards}" var="kb">
								<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
									<div class="single-popular-items mb-50 text-center">
										<div class="popular-img">
											<img src="assets/img/gallery/${kb.getAnh()}" alt="">
											<div class="img-cap">
												<span onclick="addToCart('${kb.getMaBanPhim()}', 1)">Thêm
													vào giỏ hàng</span>
											</div>
											<div class="favorit-items">
												<span class="flaticon-heart"></span>
											</div>
										</div>
										<div class="popular-caption">
											<h3>
												<a href="product-details?keyboardId=${kb.getMaBanPhim()}">
													<c:out value="${kb.getTenBanPhim()}"></c:out>
												</a>
											</h3>
											<span> <fmt:formatNumber value="${kb.getGiaBan()}"
													type="currency" />
											</span>
										</div>
									</div>
								</div>
							</c:forEach>


						</div>
					</div>
				</div>
				<!-- End Nav Card -->
			</div>
		</section>
		<!-- Latest Products End -->

		<%@ include file="./common/services.jsp"%>

	</main>
	<!-- Footer HTML -->
	<%@ include file="./common/footer.jsp"%>

	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>


	<script type="text/javascript">
		function addToCart(keyboardId, quantity) {
			
			$.ajax({
				url : "/JavaProject/cart",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					keyboardId,
					quantity,
				},
				success : function(data) {
					  $(document).ready(function(){
				            $('.toast').toast('show');
				        })
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function filterProduct(filterName) {
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					filterName : filterName,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function filterViewProduct(selectElem) {
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			let filterName =  document.querySelector('.nav-item.active').id	
			console.log(filterName, quantityProductShow)
			$.ajax({
				url : "/JavaProject/filter",
				type : "get", //send it through get method
				data : {
					filterName : filterName,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					var row = document.getElementById("product_list");
					row.innerHTML = data;
				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});
		}

		function searchByName(param) {
			let txtSearch = param.value;
			let selectElem = document.querySelector("#view-product")
			let quantityProductShow = selectElem.options[selectElem.selectedIndex].value
			$.ajax({
				url : "/JavaProject/search",
				type : "get", //send it through get method
				timeout : 100000,
				data : {
					txtSearch : txtSearch,
					quantityProductShow : quantityProductShow
				},
				success : function(data) {
					console.log(data)
					var row = document.getElementById("product_list");
					row.innerHTML = data;

				},
				error : function(xhr) {
					//Do Something to handle error
				}
			});

		}
	</script>
</body>
</html>