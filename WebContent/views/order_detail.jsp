<%@page import="model.bo.HoaDonChiTietBo"%>
<%@page import="model.bean.HoaDonBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">

<link rel="stylesheet" href="css/timeline.css">

</head>
<body>
	<%@ include file="./common/header.jsp"%>

	<!--Main layout-->
	<main>

		<!--Section: Block Content-->
		<section class="pb-100 pt-30">
			<div class="container-fuild padding-all">
				<!--Grid row-->
				<div class="row">
					<!--Grid column-->
					<div class="col-lg-3">
						<div>
							<div class="d-flex justify-content-between">
								<div class="col">
									<h5>Thông tin vận chuyển</h5>
								</div>
							</div>

							<div class="d-flex justify-content-between align-items-center">
								<p class="p-2 m-0">
									<span><strong>Đơn hàn đang chờ xác nhận từ
											người bán</strong></span>
								</p>
							</div>

							<div class="d-flex justify-content-between align-items-center">
								<p class="p-2 m-0">
									<span><strong>Người nhận: Nguyễn Văn A</strong></span>
								</p>
							</div>

							<div class="d-flex justify-content-between align-items-center">
								<p class="p-2 m-0">
									<span><strong>Số điện thoại: 098321321</strong></span>
								</p>
							</div>

							<div class="d-flex justify-content-between align-items-center">
								<p class="p-2 m-0">
									<span><strong>Địa chỉ nhận hàng: 327 Lê Đại
											Hành ....</strong></span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<!-- Card -->
						<div class="card wish-list mb-4">
							<div class="d-flex justify-content-between align-items-center">
								<h5 class="p-4 m-0">
									<span><strong>Hoá đơn:
											${listOrderDetailHistory.get(0).getMaHoaDon()}</strong></span>
								</h5>
								<h5 class="p-4 m-0">
									<span><strong>Số sản phẩm:
											${listOrderDetailHistory.size()}</strong></span>
								</h5>
							</div>

							<div class="card-body scroll">
								<c:forEach items="${listOrderDetailHistory}" var="odt">
									<div class="row mb-4">
										<div class="col-md-5 col-lg-3 col-xl-3">
											<div class="">
												<c:choose>
													<c:when test="${not empty odt.getAnh()}">
														<img alt="" src="assets/img/gallery/${odt.getAnh()}"
															class=" mx-auto d-block w-75 h-100">
													</c:when>
													<c:otherwise>
														<img alt="" src="https://picsum.photos/200/300"
															class="rounded mx-auto d-block w-75 h-100">
													</c:otherwise>
												</c:choose>

											</div>
										</div>
										<div class="col-md-7 col-lg-9 col-xl-9">
											<div>
												<div class="d-flex justify-content-between">
													<div class="col">
														<h5>${odt.getTenBanPhim()}</h5>
													</div>
												</div>
												<div
													class="d-flex justify-content-between align-items-center">
													<p class="p-4 m-0">
														<span><strong>Giá bán:
																${odt.getThanhTien()} đ</strong></span>
													</p>
													<p class="p-4 m-0">
														<span><strong>Số lượng:
																${odt.getSoLuong()}</strong></span>
													</p>

												</div>
											</div>
										</div>
									</div>
									<hr class="mb-4" />
								</c:forEach>
							</div>
						</div>

					</div>
					<!--Grid column-->

					<!--Grid column-->
					<div class="col-lg-3">
						<!-- Card -->
						<div class="card mb-4">
							<h5 class="p-2 shadow-2">Tổng tiền</h5>
							<div class="card-body">
								<ul class="list-group list-group-flush">
									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
										Số lượng <span>${listOrderDetailHistory.size()}</span>
									</li>
									<li
										class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
										<div>
											<strong>Thành tiền</strong>
										</div> <span><strong>${orderDetailHistory.tongTien()}
												đ</strong></span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title mb-5">Quá trình vận chuyển</h4>

								<div class="hori-timeline" dir="ltr">
									<ul class="list-inline events">
										<li class="list-inline-item event-list">
											<div class="px-4">
												<div class="event-date bg-soft-primary text-primary">2
													June</div>
												<h5 class="font-size-16">Đặt hàng</h5>
												<p class="text-muted">Lorem Ipsum is simply dummy text
													of the printing and typesetting industry.</p>
												<div>
													<a href="#" class="btn btn-primary btn-sm"></a>
												</div>
											</div>
										</li>
										<li class="list-inline-item event-list">
											<div class="px-4">
												<div class="event-date bg-soft-success text-success">5
													June</div>
												<h5 class="font-size-16">Đóng gói hàng</h5>
												<p class="text-muted">Lorem Ipsum is simply dummy text
													of the printing and typesetting industry.</p>
												<div>
													<a href="#" class="btn btn-primary btn-sm">Quy trình
														đóng gói</a>
												</div>
											</div>
										</li>
										<li class="list-inline-item event-list">
											<div class="px-4">
												<div class="event-date bg-soft-danger text-danger">7
													June</div>
												<h5 class="font-size-16">Vận chuyển</h5>
												<p class="text-muted">Lorem Ipsum is simply dummy text
													of the printing and typesetting industry.</p>
												<div>
													<a href="#" class="btn btn-primary btn-sm">Tra cứu vận
														đơn</a>
												</div>
											</div>
										</li>
										<li class="list-inline-item event-list">
											<div class="px-4">
												<div class="event-date bg-soft-warning text-warning">8
													June</div>
												<h5 class="font-size-16">Nhận hàng và đánh giá</h5>
												<p class="text-muted">Lorem Ipsum is simply dummy text
													of the printing and typesetting industry..</p>
												<div>
													<a href="#" class="btn btn-primary btn-sm">Đánh giá</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- end card -->
					</div>
				</div>
			</div>
		</section>

		<%@ include file="./common/services.jsp"%>
	</main>
	<!-- Footer HTML -->
	<%@ include file="./common/footer.jsp"%>

	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>


</body>
</html>

