<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page import="model.bo.GioHangBo"%>
<%@page import="model.bean.KhachHangBean"%>
<%@page import="model.bean.LoaiBean"%>
<%@page import="model.bean.BanPhimBean"%>

<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="css/new_style.css">
</head>
<body>
	<%@ include file="./common/header.jsp"%>
	<main>
		<section class="checkout_area pb-20 pt-20">
			<div class="container">
				<div class="row">
					<div class="col-lg-7">
						<h3>Thanh toán</h3>
						<form class="row contact_form">
							<div class="col-md-6 form-group p_star">
								<input type="text" class="form-control" id="first" name="name"
									placeholder="Tên người nhận" required>
							</div>
							<div class="col-md-6 form-group p_star">
								<input type="text" class="form-control" id="last" name="name"
									placeholder="Số điện thoại" required>
							</div>

							<div class="col-md-4 form-group p_star">
								<input type="text" class="form-control" id="number" required
									placeholder="Tỉnh/Thành phố" name="number">
							</div>
							<div class="col-md-4 form-group p_star">
								<input type="text" class="form-control" id="email" required
									placeholder="Quận/Huyện" name="compemailany">
							</div>
							<div class="col-md-4 form-group p_star">
								<input type="text" class="form-control" id="email" required
									placeholder="Phường/Xã" name="compemailany">
							</div>
							<div class="col-md-12 form-group p_star">
								<input type="text" class="form-control" id="add1" name="add1"
									placeholder="Số nhà, tên đường, phường xã" required>
							</div>

							<div class="col-md-12 form-group">
								<div class="creat_account">
									<h3>Lời nhắn</h3>
								</div>
								<textarea class="form-control" name="message" id="message"
									rows="1" cols="50" placeholder="Lưu ý cho người bán"></textarea>
							</div>
						</form>
					</div>
					<div class="col-lg-5">
						<div class="order_box">
							<h2>Hoá đơn của bạn</h2>
							<ul class="list">
								<c:forEach items="${listgiohang}" var="kb">
									<li><h5 class="list_title">
											Sản phẩm <span>Số lượng</span>
										</h5></li>
									<li><p class="name-wrap">
											<span class="name-product">${kb.getTenBanPhim()}</span> <span
												class="middle">x ${kb.getSoLuong()}</span>
										</p></li>
								</c:forEach>
							</ul>
							<ul class="list list_2">
								<li><a href="#">Tổng tiền hàng <span> <fmt:formatNumber
												pattern="###,###,###₫" value="${giohang.getTongTien()}"
												type="currency" />
									</span>
								</a></li>
								<li><a href="#">Phí Vận chuyển <span> <fmt:formatNumber
												pattern="###,###,###₫" value="0" type="currency" /></span>
								</a></li>
								<li><a href="#">Tổng thanh toán <span><fmt:formatNumber
												pattern="###,###,###₫" value="${giohang.getTongTien()}"
												type="currency" /></span>
								</a></li>
							</ul>
							<a class="btn_3 mt-4" href="checkout?order=true">Đặt hàng</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<%@ include file="./common/footer.jsp"%>

	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>

	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>

	<script src="js/aos.js"></script>

	<script src="js/scrollax.min.js"></script>
	<script src="js/main.js"></script>




</body>
</html>