<%@page import="utils.MyConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS here -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/flaticon.css">
</head>
<body>
	<%@ include file="./common/header.jsp"%>
	<main>
		<!--================login_part Area =================-->
		<section class="login_part">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-6">
						<div class="login_part_text text-center">
							<div class="login_part_text_iner">
								<h2>Bạn đã có tài khoản? Hãy bấm vào dưới để đăng nhập</h2>
								<p>AKKO.VN cung cấp các sản phẩm phân phối chính hãng tại
									Việt Nam</p>
								<a href="signip" class="btn_3">Đăng nhập</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="login_part_form">
							<div class="login_part_form_iner">
								<h3>
									Chỉ mất 30s ! <br> điền thông tin vào bên dưới
								</h3>
								<form class="row contact_form" action="signup" method="post"
									novalidate="novalidate">
									<div class="col-md-12 form-group p_star">
										<input type="text" class="form-control" id="fullname"
											name="fullname" value="" placeholder="Họ và Tên">
									</div>

									<div class="col-md-12 form-group p_star">
										<input type="email" class="form-control" id="email"
											name="email" value="" placeholder="Email">
									</div>

									<div class="col-md-12 form-group p_star">
										<input type="text" class="form-control" id="username"
											name="username" value="" placeholder="Tên đăng nhập">
									</div>

									<div class="col-md-12 form-group p_star">
										<input type="text" class="form-control" id="address"
											name="address" value="" placeholder="Địa chỉ">
									</div>

									<div class="col-md-12 form-group p_star">
										<input type="number" class="form-control" id="name"
											name="phone" placeholder="Số điện thoại">
									</div>

									<div class="col-md-12 form-group p_star">
										<input type="password" class="form-control" name="password"
											id="password" placeholder="Mật khẩu" required="required">
									</div>


									<div class="col-md-12 form-group p_star">
										<input type="password" class="form-control"
											id="confirm_password" name="confirm_password"
											placeholder="Xác nhận mật khẩu" required="required">
									</div>

									<div class="col-md-12 form-group">
										<div role="alert" id="message" class="mt-2"></div>
									</div>
									<div class="col-md-12 form-group p_star">
										<div class="g-recaptcha" 
											data-sitekey="<%=MyConstants.SITE_KEY%>"></div>
									</div>
									<div class="col-md-12 form-group">
										<button type="submit" value="submit" class="btn_3"
											id="signup_submit" disabled>Đăng ký</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--================login_part end =================-->
	</main>
	<%@ include file="./common/new_product.jsp"%>
	<%@ include file="./common/services.jsp"%>
	<%@ include file="./common/footer.jsp"%>


	<!-- Assets Folder -->
	<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="./assets/js/popper.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<!-- Jquery Mobile Menu -->
	<script src="./assets/js/jquery.slicknav.min.js"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="./assets/js/owl.carousel.min.js"></script>
	<script src="./assets/js/slick.min.js"></script>

	<!-- One Page, Animated-HeadLin -->
	<script src="./assets/js/wow.min.js"></script>
	<script src="./assets/js/animated.headline.js"></script>
	<script src="./assets/js/jquery.magnific-popup.js"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="./assets/js/jquery.scrollUp.min.js"></script>
	<script src="./assets/js/jquery.nice-select.min.js"></script>
	<script src="./assets/js/jquery.sticky.js"></script>

	<!-- contact js -->
	<script src="./assets/js/contact.js"></script>
	<script src="./assets/js/jquery.form.js"></script>
	<script src="./assets/js/jquery.validate.min.js"></script>
	<script src="./assets/js/mail-script.js"></script>
	<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

	<!-- Jquery Plugins, main Jquery -->
	<script src="./assets/js/plugins.js"></script>
	<script src="./assets/js/main.js"></script>

	<script type="text/javascript">
		function recaptchaCallback() {
			console.log("hha")
			$('#submitBtn').removeAttr('disabled');

		}
		$(document).ready(
				function() {
					function checkPasswordMatch() {
						var password = $("#password").val();

						console.log(password)
						var confirmPassword = $("#confirm_password").val();
						if (password != confirmPassword) {
							$("#message").html("Mật khẩu không khớp").addClass(
									"alert alert-warning");
							$("#signup_submit").attr("disabled", "disabled");
						} else {
							$("#message").html("").removeAttr("class");
							$("#signup_submit").removeAttr("disabled");
						}

					}
					$(document).ready(function() {
						$("#confirm_password").keyup(checkPasswordMatch);
					});
				});
	</script>
	<!-- reCAPTCHA with English language -->
	<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
</body>
</html>