<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!--? Popular Items Start -->
<div class="popular-items section-padding30">



	<div class="container">
		<!-- Section tittle -->
		<div class="row justify-content-center">
			<div class="col-xl-7 col-lg-8 col-md-10">
				<div class="section-tittle mb-70 text-center">
					<h2>Sản phẩm phổ biến</h2>
					<p>Được yêu thích bởi hầu hết người dùng</p>
				</div>
			</div>
		</div>

		<div class="toast" id="element" role="alert" aria-live="assertive"
			aria-atomic="true">
			<div class="toast-body">Hello, world! This is a toast message.
			</div>
		</div>

		<div class="row" id="content_product">
			<c:forEach items="${sixKeyBoards}" var="kb">
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 product_more">
					<div class="single-popular-items mb-50 text-center">
						<div class="popular-img">
							<img src="assets/img/gallery/${kb.getAnh()}" alt="">
							<div class="img-cap">
								<span onclick="addToCart('${kb.getMaBanPhim()}', 1)">Thêm
									vào giỏ hàng</span>

							</div>
							<div class="favorit-items">
								<span class="flaticon-heart"></span>
							</div>
						</div>
						<div class="popular-caption">
							<h3>
								<a href="product-details?keyboardId=${kb.getMaBanPhim()}"> <c:out
										value="${kb.getTenBanPhim()}"></c:out></a>
							</h3>
							<span> <fmt:formatNumber pattern="###,###,###₫"
									value="${kb.getGiaBan()}" type="currency" />
							</span>
						</div>
					</div>
				</div>
			</c:forEach>


		</div>
	</div>
	<!-- Button -->
	<div class="row justify-content-center">
		<div class="room-btn mt-70 more_product">
			<button class="btn view-btn1 btn_load" onclick="loadMore(this)">Xem
				thêm</button>
		</div>
	</div>
</div>
<!-- Popular Items End -->

<!-- Assets Folder -->
<script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="./assets/js/popper.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<!-- Jquery Mobile Menu -->
<script src="./assets/js/jquery.slicknav.min.js"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="./assets/js/owl.carousel.min.js"></script>
<script src="./assets/js/slick.min.js"></script>

<!-- One Page, Animated-HeadLin -->
<script src="./assets/js/wow.min.js"></script>
<script src="./assets/js/animated.headline.js"></script>
<script src="./assets/js/jquery.magnific-popup.js"></script>

<!-- Scrollup, nice-select, sticky -->
<script src="./assets/js/jquery.scrollUp.min.js"></script>
<script src="./assets/js/jquery.nice-select.min.js"></script>
<script src="./assets/js/jquery.sticky.js"></script>

<!-- contact js -->
<script src="./assets/js/contact.js"></script>
<script src="./assets/js/jquery.form.js"></script>
<script src="./assets/js/jquery.validate.min.js"></script>
<script src="./assets/js/mail-script.js"></script>
<script src="./assets/js/jquery.ajaxchimp.min.js"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="./assets/js/plugins.js"></script>
<script src="./assets/js/main.js"></script>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<script type="text/javascript">
	function addToCart(keyboardId, quantity) {
		console.log(keyboardId, quantity)
		
		$.ajax({
			url : "/JavaProject/cart",
			type : "get", //send it through get method
			timeout : 100000,
			data : {
				keyboardId,
				quantity,
			},
			success : function(data) {
				$(document).ready(function(){
		            $('#element').toast('show');
		        })
			},
			error : function(xhr) {
				//Do Something to handle error
			}
		});
	}

	function loadMore() {
		let amount = document.getElementsByClassName("product_more").length;
		console.log(amount)

		$.ajax({
			url : "/JavaProject/load",
			type : "get", //send it through get method
			data : {
				popular : amount
			},
			success : function(data) {
				var row = document.getElementById("content_product");
				row.innerHTML += data;
			},
			error : function(xhr) {
				//Do Something to handle error
			}
		});
	}
</script>


