<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	
<!--? Gallery Area Start -->
<div class="gallery-area">
	<div class="container-fluid p-0 fix">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="section-tittle mb-70">
						<h2>Bộ sưu tập</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 col-lg-4 col-md-6 col-sm-6">
				<div class="single-gallery mb-30">
					<div class="gallery-img big-img"
						style="background-image: url(assets/img/collection/tokyo.jpg);"></div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
				<div class="single-gallery mb-30">
					<div class="gallery-img big-img"
						style="background-image: url(assets/img/collection/one.jpg);"></div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-4 col-md-12">
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-6 col-sm-6">
						<div class="single-gallery mb-30">
							<div class="gallery-img small-img"
								style="background-image: url(assets/img/collection/red.jpg);"></div>
						</div>
					</div>
					<div class="col-xl-12 col-lg-12  col-md-6 col-sm-6">
						<div class="single-gallery mb-30">
							<div class="gallery-img small-img"
								style="background-image: url(assets/img/collection/doremon.jpg);"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- Gallery Area End -->
