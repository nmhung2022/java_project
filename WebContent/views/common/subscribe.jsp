<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!--================End Single Product Area =================-->
	<!-- subscribe part here -->
	<section class="subscribe_part section_padding">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="subscribe_part_content">
						<h2>ĐĂNG KÝ NHẬN TIN TỪ AKKO.VN</h2>
						<p>Cảm ơn bạn đã luôn đồng hành cùng Akko Việt Nam.</p>
						<div class="subscribe_form">
							<input type="email" placeholder="Email của bạn"> <a
								href="#" class="btn_1">Đăng ký</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- subscribe part end -->
</body>
</html>