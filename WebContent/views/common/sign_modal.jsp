<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="modal.css" rel="stylesheet" type="text/css"/>
</head>
<body>


	<div id="loginModal" class="modal fade">
		<div class="modal-dialog modal-login">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Đăng nhập</h4>
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<form action="signin" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" class="form-control" name="username"
									placeholder="Tên đăng nhập" required="required">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control" name="password"
									placeholder="Mật khẩu" required="required">
							</div>
							<div class="alert alert-danger mt-3" role="alert"
								id="message-password">Tên đăng nhập hoặc mật khẩu không
								chính xác!</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btn-lg">Đăng
								nhập</button>
						</div>
						<p class="hint-text">
							<a href="forgot">Quên mật khẩu?</a>
						</p>
					</form>
				</div>
				<div class="modal-footer">
					<a href="signup">Tao mới tài khoản</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>