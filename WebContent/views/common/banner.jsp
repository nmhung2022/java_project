<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS here -->

<link rel="stylesheet" href="css/animate.css" />
<link rel="stylesheet" href="css/owl.carousel.min.css" />

<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<!--? slider Area Start -->
	<section id="home-section" class="hero">
		<div class="home-slider owl-carousel">
			<div class="slider-item js-fullheight">
				<div class="overlay overlayy"></div>
				<div class="container-fluid p-0">
					<div
						class="row d-md-flex no-gutters slider-text align-items-center justify-content-end"
						data-scrollax-parent="true">
						<img class="one-third order-md-last img-fluid" src="images/1.jpg"
							alt="World Tour Tokyo R2" />
						<div class="one-forth d-flex align-items-center ftco-animate"
							data-scrollax=" properties: { translateY: '70%' }">
							<div class="text">
								<span class="subheading">#Hot new</span>
								<div class="horizontal">
									<h1 class="mb-4 mt-3">AKKO 5108 World Tour Tokyo R2</h1>
									<p>
										<a href="#" class="btn-custom">Khám Phá</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="slider-item js-fullheight">
				<div class="overlay overlayy"></div>
				<div class="container-fluid p-0">
					<div
						class="row d-flex no-gutters slider-text align-items-center justify-content-end"
						data-scrollax-parent="true">
						<img class="one-third order-md-last img-fluid"
							src="images/store.png" alt="" />
						<div class="one-forth d-flex align-items-center ftco-animate"
							data-scrollax=" properties: { translateY: '70%' }">
							<div class="text">
								<span class="subheading">#Official</span>
								<div class="horizontal">
									<h1 class="mb-4 mt-3">Tại Việt Nam</h1>

									<p>
										<a href="#" class="btn-custom">Khám Phá</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slider-item js-fullheight">
				<div class="overlay overlayy"></div>
				<div class="container-fluid p-0">
					<div
						class="row d-flex no-gutters slider-text align-items-center justify-content-end"
						data-scrollax-parent="true">
						<img class="one-third order-md-last img-fluid" src="images/2.png"
							alt="" />
						<div class="one-forth d-flex align-items-center ftco-animate"
							data-scrollax=" properties: { translateY: '70%' }">
							<div class="text">
								<span class="subheading">#Hot New</span>
								<div class="horizontal">
									<h1 class="mb-4 mt-3">New Color</h1>
									<p>
										<a href="#" class="btn-custom">Khám Phá</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/aos.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>