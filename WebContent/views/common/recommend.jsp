<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!--? Popular Items Start -->
<div class="popular-items section-padding30">
	<div class="container">
		<!-- Section tittle -->
		<div class="row justify-content-center">
			<div class="col-xl-7 col-lg-8 col-md-10">
				<div class="section-tittle mb-70 text-center">
					<h2>Có thể bạn cũng thích</h2>
				</div>
			</div>
		</div>
		<div class="row">

			<c:forEach items="${keyboardsRelated}" var="kb">

				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
					<div class="single-popular-items mb-50 text-center">
						<div class="popular-img">
							<img src="assets/img/gallery/${kb.getAnh()}" alt="">
							<div class="img-cap">
								<a href="cart"> <span>Thêm vào giỏ hàng</span>
								</a>
							</div>
							<div class="favorit-items">
								<span class="flaticon-heart"></span>
							</div>
						</div>
						<div class="popular-caption">
							<h3>
								<a href="product-details?keyboardId=${kb.getMaBanPhim()}"> <c:out
										value="${kb.getTenBanPhim()}"></c:out></a>
							</h3>
							<span> <fmt:formatNumber value="${kb.getGiaBan()}"
									pattern="###,###,###₫" type="currency" />
							</span>
						</div>
					</div>
				</div>
			</c:forEach>


		</div>
	</div>
	<!-- Button -->
	<div class="row justify-content-center">
		<div class="room-btn mt-70 more_product">
			<a href="catagori.html" class="btn view-btn1">Xem thêm</a>
		</div>
	</div>
</div>
<!-- Popular Items End -->
