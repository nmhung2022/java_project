<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="css/new_style.css">
</head>

<body>

	<div id="preloader-active">
		<div
			class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="assets/img/logo/logo.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<header>
		<!-- Header Start -->
		<div class="header-area">
			<div class="main-header header-sticky">
				<div class="container-fluid">
					<div class="menu-wrapper">
						<!-- Logo -->
						<div class="logo">
							<a href="home"><img src="assets/img/logo/logo.jpg" alt=""></a>
						</div>
						<!-- Main-menu -->
						<div class="main-menu d-none d-lg-block">
							<nav>
								<ul id="navigation">
									<li><a href="product">BÀN PHÍM</a></li>
									<li><a href="product">CHUỘT</a></li>
									<li class="hot"><a href="#">SẢN PHẨM</a>
										<ul class="submenu">
											<li><a href="shop.html">Sắp ra mắt</a></li>
											<li><a href="product_details.html">Mới ra mắt</a></li>
										</ul></li>
									<li><a href="blog.html">TIN TỨC</a>
										<ul class="submenu">
											<li><a href="blog.html">BÀI VIẾT</a></li>
										</ul></li>
									<li><a href="#">KEYCAP</a></li>
									<li><a href="#">SWITCH</a></li>
									<li><a href="#">PHỤ KIỆN</a></li>
								</ul>
							</nav>
						</div>
						<!-- Header Right -->
						<div class="header-right">
							<ul>
								<li><span class="pd"> <i class="fa fa-user"> </i>
								</span>
									<ul class="submenu">
										<c:choose>
											<c:when test="${ not empty sessionScope['auth']}">
												<li class="icon"><a href="profile">Tài khoản</a></li>
												<li class="icon"><a href="order">Lịch sử mua hàng</a></li>
												<li class="icon"><a href="signout">Đăng xuất</a></li>
											</c:when>
											<c:otherwise>
												<li class="icon"><a href="signin">Đăng nhập</a></li>
												<li class="icon"><a href="signup">Tạo tài khoản</a></li>
											</c:otherwise>
										</c:choose>
									</ul></li>
								<li>
									<div class="nav-search search-switch ">
										<span> <i class="fa fa-search"></i>
										</span>
									</div>
								</li>

								<li><a href="cart"><span> <i
											class="fa fa-shopping-cart"></i>
									</span></a></li>
							</ul>
						</div>



					</div>
					<!-- Mobile Menu -->
					<div class="col-12">
						<div class="mobile_menu d-block d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header End -->
	</header>
</body>
</html>