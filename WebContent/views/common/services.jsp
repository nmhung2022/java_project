<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/animate.css" />

<link rel="stylesheet" href="css/flaticon.css" />
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<section class="ftco-section ftco-no-pt ftco-no-pb">
		<div class="container">
			<div class="row no-gutters ftco-services">
				<div
					class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services p-4 py-md-5">
						<div
							class="icon d-flex justify-content-center align-items-center mb-4">
							<span class="flaticon-bag"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">Miễn phí vận chuyển</h3>
							<p>Tất cả sản phẩm được đặt hàng tại Akko.vn đều được miễn
								phí vận chuyển. Khách hàng được kiểm tra hàng trước khi thanh
								toán.</p>
						</div>
					</div>
				</div>
				<div
					class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services p-4 py-md-5">
						<div
							class="icon d-flex justify-content-center align-items-center mb-4">
							<span class="flaticon-customer-service"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">HỖ TRỢ 247</h3>
							<p>Hỗ trợ khách hàng trực tuyến về mua hàng cũng như các thắc
								mắc về sản phẩm.</p>
						</div>
					</div>
				</div>
				<div
					class="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services p-4 py-md-5">
						<div
							class="icon d-flex justify-content-center align-items-center mb-4">
							<span class="flaticon-payment-security"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">CHẾ ĐỘ BẢO HÀNH VƯỢT TRỘI</h3>
							<p>ất cả sản phẩm Akko đều được bảo hành 12 tháng, 1 đổi 1.
								Trung tâm bảo hành toàn quốc.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/aos.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>