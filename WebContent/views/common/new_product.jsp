<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- ? New Product Start -->
<section class="new-product-area section-padding30">
	<div class="container">
		<!-- Section tittle -->
		<div class="row">
			<div class="col-xl-12">
				<div class="section-tittle mb-70">
					<h2>Sản phẩm nổi bật</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
				<div class="single-new-pro mb-30 text-center">
					<div class="product-img">
						<img src="images/highlight/3087v2.png" alt="">
					</div>
					<div class="product-caption">
						<h3>
							<a href="product_details.html">Bàn phím cơ AKKO 3087 v2
								Monet’s Pond (Akko switch v2)</a>
						</h3>
						<span>1,590,000₫</span>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
				<div class="single-new-pro mb-30 text-center">
					<div class="product-img">
						<img src="images/highlight/3068v2.png" alt="">
					</div>
					<div class="product-caption">
						<h3>
							<a href="product_details.html">Bàn phím AKKO 3087S RGB – Pink
								(Cherry switch)</a>
						</h3>
						<span>1,790,000₫</span>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
				<div class="single-new-pro mb-30 text-center">
					<div class="product-img">
						<img src="images/highlight/3087sRGB.png" alt="">
					</div>
					<div class="product-caption">
						<h3>
							<a href="product_details.html">Bàn phím cơ AKKO 3068 v2 World
								Tour Tokyo R2 RGB – Bluetooth 5.0</a>
						</h3>
						<span>1,890,000₫</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--  New Product End -->
