<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<footer>
	<!-- Footer Start-->
	<div class="footer-area footer-padding">
		<div class="container">
			<div class="row d-flex justify-content-between">
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-7">
					<div class="single-footer-caption mb-50">
						<div class="single-footer-caption mb-30">
							<!-- logo -->
							<div class="footer-logo">
								<a href="home"><img src="assets/img/logo/logo.jpg" alt=""></a>
							</div>
							<div class="footer-tittle">
								<h3>AKKO VIỆT NAM.</h3>
								<div class="footer-pera">
									<p>AKKO.VN cung cấp các sản phẩm phân phối chính hãng tại
										Việt Nam.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-4 col-sm-7">
					<div class="single-footer-caption mb-50">
						<div class="footer-tittle">
							<h4>Sản phẩm mới</h4>
							<ul>
								<li><a href="#">AKKO Coiled Cable</a></li>
								<li><a href="#">AKKO Hamster Plus – HIMA</a></li>
								<li><a href="#">AKKO Keycap Set</a></li>
								<li><a href="#">Kit bàn phím cơ AKKO ACR64</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-5 col-sm-8">
					<div class="single-footer-caption mb-50">
						<div class="footer-tittle">
							<h4>Hỗ trợ</h4>
							<ul>
								<li><a href="#">TRUNG TÂM BẢO HÀNH MIỀN BẮC</a></li>
								<li><a href="#">TRUNG TÂM BẢO HÀNH MIỀN TRUNG</a></li>
								<li><a href="#">TRUNG TÂM BẢO HÀNH MIỀN NAM</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Footer bottom -->
			<div class="row align-items-center">
				<div class="col-xl-7 col-lg-8 col-md-7">
					<div class="footer-copy-right">
						<p>
							Copyright &copy;
							<script>
								document.write(new Date().getFullYear());
							</script>
							All rights reserved <a href="https://akkogear.com.vn"
								target="_blank">AKKOGEAR.</a>

						</p>
					</div>
				</div>
				<div class="col-xl-5 col-lg-4 col-md-5">
					<div class="footer-copy-right f-right">
						<!-- social -->
						<div class="footer-social">
							<a href="#"><i class="fab fa-twitter"></i></a> <a
								href="https://www.facebook.com/sai4ull"><i
								class="fab fa-facebook-f"></i></a> <a href="#"><i
								class="fab fa-behance"></i></a> <a href="#"><i
								class="fas fa-globe"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer End-->
</footer>
