<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="model.bean.KhachHangBean"%>
<%@page import="java.util.ArrayList"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Akko | Việt Nam</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/slicknav.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<link rel="stylesheet" href="assets/css/themify-icons.css">
<link rel="stylesheet" href="assets/css/slick.css">
<link rel="stylesheet" href="assets/css/nice-select.css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="css/new_style.css">
</head>
<body>


	<%@ include file="./common/header.jsp"%>
	<main>
		<section class="ftco-cart pt-20 pb-20">
			<div class="container">

				<c:choose>
					<c:when test="${not empty paidSuccess}">
						<div class="container mt-4">
							<div class="row">
								<div class="col">
									<div class="alert alert-success" role="alert">
										<h4 class="alert-heading">Đặt hàng hoàn tất!</h4>
										<p>Nếu quá trình đặt hàng của bạn thành công, bạn sẽ nhận
											được xác nhận đặt hàng qua địa chỉ e-mail mà bạn đã cung cấp
											tại trang thanh toán. Nếu không, xin vui lòng liên hệ với
											chúng tôi và chúng tôi có thể hỗ trợ bạn trong vấn đề này.</p>
										<hr>
										<p class="mb-0">Vui lòng lưu ý rằng thời gian giao hàng
											ước tính (3-5 ngày làm việc) là trong vòng dự tính và có thể
											thay đổi do các yếu tố tác động bên ngoài không thuộc quyền
											kiểm soát của Company và dịch vụ chuyển phát.</p>
									</div>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${not empty emptycart}">
								<div class="container mt-4">
									<div class="row">
										<div class="col">
											<div class="alert alert-danger" role="alert">
												<h4 class="alert-heading">Không thể thanh toán!</h4>
												<p>Hiện tại giỏ hàng của bạn đang không chưa sản phẩm
													nào nên không thể thanh toán, nhưng bạn có thể donate để
													ủng hộ.</p>
												<hr>
												<p class="mb-0">
													Bạn có thể bấm vào <strong>Mua xắm ngay</strong> ở bên dưới
													để xem các sản phẩm đang được bán
												</p>
											</div>
										</div>
									</div>
								</div>
							</c:when>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<%@ include file="list_cart.jsp"%>
			</div>
		</section>
	</main>
	<!-- Footer HTML -->
	<%@ include file="./common/footer.jsp"%>

</body>
</html>
