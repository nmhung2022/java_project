USE [QLBanPhim]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsValidEmail]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_IsValidEmail](@EMAIL varchar(100))RETURNS bit as
BEGIN     
  DECLARE @bitEmailVal as Bit
  DECLARE @EmailText varchar(100)

  SET @EmailText=ltrim(rtrim(isnull(@EMAIL,'')))

  SET @bitEmailVal = case when @EmailText = '' then 0
                          when @EmailText like '% %' then 0
                          when @EmailText like ('%["(),:;<>\]%') then 0
                          when substring(@EmailText,charindex('@',@EmailText),len(@EmailText)) like ('%[!#$%&*+/=?^`_{|]%') then 0
                          when (left(@EmailText,1) like ('[-_.+]') or right(@EmailText,1) like ('[-_.+]')) then 0                                                                                    
                          when (@EmailText like '%[%' or @EmailText like '%]%') then 0
                          when @EmailText LIKE '%@%@%' then 0
                          when @EmailText NOT LIKE '_%@_%._%' then 0
                          else 1 
                      end
  RETURN @bitEmailVal
END 
GO
/****** Object:  Table [dbo].[Loai]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loai](
	[MaLoai] [int] NOT NULL,
	[TenLoai] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_loai] PRIMARY KEY CLUSTERED 
(
	[MaLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[HtSach]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[HtSach]
AS
SELECT     dbo.sach.maloai, dbo.loai.tenloai, dbo.sach.masach, dbo.sach.tensach, dbo.sach.tacgia, dbo.sach.gia
FROM         dbo.loai INNER JOIN
                      dbo.sach ON dbo.loai.maloai = dbo.sach.maloai
GO
/****** Object:  View [dbo].[VTK]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTK]
AS
SELECT     dbo.loai.tenloai, COUNT(dbo.sach.tacgia) AS SoSach, SUM(dbo.sach.soluong) AS TongSl, AVG(dbo.sach.gia) AS TbcGia
FROM         dbo.loai INNER JOIN
                      dbo.sach ON dbo.loai.maloai = dbo.sach.maloai
GROUP BY dbo.loai.tenloai
GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[MaHoaDon] [bigint] IDENTITY(1,1) NOT NULL,
	[MaKhachHang] [bigint] NULL,
	[NgayDatMua] [datetime] NOT NULL,
	[DaMua] [bit] NULL,
 CONSTRAINT [PK_hoadon] PRIMARY KEY CLUSTERED 
(
	[MaHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaChiTietHD] [bigint] IDENTITY(1,1) NOT NULL,
	[MaBanPhim] [nvarchar](50) NOT NULL,
	[SoLuongMua] [int] NOT NULL,
	[MaHoaDon] [bigint] NOT NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_ChiTietHoaDon] PRIMARY KEY CLUSTERED 
(
	[MaChiTietHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[view_OrderHisoty]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_OrderHisoty]
as
select h.MaHoaDon, h.NgayDatMua, count(c.SoLuongMua) as SoLuongMua, c.TrangThai, h.MaKhachHang
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = c.MaHoaDon
	where c.MaHoaDon = h.MaHoaDon
	group by h.MaHoaDon, h.NgayDatMua, c.SoLuongMua, c.TrangThai, h.MaKhachHang
GO
/****** Object:  View [dbo].[tk]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tk]
AS
SELECT     dbo.loai.tenloai, SUM(dbo.sach.soluong) AS TongSoLuong, AVG(dbo.sach.gia) AS TBCGia, COUNT(dbo.sach.tensach) AS SoSach
FROM         dbo.loai INNER JOIN
                      dbo.sach ON dbo.loai.maloai = dbo.sach.maloai
GROUP BY dbo.loai.tenloai
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKhachHang] [bigint] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](50) NOT NULL,
	[DiaChi] [nvarchar](50) NULL,
	[SoDienThoai] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[TenDangNhap] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vtktest]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vtktest]
AS
SELECT     dbo.loai.tenloai, SUM(dbo.sach.soluong) AS Tongsl, SUM(dbo.ChiTietHoaDon.SoLuongMua) AS Tongslmua, COUNT(dbo.KhachHang.hoten) AS SoKhach
FROM         dbo.hoadon INNER JOIN
                      dbo.ChiTietHoaDon ON dbo.hoadon.MaHoaDon = dbo.ChiTietHoaDon.MaHoaDon INNER JOIN
                      dbo.sach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.KhachHang ON dbo.hoadon.makh = dbo.KhachHang.makh
GROUP BY dbo.loai.tenloai
GO
/****** Object:  View [dbo].[VTKabc]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTKabc]
AS
SELECT     dbo.loai.tenloai, SUM(dbo.sach.soluong) AS Tongkho, SUM(dbo.ChiTietHoaDon.SoLuongMua) AS Tongmua, COUNT(dbo.KhachHang.hoten) AS SoKH
FROM         dbo.loai INNER JOIN
                      dbo.sach ON dbo.loai.maloai = dbo.sach.maloai INNER JOIN
                      dbo.ChiTietHoaDon ON dbo.sach.masach = dbo.ChiTietHoaDon.MaSach INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.KhachHang ON dbo.hoadon.makh = dbo.KhachHang.makh
GROUP BY dbo.loai.tenloai
GO
/****** Object:  View [dbo].[Vtest]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vtest]
AS
SELECT     dbo.sach.tensach, dbo.loai.tenloai, dbo.ChiTietHoaDon.SoLuongMua, dbo.sach.gia, dbo.hoadon.NgayMua, dbo.KhachHang.hoten
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.KhachHang ON dbo.hoadon.makh = dbo.KhachHang.makh INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  View [dbo].[VTamBay]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTamBay]
AS
SELECT     dbo.ChiTietHoaDon.MaSach, dbo.ChiTietHoaDon.SoLuongMua, dbo.hoadon.makh, dbo.hoadon.NgayMua, dbo.KhachHang.diachi, dbo.KhachHang.hoten, 
                      dbo.sach.tensach, dbo.sach.tacgia
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.KhachHang ON dbo.hoadon.makh = dbo.KhachHang.Makh INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  View [dbo].[Vqq]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vqq]
AS
SELECT     dbo.loai.tenloai, dbo.sach.tensach, dbo.sach.tacgia, dbo.ChiTietHoaDon.SoLuongMua
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  View [dbo].[Vhoadon]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vhoadon]
AS
SELECT     dbo.sach.masach, dbo.sach.tensach, dbo.sach.gia, dbo.ChiTietHoaDon.SoLuongMua
FROM         dbo.sach INNER JOIN
                      dbo.ChiTietHoaDon ON dbo.sach.masach = dbo.ChiTietHoaDon.MaSach INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon
WHERE     (dbo.hoadon.damua = 0)
GO
/****** Object:  View [dbo].[Vabcd]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vabcd]
AS
SELECT     dbo.ChiTietHoaDon.MaSach, dbo.ChiTietHoaDon.SoLuongMua, dbo.hoadon.NgayMua, dbo.loai.tenloai, dbo.sach.soluong
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  View [dbo].[rrr]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[rrr]
AS
SELECT     dbo.loai.tenloai, dbo.sach.tensach, dbo.sach.gia, dbo.ChiTietHoaDon.SoLuongMua
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.hoadon ON dbo.ChiTietHoaDon.MaHoaDon = dbo.hoadon.MaHoaDon INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  View [dbo].[abc]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[abc]
AS
SELECT     dbo.sach.tensach, dbo.loai.tenloai, dbo.ChiTietHoaDon.SoLuongMua
FROM         dbo.ChiTietHoaDon INNER JOIN
                      dbo.sach ON dbo.ChiTietHoaDon.MaSach = dbo.sach.masach INNER JOIN
                      dbo.loai ON dbo.sach.maloai = dbo.loai.maloai
GO
/****** Object:  Table [dbo].[BanPhim]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BanPhim](
	[MaBanPhim] [nvarchar](50) NOT NULL,
	[TenBanPhim] [nvarchar](250) NOT NULL,
	[SoLuong] [bigint] NOT NULL,
	[GiaBan] [money] NOT NULL,
	[MaLoai] [int] NOT NULL,
	[Anh] [nvarchar](50) NOT NULL,
	[NgayNhap] [date] NOT NULL,
	[MaSwitch] [int] NOT NULL,
	[DanhGia] [int] NULL,
	[Mota] [ntext] NULL,
 CONSTRAINT [PK_sach] PRIMARY KEY CLUSTERED 
(
	[MaBanPhim] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BinhLuan]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BinhLuan](
	[TenDangNhap] [nvarchar](50) NULL,
	[TenTaiKhoan] [nvarchar](50) NULL,
	[MaSach] [nvarchar](50) NULL,
	[NoiDung] [ntext] NULL,
	[ThoiGian] [datetime] NULL,
	[DanhGia] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DangNhap]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DangNhap](
	[TenDangNhap] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](50) NULL,
	[Quyen] [bit] NOT NULL,
 CONSTRAINT [PK_DangNhap] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Switch]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Switch](
	[MaSwitch] [int] NOT NULL,
	[TenSwitch] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Switch] PRIMARY KEY CLUSTERED 
(
	[MaSwitch] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3068sa', N'Bàn phím cơ Akko 3068 Silent - (Akko switch)', 18, 1399000.0000, 1, N'bitcoin.jpg', CAST(N'2021-12-19' AS Date), 1, NULL, N'Model: 3068 (68 keys) – LED trắng đơn sắc (17 hiệu ứng)
Kết nối: Bluetooth 5.0 / USB Type C, có thể tháo rời
Kết nối 4 thiết bị (4 profiles)
Dung lượng pin: 1800 mah ~ 200 giờ hoạt động
Kích thước: 312x102x40 mm
Keycap: PBT Dye-Subbed – OEM Profile
Loại switch: Gateron switch (Pink/Orange)
Hỗ trợ NKRO/Multimedia/Macro/Khóa phím Windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + keycap tặng kèm
Tương thích: Windows / MacOS / Linux / IOS / ANDROID')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3068sg', N'Bàn phím cơ Akko 3068 Silent - (Gateron switch)', 32, 1599000.0000, 3, N'3068sg.jpg', CAST(N'2021-04-23' AS Date), 2, NULL, N'Model: 3068 (68 keys) – LED trắng đơn sắc (17 hiệu ứng)
Kết nối: Bluetooth 5.0 / USB Type C, có thể tháo rời
Kết nối 4 thiết bị (4 profiles)
Dung lượng pin: 1800 mah ~ 200 giờ hoạt động
Kích thước: 312x102x40 mm
Keycap: PBT Dye-Subbed – OEM Profile
Loại switch: Akko switch (Blue/Pink/Orange)
Hỗ trợ NKRO/Multimedia/Macro/Khóa phím Windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + keycap tặng kèm
Tương thích: Windows / MacOS / Linux / IOS / ANDROID')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3084as', N'Bàn phím cơ Akko 3084 Silent - (Akko switch)', 3, 1599000.0000, 3, N'3084s.jpg', CAST(N'2020-12-03' AS Date), 1, NULL, N'Model: 3084 (84keys) – LED trắng đơn sắc (17 hiệu ứng)
Kết nối: Bluetooth 5.0 / USB Type C, có thể tháo rời
Kết nối 4 thiết bị (4 profiles)
Dung lượng pin: 1800 mah ~ 200 giờ hoạt động
Kích thước: 315 x 127 x 40mm
Keycap: PBT – OEM Profile
Loại switch: Gateron switch (Pink/Orange)
Hỗ trợ NKRO/Multimedia/Macro/Khóa phím Windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + keycap tặng kèm
Tương thích: Windows / MacOS / Linux / IOS / ANDROID')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3084sg', N'Bàn phím cơ Akko 3084 Silent - Gateron switch)', 12, 1799000.0000, 3, N'3084sg.jpg', CAST(N'2021-04-21' AS Date), 2, NULL, N'Model: 3084 (84keys) – LED trắng đơn sắc (17 hiệu ứng)
Kết nối: Bluetooth 5.0 / USB Type C, có thể tháo rời
Kết nối 4 thiết bị (4 profiles)
Dung lượng pin: 1800 mah ~ 200 giờ hoạt động
Kích thước: 315 x 127 x 40mm
Keycap: PBT – OEM Profile
Loại switch: Gateron switch (Pink/Orange)
Hỗ trợ NKRO/Multimedia/Macro/Khóa phím Windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + keycap tặng kèm
Tương thích: Windows / MacOS / Linux / IOS / ANDROID')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3084sposc', N'Bàn phím cơ AKKO 3084 SP Ocean Star (Cherry switch)', 30, 2189000.0000, 1, N'3084sposc.jpg', CAST(N'2020-05-12' AS Date), 1, NULL, N'Model: 3084 (84 keys)
Kết nối: USB Type C, có thể tháo rời
Hỗ trợ NKRO
Keycap: PBT
Font được in ở mặt nghiêng của nút
Keycap custom lấy chủ đạo là màu xanh, đặc trưng của đại dương
Loại switch: Cherry MX
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + keycap tặng kèm')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3087v2dsmrba', N'Bàn phím cơ AKKO 3087 v2 DS Matcha Red Bean (Akko switch v2)', 29, 1399000.0000, 5, N'3087v2dsmrba.jpg', CAST(N'2021-09-12' AS Date), 1, NULL, N'Model: 3087 (Tenkeyless, 87keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 360 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Double-Shot, Cherry profile
Loại switch: Akko switch (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 20 keycap tặng kèm + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3087v2dsosa', N'Bàn phím cơ AKKO 3087 v2 DS Ocean Star (Akko sw v2)', 12, 1399000.0000, 6, N'3087v2dsosa.jpg', CAST(N'2021-11-23' AS Date), 1, NULL, N'NULLModel: 3087 (Tenkeyless, 87 keys)
Kết nối: USB Type-C, có thể tháo rời
Kích thước: 360 x 140 x 40mm | Trọng lượng ~ 0.95 kg
Hỗ trợ NKRO / Multimedia / Macro / Khóa phím windows
Keycap: PBT Double-Shot, Cherry profile
Loại switch: Akko switch (Blue/Orange/Pink) v2
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB Type-C to USB + 20 keycap tặng kèm
Tương thích: Windows / MacOS / Linux
Bàn phím AKKO khi kết nối với MacOS: (Ctrl -> Control | Windows -> Command | Alt -> Option, Mojave OS trở lên sẽ điều chỉnh được thứ tự của các phím này)')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3087v2opca', N'Bàn phím cơ Akko 3087 v2 One Piece – Chopper (Akko switch)', 25, 1499000.0000, 4, N'3087v2opca.jpg', CAST(N'2021-05-12' AS Date), 1, NULL, N'Model: 3087 (TKL, 108 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 442 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Bàn phím lấy cảm hứng từ nhân vật Chopper trong bộ truyện tranh/anime One Piece với màu chủ đạo là hồng/trắng/xanh
Loại switch: Akko (Blue/Orange/Pink)
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3087v2opla', N'Bàn phím cơ AKKO 3087 v2 One Piece – Luffy (Akko switch v2)', 6, 1499000.0000, 4, N'3087v2opla.jpg', CAST(N'2021-06-22' AS Date), 1, NULL, N'Model: 3087 (TKL, 87 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 360 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Bàn phím lấy cảm hứng từ nhân vật Luffy trong bộ truyện tranh/anime One Piece với màu chủ đạo là hồng/trắng/xanh
Loại switch: Akko (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3087v2sea', N'Bàn phím cơ AKKO 3087 Steam Engine (Akko switch v2)', 12, 1399000.0000, 2, N'3087v2sea.jpg', CAST(N'2020-12-03' AS Date), 1, NULL, N'Model: 3087 (Tenkeyless, 87 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 360 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Loại switch: Akko (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + 9 keycap tặng kèm')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3098dsmrba', N'Bàn phím cơ AKKO 3098 DS Matcha Red Bean (Akko switch v2)', 4, 1599000.0000, 5, N'3098dsmrba.jpg', CAST(N'2021-05-12' AS Date), 1, NULL, N'Model: 3098 (98 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 382x134x40 mm
Hỗ trợ NKRO
Keycap: PBT Double-Shot, Cherry profile
Loại switch: Akko switch (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 20 keycap tặng kèm + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3098dsmrbg', N'Bàn phím cơ AKKO 3098 DS Matcha Red Bean (Gateron Cap Yellow)', 12, 2099000.0000, 5, N'3098dsmrbg.jpg', CAST(N'2021-08-21' AS Date), 3, NULL, N'Model: 3098 (98 keys)
Kết nối: USB Type C, có thể tháo rời
Kích thước: 382x134x40 mm
Hỗ trợ NKRO / Multimedia / Macro / Khóa phím Windows
Keycap: PBT Double-Shot, Cherry profile
Loại switch: Gateron Cap Yellow switch
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 20 keycap tặng kèm + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3098v2wtta', N'Bàn phím cơ AKKO 3098 World Tour Tokyo (Akko switch v2)', 13, 1599000.0000, 1, N'3098wtta.jpg', CAST(N'2020-03-12' AS Date), 1, NULL, N'Model: 3098 (98 keys)
Kết nối: USB Type C, có thể tháo rời
Kích thước: 382x134x40 mm
Hỗ trợ NKRO / Multimedia / Macro / Khóa phím Windows
Keycap: PBT Dye-Subbed, OEM profile
Loại switch: AKKO switch (Blue/Orange/Pink) v2
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 06 keycap tặng kèm + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3108v2dsosa', N'Bàn phím cơ AKKO 3108 v2 DS Ocean Star (AKKO sw v2)', 19, 1599000.0000, 6, N'3108v2dsosa.jpg', CAST(N'2021-12-04' AS Date), 1, NULL, N'Model: 3108 (Fullsize, 108 keys)
Kết nối: USB Type C, có thể tháo rời
Kích thước: 440 x 140 x 40mm | Trọng lượng ~ 1.2 kg
Hỗ trợ NKRO / Multimedia / Macro / Khóa phím windows
Keycap: PBT Double-Shot, Cherry profile
Loại switch: Akko switch (Blue/Orange/Pink) v2
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB Type-C to USB + 20 keycap tặng kèm
Tương thích: Windows / MacOS / Linux
Bàn phím AKKO khi kết nối với MacOS: (Ctrl -> Control | Windows -> Command | Alt -> Option, Mojave OS trở lên sẽ điều chỉnh được thứ tự của các phím này)')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3108v2opca', N'Bàn phím cơ Akko 3108 v2 One Piece – Chopper (Akko switch)', 42, 1699000.0000, 4, N'3108v2opca.jpg', CAST(N'2021-11-21' AS Date), 1, NULL, N'Model: 3108 (Fullsize, 108 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 442 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Bàn phím lấy cảm hứng từ nhân vật Chopper trong bộ truyện tranh/anime One Piece với màu chủ đạo là hồng/trắng/xanh
Loại switch: Akko (Blue/Orange/Pink)
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3108v2opla', N'Bàn phím cơ AKKO 3108 v2 One Piece – Luffy (Akko switch v2)', 56, 1699000.0000, 4, N'3108v2opla.jpg', CAST(N'2021-05-02' AS Date), 1, NULL, N'Model: 3108 (Fullsize, 108 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 442 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Bàn phím lấy cảm hứng từ nhân vật Luffy trong bộ truyện tranh/anime One Piece với màu chủ đạo là hồng/trắng/xanh
Loại switch: Akko (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3108v2sec', N'Bàn phím cơ AKKO 3108 v2 Steam Engine - (Cherry switch)', 12, 1499000.0000, 2, N'3108v2sec.jpg', CAST(N'2021-11-12' AS Date), 3, NULL, N'Model: 3108 (Fullsize, 108 keys)
Kết nối: USB Type C, có thể tháo rời – Kích thước: 440 x 140 x 40mm
Hỗ trợ NKRO
Keycap: PBT Dye-Subbed, OEM profile
Loại switch: Akko (Blue/Orange/Pink) switch v2
Hỗ trợ multimedia, macro và có thể khóa phím windows
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 1 dây cáp USB + 9 keycap tặng kèm')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'3108v2wtta', N'Bàn phím cơ AKKO 3108 v2 World Tour Tokyo - (Akko switch)', 20, 1599000.0000, 1, N'3108v2wtta.jpg', CAST(N'2021-03-01' AS Date), 1, NULL, N'Model: 3108 (Fullsize, 108 keys)
Kết nối: USB Type C, có thể tháo rời
Kích thước: 440 x 140 x 40mm
Hỗ trợ NKRO / Multimedia / Macro / Khóa phím windows
Keycap: PBT Dye-Subbed, OEM Profile
Loại switch: Akko switch (Blue/Orange/Pink) v2
Keycap custom lấy chủ đạo là màu hoa anh đào, núi Phú sĩ, cá chép và mèo may mắn (biểu tượng của Nhật Bản)
Phụ kiện: 1 sách hướng dẫn sử dụng + 1 keypuller + 1 cover che bụi + 06 keycap tặng kèm + 1 dây cáp USB')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'new v2', N'new v2', 12, 1232.0000, 1, N'photo1.jpg', CAST(N'2021-12-22' AS Date), 1, NULL, N'new v2new v2new v2new v2new v2new v2')
INSERT [dbo].[BanPhim] ([MaBanPhim], [TenBanPhim], [SoLuong], [GiaBan], [MaLoai], [Anh], [NgayNhap], [MaSwitch], [DanhGia], [Mota]) VALUES (N'new v4', N'new v4', 12, 123424.0000, 3, N'photo-3.jpg', CAST(N'2021-12-28' AS Date), 1, NULL, N'new v4new v4new v4new v4new v4')
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] ON 

INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10057, N'3084sg', 1, 10068, 2)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10063, N'3108v2wtta', 1, 10071, 1)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10064, N'3084sposc', 1, 10071, 1)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10065, N'3098v2wtta', 1, 10071, 1)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10066, N'3084as', 1, 10072, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10067, N'3084sposc', 1, 10072, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10068, N'3084sg', 1, 10072, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10069, N'3108v2wtta', 1, 10072, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10070, N'3084sposc', 2, 10073, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10071, N'3108v2wtta', 2, 10073, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10072, N'3087v2sea', 2, 10073, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10073, N'new ', 2, 10073, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10074, N'3084sg', 2, 10073, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10075, N'3084sposc', 3, 10074, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10076, N'3084as', 3, 10074, 0)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10077, N'3098v2wtta', 1, 10075, 1)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHD], [MaBanPhim], [SoLuongMua], [MaHoaDon], [TrangThai]) VALUES (10078, N'3084sposc', 1, 10075, 1)
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] OFF
INSERT [dbo].[DangNhap] ([TenDangNhap], [MatKhau], [Quyen]) VALUES (N'admin', N'202cb962ac59075b964b07152d234b70', 1)
SET IDENTITY_INSERT [dbo].[HoaDon] ON 

INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10068, 1, CAST(N'2021-12-20T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10071, 1, CAST(N'2021-12-20T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10072, 1, CAST(N'2021-12-20T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10073, 2, CAST(N'2021-12-21T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10074, 2, CAST(N'2021-12-21T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[HoaDon] ([MaHoaDon], [MaKhachHang], [NgayDatMua], [DaMua]) VALUES (10075, 1, CAST(N'2021-12-21T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[HoaDon] OFF
SET IDENTITY_INSERT [dbo].[KhachHang] ON 

INSERT [dbo].[KhachHang] ([MaKhachHang], [HoTen], [DiaChi], [SoDienThoai], [Email], [TenDangNhap], [MatKhau]) VALUES (1, N'new ', N'new', N'123', N'new@gmail.com', N'new', N'202cb962ac59075b964b07152d234b70')
INSERT [dbo].[KhachHang] ([MaKhachHang], [HoTen], [DiaChi], [SoDienThoai], [Email], [TenDangNhap], [MatKhau]) VALUES (2, N'NguyenVanA', N'Huế', N'09203122', N'nguyenvana@gmail.com', N'nguyenvana', N'202cb962ac59075b964b07152d234b70')
SET IDENTITY_INSERT [dbo].[KhachHang] OFF
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (1, N'World Tour Tokyo')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (2, N'Steam Engine')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (3, N'Silent')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (4, N'One Piece')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (5, N'Multi-modes')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (6, N'Matcha Red Bean')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (7, N'Ocean')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (8, N' Midnight')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (9, N'Led RGB')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (10, N'Horizon')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (11, N'BiliBili')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (12, N'Dragon Ball Super')
INSERT [dbo].[Switch] ([MaSwitch], [TenSwitch]) VALUES (1, N'Cherry')
INSERT [dbo].[Switch] ([MaSwitch], [TenSwitch]) VALUES (2, N'Akko')
INSERT [dbo].[Switch] ([MaSwitch], [TenSwitch]) VALUES (3, N'Gateron')
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_KhachHang]    Script Date: 12/21/2021 7:19:21 PM ******/
ALTER TABLE [dbo].[KhachHang] ADD  CONSTRAINT [IX_KhachHang] UNIQUE NONCLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BanPhim]  WITH CHECK ADD  CONSTRAINT [FK_BanPhim_Loai] FOREIGN KEY([MaLoai])
REFERENCES [dbo].[Loai] ([MaLoai])
GO
ALTER TABLE [dbo].[BanPhim] CHECK CONSTRAINT [FK_BanPhim_Loai]
GO
ALTER TABLE [dbo].[BanPhim]  WITH CHECK ADD  CONSTRAINT [FK_BanPhim_Switch] FOREIGN KEY([MaSwitch])
REFERENCES [dbo].[Switch] ([MaSwitch])
GO
ALTER TABLE [dbo].[BanPhim] CHECK CONSTRAINT [FK_BanPhim_Switch]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_hoadon] FOREIGN KEY([MaHoaDon])
REFERENCES [dbo].[HoaDon] ([MaHoaDon])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_hoadon]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_hoadon_KhachHang] FOREIGN KEY([MaKhachHang])
REFERENCES [dbo].[KhachHang] ([MaKhachHang])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_hoadon_KhachHang]
GO
/****** Object:  StoredProcedure [dbo].[proc_Admin_Authenticate]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Admin_Authenticate]
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from DangNhap as t where t.TenDangNhap = @tendn and t.MatKhau = @pass and t.Quyen = 1

end
GO
/****** Object:  StoredProcedure [dbo].[proc_Admin_GetAllOrderHistory]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Admin_GetAllOrderHistory]
as
begin	
	set nocount on

	select s.Anh, TenBanPhim, c.SoLuongMua, h.NgayDatMua, c.TrangThai, c.MaHoaDon, c.MaBanPhim
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = h.MaHoaDon 
		join BanPhim as s on s.MaBanPhim = c.MaBanPhim
	where h.MaHoaDon = c.MaHoaDon
	order by h.NgayDatMua desc
	
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Admin_Register]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Admin_Register]
@tendn varchar(50),
@pass varchar(50),
@Result int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @Result = -3
				return;
			end
		if(exists(select * from DangNhap as t where t.TenDangNhap = @tendn))
			begin
				set @Result = -2
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @Result = -1
				return;
			end

		insert into DangNhap(TenDangNhap, MatKhau, Quyen)
		values (@tendn, @pass, 1);
		set @Result = 1;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Book_Delete]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Book_Delete]
@maSach nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete BanPhim 
	where(MaBanPhim = @maSach) or (exists(select * from loai where MaBanPhim = @maSach ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Book_Insert]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Book_Insert]
@masach nvarchar(50),
@tensach nvarchar(50),
@soluong bigint,
@gia bigint,
@maloai nvarchar(50), 
@anh nvarchar(50),
@NgayNhap nvarchar(50),
@maSwitch int,
@moTa ntext,
@Result int output

as
begin
	set nocount on
	
	if(@masach is null or @masach = N'' or @maloai is null or @tensach is null or @tensach = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from BanPhim where MaBanPhim  = @masach)
		begin
			set @Result = -1
			return;
		end

	Insert into BanPhim(MaBanPhim, MaLoai, TenBanPhim, SoLuong, GiaBan, Anh, NgayNhap, MaSwitch, Mota)
			values(@masach, @maloai, @tensach,@soluong, @gia, @anh, @NgayNhap, @maSwitch, @moTa)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Book_Update]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Book_Update]
@masach nvarchar(50),
@tensach nvarchar(50),
@soluong bigint,
@gia bigint,
@maloai nvarchar(50), 
@anh nvarchar(50),
@NgayNhap nvarchar(50),
@maSwitch int,
@moTa ntext,
@Result bit output

as
begin
	set nocount on
	
	if(@masach is null or @masach = N'' or @maloai is null or @tensach is null or @tensach = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from BanPhim where MaBanPhim  <> @masach)
		begin
			set @Result = 0
			return;
		end

	Update BanPhim
	set MaLoai = @maloai,
		TenBanPhim = @tensach,
		SoLuong = @soluong,
		GiaBan = @gia,
		Anh = @anh,
		NgayNhap = @NgayNhap,
		MaSwitch = @maSwitch,
		MoTa = @moTa
	where MaBanPhim = @masach

	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_ChiTietHoaDon_Create]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_ChiTietHoaDon_Create]
@maSach varchar(50),
@soLuong int,
@maHoaDon bigint,
@trangThai int,
@StatusCode int = 0 output
as
begin

	if(not exists(select * from HoaDon as t where t.MaHoaDon = @maHoaDon))
		begin
			set @StatusCode = 0
			return;
		end

	set nocount on;
	insert into ChiTietHoaDon(MaBanPhim, SoLuongMua, MaHoaDon, TrangThai)
	select @maSach, @soLuong, @maHoaDon, @trangThai
	if(@@ROWCOUNT > 0)
		set @StatusCode = 1
		return;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_HoaDon_Create]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_HoaDon_Create]
@maKh bigint,
@ngayMua date,
@damua bit,
@maHoaDon int output
as
begin
	set nocount on;
	if(not exists(select * from KhachHang as t where t.MaKhachHang = @maKh))
		begin
			set @maHoaDon = -1
			return;
		end
	insert into HoaDon (MaKhachHang, NgayDatMua, DaMua) values(@maKh, @ngayMua, @damua)
	set @maHoaDon = @@IDENTITY;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_HoaDon_Order]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_HoaDon_Order]
@customerId int,
@quantity int
as
begin	
	set nocount on
	
	select h.MaHoaDon, h.NgayDatMua, count(c.SoLuongMua) as SoLuongMua, c.TrangThai
	from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon 
		INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim
	where h.MaKhachHang = @customerId and c.MaHoaDon = h.MaHoaDon
	group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai
	ORDER BY h.NgayDatMua DESC OFFSET 0 ROWS FETCH NEXT @quantity ROWS ONLY

end
GO
/****** Object:  StoredProcedure [dbo].[proc_HoaDon_OrderDetail]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_HoaDon_OrderDetail]
@customerId int,
@orderId int
as
begin	
	set nocount on

	select c.MaHoaDon, s.TenBanPhim, Anh, GiaBan, c.SoLuongMua
	from ChiTietHoaDon as c join BanPhim as s on c.MaBanPhim = s.MaBanPhim join hoadon as h
		on h.MaHoaDon = c.MaHoaDon
	where h.MaKhachHang = @customerId and h.MaHoaDon = @orderId and c.MaHoaDon = @orderId

end
GO
/****** Object:  StoredProcedure [dbo].[proc_HoaDon_OrderDetailHistory]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_HoaDon_OrderDetailHistory]
@customerId int,
@orderId int
as
begin	
	set nocount on

	select c.MaHoaDon, s.tensach, anh, tacgia, gia, c.SoLuongMua
	from ChiTietHoaDon as c join sach as s on c.MaSach = s.MaSach join hoadon as h
		on h.MaHoaDon = c.MaHoaDon
	where h.makh = @customerId and h.MaHoaDon = @orderId and c.MaHoaDon = @orderId

	
end
GO
/****** Object:  StoredProcedure [dbo].[proc_HoaDon_OrderHistory]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_HoaDon_OrderHistory]
@customerId int
as
begin	
	set nocount on

	select h.makh, h.MaHoaDon, count(h.MaHoaDon), c.SoLuongMua, s.gia
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = c.MaHoaDon 
		join sach as s on s.masach = c.MaSach

	where h.makh = @customerId 
	group by h.MaHoaDon, makh, c.SoLuongMua, s.gia
	having h.makh = @customerId	
	
end
GO
/****** Object:  StoredProcedure [dbo].[proc_KhachHang_Authenticate]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_KhachHang_Authenticate]
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from KhachHang as t where t.TenDangNhap = @tendn and t.MatKhau = @pass

end
GO
/****** Object:  StoredProcedure [dbo].[proc_KhachHang_GetInfo]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_KhachHang_GetInfo]
@customerId int
as
begin
	set nocount on;
	select * from KhachHang where MaKhachHang = @customerId

end
GO
/****** Object:  StoredProcedure [dbo].[proc_KhachHang_Register]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_KhachHang_Register]
@hoten nvarchar(255),
@tendn varchar(50),
@pass varchar(50),
@diachi nvarchar(455),
@email nvarchar(50),
@sodt nvarchar(15),
@makh int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @makh = -3
				return;
			end
		if(exists(select * from KhachHang as t where t.TenDangNhap = @tendn))
			begin
				set @makh = -5
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @makh = -2
				return;
			end
		if ([dbo].[fn_IsValidEmail](@email) = 0)
			begin
				set @makh = -1
				return;
			end
		if(@sodt = NULL or LEN(@sodt) < 1)
			begin
				set @makh = -4
				return;
			end

		insert into KhachHang(HoTen, TenDangNhap, MatKhau, Email, DiaChi, SoDienThoai)
		values (@hoten, @tendn, @pass,Lower( @email), @diachi, @sodt);
		set @makh = SCOPE_IDENTITY();
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Oder_searchOrderByOrderId]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[proc_Oder_searchOrderByOrderId]
@orderId int,
@quantity int
as
begin
	set nocount on
	select h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai, count(c.MaHoaDon) as SoLuongMua
	from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim
	Where h.MaHoaDon like '%' + @orderId + '%'
	group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai
	ORDER BY h.NgayDatMua desc OFFSET 0 ROWS FETCH NEXT @quantity ROWS ONLY
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Order_Cancel_Cus]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Order_Cancel_Cus]
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Order_Delete]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Order_Delete]
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Order_Tranforms_Cus]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Order_Tranforms_Cus]
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int

	UPDATE hoadon
	SET damua = 1
	WHERE MaHoaDon = @maHoaDon and damua = 0

	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon


	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Order_Transforms]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Order_Transforms]
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int
	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Product_Delete]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Product_Delete]
@maSach nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete BanPhim 
	where(MaBanPhim = @maSach) or (exists(select * from loai where MaBanPhim = @maSach ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Product_Insert]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Product_Insert]
@MaBanPhim nvarchar(50),
@TenBanPhim nvarchar(50),
@MaLoai int, 
@MaSwitch int, 
@GiaBan bigint,
@Anh nvarchar(50),
@Soluong bigint,
@NgayNhap date,
@MoTa ntext,
@Result bit output

as
begin
	set nocount on
	
	if(@MaBanPhim is null or @MaBanPhim = N'' or @MaLoai is null or @TenBanPhim is null or @TenBanPhim = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from BanPhim where MaBanPhim  = @MaBanPhim)
		begin
			set @Result = -1
			return;
		end

	Insert into BanPhim(MaBanPhim, TenBanPhim, MaLoai, MaSwitch, GiaBan, Anh, SoLuong, NgayNhap, Mota)
			values(@MaBanPhim, @TenBanPhim, @MaLoai,@MaSwitch, @GiaBan, @Anh, @Soluong, @NgayNhap,  @moTa)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_Product_Update]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_Product_Update]
@MaBanPhim nvarchar(50),
@TenBanPhim nvarchar(250),
@MaLoai int, 
@MaSwitch int, 
@GiaBan money,
@Anh nvarchar(50),
@Soluong bigint,
@NgayNhap date,
@MoTa ntext,
@Result bit output

as
begin
	set nocount on
	
	if(@MaBanPhim is null or @MaBanPhim = N'' or @MaLoai is null or @TenBanPhim is null or @TenBanPhim = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from BanPhim where MaBanPhim  <> @MaBanPhim)
		begin
			set @Result = 0
			return;
		end

	Update BanPhim
	set 
		TenBanPhim = @TenBanPhim,
		MaLoai = @maloai,
		MaSwitch = @MaSwitch,
		SoLuong = @Soluong,
		GiaBan = @GiaBan,
		Anh = @Anh,
		NgayNhap = @NgayNhap,
		MoTa = @MoTa
	where MaBanPhim = @MaBanPhim

	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfBooks_Delete]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfBooks_Delete]
@maLoai nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete loai 
	where(maloai = @maLoai) or (exists(select * from BanPhim where MaLoai = @maLoai ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfBooks_Insert]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfBooks_Insert]
@maLoai nvarchar(50),
@tenLoai nvarchar(250),
@Result int output

as
begin
	set nocount on
	
	if(@maLoai is null or @maLoai = N'' or @maloai is null or @tenLoai is null or @tenLoai = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from loai where maloai  = @maLoai)
		begin
			set @Result = -1
			return;
		end

	Insert into loai (maloai, tenloai) values(@maLoai, @tenLoai)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfBooks_Update]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfBooks_Update]
@maLoaiNew nvarchar(50),
@maLoaiOld nvarchar(50),
@tenLoai nvarchar(250),
@Result bit output

as
begin
	set nocount on
	
	if(@maLoaiOld is null or @maLoaiOld = N'' or @maLoaiNew is null or @maLoaiNew = N''
	 or @tenLoai is null or @tenLoai = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from BanPhim where MaLoai  <> @maLoaiOld)
		begin
			set @Result = 0
			return;
		end

	Update loai
	set maloai = @maLoaiNew,
		tenloai = @tenLoai
	where maloai = @maLoaiOld


	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfProducts_Delete]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfProducts_Delete]
@maLoai int,
@Result bit output
as
begin
	set nocount on
	
	Delete loai 
	where(maloai = @maLoai) or (exists(select * from BanPhim where MaLoai = @maLoai ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfProducts_Insert]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfProducts_Insert]
@maLoai int,
@tenLoai nvarchar(250),
@Result int output

as
begin
	set nocount on
	
	if exists(select * from loai where maloai  = @maLoai)
		begin
			set @Result = -1
			return;
		end

	Insert into loai (maloai, tenloai) values(@maLoai, @tenLoai)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
GO
/****** Object:  StoredProcedure [dbo].[proc_TypesOfProducts_Update]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[proc_TypesOfProducts_Update]
@maLoaiNew int,
@maLoaiOld int,
@tenLoai nvarchar(250),
@Result bit output

as
begin
	set nocount on
	
	if not exists(select * from BanPhim where MaLoai  <> @maLoaiOld)
		begin
			set @Result = 0
			return;
		end

	Update loai
	set maloai = @maLoaiNew,
		tenloai = @tenLoai
	where maloai = @maLoaiOld


	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
GO
/****** Object:  StoredProcedure [dbo].[TimKiem]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[TimKiem](@bien nvarchar(5))
as
  select * from sach where maloai =@bien
GO
/****** Object:  StoredProcedure [dbo].[TimMaLoai]    Script Date: 12/21/2021 7:19:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[TimMaLoai]( @maloai nvarchar(50))
as
select * from sach where maloai=@maloai
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 95
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 125
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'abc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'abc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'HtSach'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'HtSach'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 95
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 0
               Left = 588
               Bottom = 119
               Right = 748
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'rrr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'rrr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'rrr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[37] 4[37] 2[1] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 2505
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'tk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'tk'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 95
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 96
               Left = 434
               Bottom = 215
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vabcd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vabcd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vabcd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[30] 4[37] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sach"
            Begin Extent = 
               Top = 6
               Left = 240
               Bottom = 125
               Right = 400
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 0
               Left = 445
               Bottom = 119
               Right = 605
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 4
               Left = 31
               Bottom = 123
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vhoadon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vhoadon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 95
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 5
               Left = 580
               Bottom = 124
               Right = 740
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vqq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vqq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vqq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -172
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 125
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KhachHang"
            Begin Extent = 
               Top = 114
               Left = 236
               Bottom = 233
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 215
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 126
               Left = 434
               Bottom = 245
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTamBay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTamBay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTamBay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[14] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KhachHang"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 125
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 215
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 245
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[23] 4[43] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[19] 4[43] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "loai"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sach"
            Begin Extent = 
               Top = 0
               Left = 244
               Bottom = 119
               Right = 404
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KhachHang"
            Begin Extent = 
               Top = 16
               Left = 547
               Bottom = 135
               Right = 707
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 96
               Left = 38
               Bottom = 215
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 97
               Left = 439
               Bottom = 216
               Right = 599
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Ta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTKabc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'ble = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTKabc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTKabc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[30] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sach"
            Begin Extent = 
               Top = 0
               Left = 206
               Bottom = 119
               Right = 366
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "loai"
            Begin Extent = 
               Top = 2
               Left = 535
               Bottom = 91
               Right = 695
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hoadon"
            Begin Extent = 
               Top = 47
               Left = 375
               Bottom = 166
               Right = 535
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ChiTietHoaDon"
            Begin Extent = 
               Top = 36
               Left = 0
               Bottom = 155
               Right = 160
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KhachHang"
            Begin Extent = 
               Top = 96
               Left = 573
               Bottom = 215
               Right = 733
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtktest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtktest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vtktest'
GO
