package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.SwitchBean;
import model.bean.BanPhimBean;
import model.bo.LoaiBo;
import model.bo.SwitchBo;
import model.bo.BanPhimBo;

/**
 * Servlet implementation class ProductDetail
 */
@WebServlet("/product-details")
public class ProductDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String keyBoardId = request.getParameter("keyboardId");
			if (keyBoardId != null) {
				BanPhimBo sbo = new BanPhimBo();
				BanPhimBean sbean = sbo.getKeyBoardById(keyBoardId);

				SwitchBo swo = new SwitchBo();

				SwitchBean swb = swo.getSwitchById(sbean.getMaSwitch());

				request.setAttribute("keyboardDetailSwitch", swb);
				request.setAttribute("keyboardDetail", sbean);

				ArrayList<BanPhimBean> listBanPhimRelated = sbo.getProductByQuantity(3);
				request.setAttribute("keyboardsRelated", listBanPhimRelated);

				RequestDispatcher rd = request.getRequestDispatcher("views/keyboard_detail.jsp");
				rd.forward(request, response);
				return;

			}
			RequestDispatcher rd = request.getRequestDispatcher("views/mouse_detail.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
