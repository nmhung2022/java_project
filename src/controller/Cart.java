package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.LoaiBean;
import model.bean.BanPhimBean;
import model.bo.GioHangBo;
import model.bo.KhachHangBo;
import model.bo.LoaiBo;
import model.bo.BanPhimBo;

/**
 * Servlet implementation class CartController
 */
@WebServlet("/cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			HttpSession session = request.getSession();

			String keyboardId = request.getParameter("keyboardId");
			int quantity = 1;
			if (request.getParameter("quantity") != null)
				quantity = Integer.parseInt(request.getParameter("quantity"));

			// Xoá
			String delKeyboardId = request.getParameter("delKeyboardId");

			// Cập nhật
			String upQuantity = request.getParameter("upQuantity");
			String upKeyboardId = request.getParameter("upKeyboardId");

			BanPhimBo sbo = new BanPhimBo();

			BanPhimBean bpBean = sbo.getKeyBoardById(keyboardId);

			if (bpBean != null) {
				GioHangBo ghbo = new GioHangBo();
				if (session.getAttribute("giohang") == null) {
					session.setAttribute("giohang", ghbo);
					session.setAttribute("listgiohang", ghbo.ds);
				}
				ghbo = (GioHangBo) session.getAttribute("giohang");
				ghbo.addToCart(bpBean.getMaBanPhim(), bpBean.getTenBanPhim(), bpBean.getAnh(), bpBean.getGiaBan(),
						quantity);

				session.setAttribute("giohang", ghbo);

				session.setAttribute("listgiohang", ghbo.ds);
			}

			else if (delKeyboardId != null) {
				GioHangBo ghbo = new GioHangBo();
				ghbo = (GioHangBo) session.getAttribute("giohang");
				ghbo.removeById(delKeyboardId);
				if (ghbo.ds.size() == 0) {
					session.removeAttribute("giohang");
					session.removeAttribute("listgiohang");
				} else {
					session.setAttribute("giohang", ghbo);
					session.setAttribute("listgiohang", ghbo.ds);
				}
			}

			else if (upQuantity != null && upKeyboardId != null) {
				int soLuong = Integer.parseInt(upQuantity);

				GioHangBo ghbo = new GioHangBo();
				ghbo = (GioHangBo) session.getAttribute("giohang");

				ghbo.updateQuantity(upKeyboardId, soLuong);
				session.setAttribute("giohang", ghbo);
				session.setAttribute("listgiohang", ghbo.ds);
			}

			if (request.getAttribute("paidSuccess") != null) {
				session.removeAttribute("giohang");
				session.removeAttribute("listgiohang");
				request.setAttribute("paidSuccess", true);
			}

			RequestDispatcher rd = request.getRequestDispatcher("views/cart.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
