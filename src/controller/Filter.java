package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BanPhimBean;
import model.bean.HoaDonBean;
import model.bo.BanPhimBo;
import model.bo.HoaDonBo;

/**
 * Servlet implementation class Filter
 */
@WebServlet("/filter")
public class Filter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Filter() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {

			BanPhimBo sbo = new BanPhimBo();
			HoaDonBo hbo = new HoaDonBo();

			String filterName = request.getParameter("filterName");
			String quantityProduct = request.getParameter("quantityProductShow");
			String quantityOrder = request.getParameter("quanityOrderShow");

			String quanityOrderAllShow = request.getParameter("quanityOrderAllShow");

			String customerId = request.getParameter("customerId");

			ArrayList<BanPhimBean> dskeyboard = null;

			ArrayList<HoaDonBean> dsorder = null;

			if (filterName != null) {
				if (filterName.equals("new-product")) {
//					System.err.println("new");
					dskeyboard = sbo.filterNewProduct(Integer.parseInt(quantityProduct));
				} else if (filterName.equals("price-asc")) {
//					System.err.println("asc");
					dskeyboard = sbo.filterPriceASC(Integer.parseInt(quantityProduct));
				} else {
//					System.err.println("desc");
					dskeyboard = sbo.filterPriceDESC(Integer.parseInt(quantityProduct));
				}

				DecimalFormat formatter = new DecimalFormat("###,###,###");
				PrintWriter out = response.getWriter();

				for (BanPhimBean kb : dskeyboard) {
					out.print("<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-6\">\r\n"
							+ "									<div class=\"single-popular-items mb-50 text-center\">\r\n"
							+ "										<div class=\"popular-img\">\r\n"
							+ "											<img src=\"assets/img/gallery/" + kb.getAnh()
							+ "\" alt=\"\">\r\n"
							+ "											<div class=\"img-cap\">\r\n"
							+ "												<span onclick=\"addToCart('"
							+ kb.getMaBanPhim() + "', 1)\">Thêm\r\n"
							+ "													vào giỏ hàng</span>\r\n"
							+ "											</div>\r\n"
							+ "											<div class=\"favorit-items\">\r\n"
							+ "												<span class=\"flaticon-heart\"></span>\r\n"
							+ "											</div>\r\n"
							+ "										</div>\r\n"
							+ "										<div class=\"popular-caption\">\r\n"
							+ "											<h3>\r\n"
							+ "												<a href=\"product-details?keyboardId="
							+ kb.getMaBanPhim() + "\">\r\n" + "													"
							+ kb.getTenBanPhim() + "\r\n" + "												</a>\r\n"
							+ "											</h3>\r\n"
							+ "											<span>" + formatter.format(kb.getGiaBan())
							+ "₫\r\n" + "											</span>\r\n"
							+ "										</div>\r\n"
							+ "									</div>\r\n" + "								</div>");
				}
			}

			if (quantityOrder != null && customerId != null && quanityOrderAllShow == null) {
				dsorder = hbo.getOrderByQuantity(Integer.parseInt(customerId), Integer.parseInt(quantityOrder));

				PrintWriter out = response.getWriter();

				for (HoaDonBean od : dsorder) {

					out.print("<tr>");
					out.print("<td>" + od.getMaHoaDon() + "</td>");
					out.print("<td>" + od.getThoiGianMua() + "</td>");
					out.print("<td>" + od.getSoLuongMua() + "</td>");

					if (od.getTrangThai() == 0) {
						out.print("<td>Đang chờ xác nhận từ người bán</td>");
					} else if (od.getTrangThai() == 1) {
						out.print("<td>Đang giao hàng</td>");
					} else if (od.getTrangThai() == 2) {

						out.print("<td>Đã thanh toán</td>");
					}
					if (od.getTrangThai() == 0) {
						out.print("<td>\r\n"
								+ "										<div class=\"col d-flex align-items-center\">\r\n"
								+ "											<a class=\"btn btn-danger min-width\"\r\n"
								+ "												href=\"delete-order-cus?mhd="
								+ od.getMaHoaDon() + "\">Huỷ đặt hàng</a>\r\n"
								+ "										</div>\r\n"
								+ "									</td>");
					} else if (od.getTrangThai() == 1) {
						out.print("<td><div class=\"col d-flex align-items-center\">\r\n"
								+ "											<a class=\"btn btn-success min-width ml-2\"\r\n"
								+ "												href=\"update-order-cus?mhd="
								+ od.getMaHoaDon() + "&tt=" + od.getTrangThai() + "\">Đã nhận hàng</a>\r\n"
								+ "										</div></td>");

					} else if (od.getTrangThai() == 2) {
						out.print("<td><div class=\"col d-flex align-items-center\">"
								+ "<a class=\"btn btn-success min-width ml-2 \" " + "href=\"delete-order-cus?mhd="
								+ od.getMaHoaDon() + "\">Đánh giá</a></div></td>");
					}
					out.print("<td><a href=\"order-detail?mhd="+ od.getMaHoaDon()
							+ "\" class=\"btn btn-primary\">Xem chi tiết</a></td>");
					out.print("</tr>");

				}
			} else if (quanityOrderAllShow != null) {
				dsorder = hbo.getOrderAllByQuantity(Integer.parseInt(quanityOrderAllShow));
				PrintWriter out = response.getWriter();
				System.err.println(quanityOrderAllShow);
				for (HoaDonBean od : dsorder) {

					out.print("<tr>");
					out.print("<td>" + od.getMaHoaDon() + "</td>");
					out.print("<td>" + od.getThoiGianMua() + "</td>");
					out.print("<td>" + od.getSoLuongMua() + "</td>");

					if (od.getTrangThai() == 0) {
						out.print("<td>Chưa thanh toán</td>");
					} else if (od.getTrangThai() == 1) {
						out.print("<td>Đang giao hàng</td>");
					} else if (od.getTrangThai() == 2) {

						out.print("<td>Đã thanh toán</td>");
					}
					if (od.getTrangThai() == 0) {
						out.print("<td>\r\n"
								+ "																	<div class=\"col \">\r\n"
								+ "																		<a class=\"genric-btn danger circle\"\r\n"
								+ "																			href=\"delete-order?mhd="
								+ od.getMaHoaDon() + "\">Huỷ đơn</a> <a class=\"genric-btn success circle ml-3\"\r\n"
								+ "																			href=\"update-order?mhd="
								+ od.getMaHoaDon() + "&tt=" + od.getTrangThai() + "\">Chấp nhận đơn</a>\r\n</div>\r\n"
								+ "																</td>");
					} else if (od.getTrangThai() == 1) {
						out.print("<td>\r\n"
								+ "																	<div class=\"col \">\r\n"
								+ "																		<a class=\"genric-btn danger circle\"\r\n"
								+ "																			href=\"delete-order?mhd="
								+ od.getMaHoaDon()
								+ "\">Thay đổi thông tin</a> <a class=\"genric-btn danger circle\" href=\"delete-order?mhd="
								+ od.getMaHoaDon() + "\">Đã nhận tiền</a> </div>\r\n"
								+ "																</td>");

					} else {
						out.print("<td><a class=\"genric-btn danger circle\" href=\"delete-order?mhd="
								+ od.getMaHoaDon() + "\">Xoá</a></td>");
					}
					out.print("<td><a class=\"genric-btn info circle\" href=\"order-history?mhd=" + od.getMaHoaDon()
							+ "&mkh=" + od.getMaKhachHang() + "\">Xem\r\n"
							+ "																chi tiết</a></td>");
					
					out.print("</tr>");
				}
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
