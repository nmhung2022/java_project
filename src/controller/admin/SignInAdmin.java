package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.AdminBean;
import model.bean.KhachHangBean;
import model.bo.AdminBo;
import model.bo.KhachHangBo;
import utils.MD5;

/**
 * Servlet implementation class SignInAdmin
 */
@WebServlet("/signin-admin")
public class SignInAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignInAdmin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String username = request.getParameter("username");
			String password = request.getParameter("password");

			HttpSession session = request.getSession();
			AdminBean adBean = new AdminBean();
			AdminBo adBo = new AdminBo();

			String usernameLowerCase = username.toLowerCase();

			MD5 md = new MD5();

			String hassPass = md.getHashPass(password);

			adBean = adBo.kiemTraDN(usernameLowerCase, hassPass);

			if (adBean != null) {// thông tin tài khoản chính xác
				if (session.getAttribute("auth-admin") == null) {
					session.setAttribute("auth-admin", (AdminBean) adBean);
					session.setAttribute("flag_auth", null);
				}
			} else {
				session.setAttribute("auth-admin", null);
				request.setAttribute("isSignin", false);
				RequestDispatcher rd = request.getRequestDispatcher("views/admin/signin.jsp");
				rd.forward(request, response);
				return;
			}

			RequestDispatcher rd = request.getRequestDispatcher("dashboard");
			rd.forward(request, response);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
