package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.AdminBean;
import model.bo.AdminBo;
import utils.MD5;

/**
 * Servlet implementation class SignUpAdmin
 */
@WebServlet("/signup-admin")
public class SignUpAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignUpAdmin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String username = request.getParameter("username");
			String password = request.getParameter("password");

			HttpSession session = request.getSession();
			AdminBean admin = (AdminBean) session.getAttribute("auth-admin");

			if (admin != null) {
				if (username != null && password != null) {
					AdminBo adBo = new AdminBo();

					MD5 md = new MD5();

					String hassPass = md.getHashPass(password);

					int isNewAdmin = adBo.kiemTraDK(username, hassPass);

					System.err.println("1");

					if (isNewAdmin > 0) {
						RequestDispatcher rd = request.getRequestDispatcher("signin-admin");
						rd.forward(request, response);
						return;
					}

					System.err.println("2");
					AdminBean adBean = new AdminBean(username, password, true);
					request.setAttribute("exsitsAdmin", adBean);
					RequestDispatcher rd = request.getRequestDispatcher("views/admin/signup.jsp");
					rd.forward(request, response);
					return;
				}
				request.setAttribute("isSignin", true);
				RequestDispatcher rd = request.getRequestDispatcher("views/admin/signup.jsp");
				rd.forward(request, response);
				return;

			}
			RequestDispatcher rd = request.getRequestDispatcher("dashboard");
			rd.forward(request, response);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
