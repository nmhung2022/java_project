package controller.admin;

import java.io.IOException;

import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.SwitchBean;
import model.bean.BanPhimBean;
import model.bo.BanPhimBo;
import model.bo.LoaiBo;
import model.bo.SwitchBo;

/**
 * Servlet implementation class AddSach
 */

@MultipartConfig
@WebServlet("/add-or-edit")
public class AddOrEditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddOrEditProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {

			BanPhimBo bpbo = new BanPhimBo();
			SwitchBo swbo = new SwitchBo();
			LoaiBo lbo = new LoaiBo();

			ArrayList<LoaiBean> dsloai = lbo.getLoai();
			ArrayList<SwitchBean> dsSwitch = swbo.getSwitch();

			request.setAttribute("dsLoai", dsloai);
			request.setAttribute("dsSwitch", dsSwitch);

			String maBanPhim = request.getParameter("keyboardId");
			// first time
			if (maBanPhim != null) {

				BanPhimBean keyboardEdit = bpbo.getKeyBoardById(maBanPhim);

				request.setAttribute("keyboardEdit", keyboardEdit);
				request.setAttribute("keyboardEditNgayNhap", keyboardEdit.getNgayNhap());

				RequestDispatcher rd = request.getRequestDispatcher("views/admin/edit_product.jsp");

				rd.forward(request, response);
				return;
			}

			RequestDispatcher rd = request.getRequestDispatcher("views/admin/add_product.jsp");
			rd.forward(request, response);

//			if (isAdd != null) {
//				Part part = request.getPart("anh");

//				String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
//				if (!fileName.equals("")) {
////						   String realPath = "D:\\Development\\JavaProject\\JavaCourse\\WebContent\\image_sach";
//					String realPath = request.getServletContext().getRealPath("image_sach");
//					if (!Files.exists(Path.of(realPath))) {
//						Files.createDirectories(Path.of(realPath));
//					}
//					part.write(realPath + "/" + fileName);
//					anh = "image_sach/" + fileName;
//				} else {
//					anh = anhUpdate;
//				}
//
//				AdminBo adbo = new AdminBo();

//				int isEditSuccess = adbo.kiemTraThem(maSach, tenSach, tacGia, giaBan, anh, maLoai);
//				System.out.println(isEditSuccess);
//				if (isEditSuccess == -1) {
//					BanPhimBean existsMaSach = new BanPhimBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
//					request.setAttribute("existsMaSach", existsMaSach);
//					RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-sach.jsp");
//					rd.forward(request, response);
//					return;
//				} else {
//					BanPhimBean newSach = new BanPhimBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
//					request.setAttribute("sachNew", newSach);
//					RequestDispatcher rd = request.getRequestDispatcher("dashboard");
//					rd.forward(request, response);
//					return;
//				}

//			}

//			if (maBanPhim != null && tenBanPhim != null && maLoai != null) {
//				Part part = request.getPart("anh");
//
//				String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
//				if (!fileName.equals("")) {
////		   String realPath = "D:\\Development\\JavaProject\\JavaCourse\\WebContent\\image_sach";
//					String realPath = request.getServletContext().getRealPath("gallerry");
//					if (!Files.exists(Path.of(realPath))) {
//						Files.createDirectories(Path.of(realPath));
//					}
//					part.write(realPath + "/" + fileName);
//					anh = "assest/img/gallery" + fileName;
//				} else {
//					anh = anhUpdate;
//				}
//
//				AdminBo adbo = new AdminBo();
//
//				boolean isEditSuccess = adbo.kiemTraCN(maBanPhim, tenBanPhim, Integer.parseInt(maLoai),
//						Integer.parseInt(maSwitch), giaBan, anhUpdate, Long.parseLong(soLuong), ngayNhap, moTa);
//
//				System.out.println(isEditSuccess);
//				if (isEditSuccess == -1) {
////					BanPhimBean existsMaSach = new BanPhimBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
////					request.setAttribute("existsMaSach", existsMaSach);
//					RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-sach.jsp");
//					rd.forward(request, response);
//					return;
//				} else {
////					BanPhimBean newSach = new BanPhimBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);
////					request.setAttribute("sachNew", newSach);
//					RequestDispatcher rd = request.getRequestDispatcher("dashboard");
//					rd.forward(request, response);
//					return;
//				}
//
//			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
