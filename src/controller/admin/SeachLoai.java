package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.BanPhimBean;
import model.bo.LoaiBo;
import model.bo.BanPhimBo;

/**
 * Servlet implementation class SeachLoai
 */
@WebServlet("/seachloai")
public class SeachLoai extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SeachLoai() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			LoaiBo lbo = new LoaiBo();

			ArrayList<LoaiBean> dsloai = lbo.getLoai();

			String key = request.getParameter("txtLoai");

			if (key != null)
				dsloai = lbo.timMaLoaiById(Integer.parseInt(key));

			request.setAttribute("dsloai", dsloai);
			request.setAttribute("timkiemloai", key);

			RequestDispatcher rd = request.getRequestDispatcher("views/admin/manage_loai.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
