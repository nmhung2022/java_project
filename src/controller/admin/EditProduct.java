package controller.admin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.bean.LoaiBean;
import model.bean.SwitchBean;
import model.bean.BanPhimBean;
import model.bo.AdminBo;
import model.bo.LoaiBo;
import model.bo.SwitchBo;
import model.dao.BanPhimDao;

/**
 * Servlet implementation class ManageSach
 */
@MultipartConfig
@WebServlet("/edit-product")
public class EditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {

			String maBanPhim = request.getParameter("maBanPhim");
			String tenBanPhim = request.getParameter("tenBanPhim");
			String maLoai = request.getParameter("maLoai");
			String maSwitch = request.getParameter("maSwitch");
			String gia = request.getParameter("giaBan");
			String anhUpdate = request.getParameter("anhUpdate");
			String soLuong = request.getParameter("soLuong");
			String ngayNhap = request.getParameter("ngayNhap");
			String moTa = request.getParameter("moTa");

			String anh = null;
			Long giaBan = null;

			if (gia != null)
				giaBan = Long.parseLong(gia);

			AdminBo adbo = new AdminBo();

			Part part = request.getPart("anh");

			String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();

			if (!fileName.equals("")) {
				String realPath = request.getServletContext().getRealPath("assets/img/gallery");
				if (!Files.exists(Path.of(realPath))) {
					Files.createDirectories(Path.of(realPath));
				}
				part.write(realPath + "/" + fileName);
				anh = fileName;
			} else {
				anh = anhUpdate;
			}

			System.err.println(anh);

			boolean isEditSuccess = adbo.kiemTraCN(maBanPhim, tenBanPhim, Integer.parseInt(maLoai),
					Integer.parseInt(maSwitch), giaBan, anh, Long.parseLong(soLuong), ngayNhap, moTa);

			if (isEditSuccess == true) {
				BanPhimBean sb = new BanPhimBean(maBanPhim, tenBanPhim, Integer.parseInt(maLoai),
						Integer.parseInt(maSwitch), giaBan, anhUpdate, Long.parseLong(soLuong), ngayNhap, moTa);
				request.setAttribute("sachEdit", sb);

				RequestDispatcher rd = request.getRequestDispatcher("dashboard");
				rd.forward(request, response);
				return;
			}
			LoaiBo lbo = new LoaiBo();
			SwitchBo swbo = new SwitchBo();

			ArrayList<LoaiBean> dsloai = lbo.getLoai();
			request.setAttribute("dsLoai", dsloai);

			ArrayList<SwitchBean> dsSwitch = swbo.getSwitch();
			request.setAttribute("dsSwitch", dsSwitch);

			RequestDispatcher rd = request.getRequestDispatcher("views/admin/edit-sach.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
