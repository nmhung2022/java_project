package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bo.AdminBo;

/**
 * Servlet implementation class AddLoai
 */
@WebServlet("/add-loai")
public class AddLoai extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddLoai() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			String maLoaiNew = request.getParameter("ml-new");
			String tenLoai = request.getParameter("tenLoai");

			if (maLoaiNew != null && tenLoai != null) {
				AdminBo adbo = new AdminBo();
				int isEditSuccess = adbo.kiemTraTL(Integer.parseInt(maLoaiNew), tenLoai);
				System.err.println("haha : " + isEditSuccess);
				if (isEditSuccess < 0) {
					LoaiBean existsMaLoai = new LoaiBean(Integer.parseInt(maLoaiNew), tenLoai);
					request.setAttribute("existsMaLoai", existsMaLoai);
					RequestDispatcher rd = request.getRequestDispatcher("views/admin/add_or_edit_loai.jsp");
					rd.forward(request, response);
					return;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
