package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BanPhimBean;
import model.bean.LoaiBean;
import model.bean.SwitchBean;
import model.bo.AdminBo;
import model.bo.BanPhimBo;
import model.bo.LoaiBo;
import model.bo.SwitchBo;

/**
 * Servlet implementation class AddOrEditLoai
 */
@WebServlet("/add-or-edit-loai")
public class AddOrEditLoai extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddOrEditLoai() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {

			String maLoai = request.getParameter("maLoai");

			if (maLoai == null) {
				RequestDispatcher rd = request.getRequestDispatcher("views/admin/add_or_edit_loai.jsp");
				rd.forward(request, response);
				return;
			}
			LoaiBo lb = new LoaiBo();
			LoaiBean loai = lb.getLoaiById(Integer.parseInt(maLoai));
			request.setAttribute("loai", loai);
			RequestDispatcher rd = request.getRequestDispatcher("views/admin/add_or_edit_loai.jsp");
			rd.forward(request, response);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

};