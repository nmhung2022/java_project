package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.KhachHangBean;
import model.bo.AdminBo;
import model.bo.HoaDonChiTietBo;
import model.bo.KhachHangBo;

/**
 * Servlet implementation class OrderHistory
 */
@WebServlet("/order-history")
public class OrderHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OrderHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			String maHoaDon = request.getParameter("mhd");

			String maKhachHang = request.getParameter("mkh");

			if (maHoaDon != null && maKhachHang != null) {
				HoaDonChiTietBo hdct = new HoaDonChiTietBo();

				HoaDonChiTietBo orderDetailHistory = hdct.getOrderDetail(Integer.parseInt(maKhachHang), maHoaDon);

				request.setAttribute("listOrderDetailHistory", orderDetailHistory.ds);
				request.setAttribute("orderDetailHistory", orderDetailHistory);

				KhachHangBo khbo = new KhachHangBo();
				KhachHangBean khbean = khbo.getInfoCustomer(maKhachHang);
				request.setAttribute("customer", khbean);

				RequestDispatcher rd = request.getRequestDispatcher("views/admin/order-history.jsp");
				rd.forward(request, response);
				return;
			}
			request.setAttribute("isSignin", false);
			RequestDispatcher rd = request.getRequestDispatcher("home");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
