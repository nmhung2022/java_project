package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.BanPhimBean;
import model.bean.HoaDonBean;
import model.bo.LoaiBo;
import model.bo.BanPhimBo;
import model.bo.HoaDonBo;

/**
 * Servlet implementation class Search
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Search() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			String txtSearch = request.getParameter("txtSearch");
			String isDashboard = request.getParameter("isDashboard");
			String nameProductOrder = request.getParameter("nameProductOrder");
			String quantityProduct = request.getParameter("quantityProductShow");
			

			ArrayList<BanPhimBean> dsKeyBoard = new ArrayList<BanPhimBean>();

			ArrayList<HoaDonBean> dsHoaDon = null;
			if (txtSearch != null && quantityProduct != null) {
				if (isDashboard == null) {
					BanPhimBo sbo = new BanPhimBo();
					dsKeyBoard = sbo.searchByNameWithQuantity(txtSearch, Integer.parseInt(quantityProduct));
					DecimalFormat formatter = new DecimalFormat("###,###,###");
					PrintWriter out = response.getWriter();

					for (BanPhimBean kb : dsKeyBoard) {
						out.print("<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-6\">\r\n"
								+ "									<div class=\"single-popular-items mb-50 text-center\">\r\n"
								+ "										<div class=\"popular-img\">\r\n"
								+ "											<img src=\"assets/img/gallery/" + kb.getAnh() + "\" alt=\"\">\r\n"
								+ "											<div class=\"img-cap\">\r\n"
								+ "												<span onclick=\"addToCart('" + kb.getMaBanPhim() + "', 1)\">Thêm\r\n"
								+ "													vào giỏ hàng</span>\r\n"
								+ "											</div>\r\n"
								+ "											<div class=\"favorit-items\">\r\n"
								+ "												<span class=\"flaticon-heart\"></span>\r\n"
								+ "											</div>\r\n"
								+ "										</div>\r\n"
								+ "										<div class=\"popular-caption\">\r\n"
								+ "											<h3>\r\n"
								+ "												<a href=\"product-details?keyboardId=" + kb.getMaBanPhim() + "\">\r\n"
								+ "													" + kb.getTenBanPhim() + "\r\n"
								+ "												</a>\r\n"
								+ "											</h3>\r\n"
								+ "											<span>" + formatter.format(kb.getGiaBan()) + "₫\r\n"
								+ "											</span>\r\n"
								+ "										</div>\r\n"
								+ "									</div>\r\n"
								+ "								</div>");
					}
				} else {
					BanPhimBo sbo = new BanPhimBo();
					dsKeyBoard = sbo.searchByNameWithQuantity(txtSearch, Integer.parseInt(quantityProduct));
					DecimalFormat formatter = new DecimalFormat("###,###,###");
					PrintWriter out = response.getWriter();
					for (BanPhimBean kb : dsKeyBoard) {
						out.print("<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-6\">\r\n"
								+ "									<div class=\"single-popular-items mb-50 text-center\">\r\n"
								+ "										<div class=\"popular-img\">\r\n"
								+ "											<img src=\"assets/img/gallery/"
								+ kb.getAnh() + "\">\r\n"
								+ "											<div class=\"img-cap two\">\r\n"
								+ "												<span><a\r\n"
								+ "													href=\"add-or-edit?keyboardId="
								+ kb.getMaBanPhim() + "\">Chỉnh\r\n"
								+ "														sửa</a></span><span> <a\r\n"
								+ "													href=\"delete-product?ms="
								+ kb.getMaBanPhim() + ">Xoá</a></span>\r\n"
								+ "											</div>\r\n"
								+ "											<div class=\"favorit-items\">\r\n"
								+ "												<span class=\"flaticon-heart\"></span>\r\n"
								+ "											</div>\r\n"
								+ "										</div>\r\n"
								+ "										<div class=\"popular-caption\">\r\n"
								+ "											<h3>\r\n"
								+ "											" + kb.getTenBanPhim() + "\r\n"
								+ "											</h3>\r\n"
								+ "											<span>" + formatter.format(kb.getGiaBan())
								+ "</span>\r\n" + "										</div>\r\n"
								+ "									</div>\r\n"
								+ "								</div>");
					}
				}
			} else if (nameProductOrder != null && quantityProduct != null) {
				HoaDonBo hbo = new HoaDonBo();
				dsHoaDon = hbo.searchByNameWithQuantity(txtSearch, Integer.parseInt(quantityProduct));
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
