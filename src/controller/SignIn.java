package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.KhachHangBean;
import model.bo.KhachHangBo;
import utils.MD5;
import utils.VerifyUtils;

/**
 * Servlet implementation class dangNhapController
 */
@WebServlet("/signin")
public class SignIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignIn() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			HttpSession session = request.getSession();
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			boolean valid = true;

			if (session.getAttribute("auth") == null) {
				if (username != null && password != null) {
					KhachHangBean kh = new KhachHangBean();
					KhachHangBo khbo = new KhachHangBo();

					String usernameLowerCase = username.toLowerCase();

					MD5 md = new MD5();

					String hassPass = md.getHashPass(password);

					kh = khbo.kiemTraDN(usernameLowerCase, hassPass);

					if (kh != null) {
						String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
						valid = VerifyUtils.verify(gRecaptchaResponse);

						if (valid == true) {
							session.setAttribute("auth", (KhachHangBean) kh);
							RequestDispatcher rd = request.getRequestDispatcher("home");
							rd.forward(request, response);
							return;
						}
					}
				}
				RequestDispatcher rd = request.getRequestDispatcher("views/signin.jsp");
				rd.forward(request, response);
				return;
			}
			RequestDispatcher rd = request.getRequestDispatcher("home");
			rd.forward(request, response);

		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
