package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.VerifyUtils;

/**
 * Servlet implementation class doLogin
 */
@WebServlet("/doLogin")
public class doLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public doLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userName = request.getParameter("userName");
		String password = request.getParameter("password");

		boolean valid = true;
		String errorString = null;


		if (userName != null && password != null) {

			// Check userName & password
			if (!"tom".equals(userName) || !"123".equals(password)) {
				valid = false;
				errorString = "UserName or Password invalid!";
			}

			if (valid) {

				String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

				// Verify CAPTCHA.

				valid = VerifyUtils.verify(gRecaptchaResponse);
				if (!valid) {
					errorString = "Captcha invalid!";
				}
			}
			
			if (!valid) {
				request.setAttribute("errorString", errorString);
				RequestDispatcher rd = request.getRequestDispatcher("views/login.jsp");
				rd.forward(request, response);
				return;
			}
			request.getSession().setAttribute("loginedUser", userName);
			RequestDispatcher rd = request.getRequestDispatcher("views/info.jsp");
			rd.forward(request, response);
			return;

		}
		RequestDispatcher rd = request.getRequestDispatcher("views/login.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
