package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BanPhimBean;
import model.bo.BanPhimBo;

/**
 * Servlet implementation class LoadMore
 */
@WebServlet("/load")
public class LoadMore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadMore() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {

			BanPhimBo sbo = new BanPhimBo();

			String amount = "";

			int amountPopular = 0;
			if (request.getParameter("popular") != null) {
				amount = request.getParameter("popular");

				amountPopular = Integer.parseInt(amount);
			
				ArrayList<BanPhimBean> dssach = sbo.getMoreProduct(amountPopular);

				DecimalFormat formatter = new DecimalFormat("###,###,###");
				PrintWriter out = response.getWriter();

				for (BanPhimBean kb : dssach) {
					out.print("<div class=\"col-xl-4 col-lg-4 col-md-6 col-sm-6 product_more\">\r\n"
							+ "					<div class=\"single-popular-items mb-50 text-center\">\r\n"
							+ "						<div class=\"popular-img\">\r\n"
							+ "							<img src=\"assets/img/gallery/" + kb.getAnh()
							+ "\" alt=\"\">\r\n" + "							<div class=\"img-cap\">\r\n"
							+ "								<a href=\"cart\"> \r\n"
							+ "									<span>Thêm vào giỏ hàng</span>\r\n"
							+ "								</a>\r\n" + "							</div>\r\n"
							+ "							<div class=\"favorit-items\">\r\n"
							+ "								<span class=\"flaticon-heart\"></span>\r\n"
							+ "							</div>\r\n" + "						</div>\r\n"
							+ "						<div class=\"popular-caption\">\r\n"
							+ "							<h3>\r\n"
							+ "								<a href=\"product-details?keyboardId=${kb.getMaBanPhim()}\"> \r\n"
							+ "									" + kb.getTenBanPhim() + "</a>\r\n"
							+ "							</h3>\r\n" + "							<span> \r\n"
							+ "							" + formatter.format(kb.getGiaBan()) + "₫"
							+ "							</span>\r\n" + "						</div>\r\n"
							+ "					</div>\r\n" + "				</div>");
				}
			} else {
				amount = request.getParameter("productDetail");
				amountPopular = Integer.parseInt(amount);
				ArrayList<BanPhimBean> dssach = sbo.getMoreProduct(amountPopular);

				PrintWriter out = response.getWriter();

				for (BanPhimBean bp : dssach) {

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
