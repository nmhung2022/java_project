package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.HoaDonBean;
import model.bean.HoaDonChiTietBean;
import model.bean.KhachHangBean;
import model.bo.HoaDonChiTietBo;

/**
 * Servlet implementation class OrderDetailHistory
 */
@WebServlet("/order-detail")
public class OrderDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OrderDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			String maHoaDon = request.getParameter("mhd");
			HttpSession session = request.getSession();
			KhachHangBean kh = (KhachHangBean) session.getAttribute("auth");

			if (kh != null) {
				HoaDonChiTietBo hdct = new HoaDonChiTietBo();

				HoaDonChiTietBo orderDetailHistory = hdct.getOrderDetail(kh.getMaKH(), maHoaDon);
				
				request.setAttribute("orderDetailHistory", orderDetailHistory);
				request.setAttribute("listOrderDetailHistory", orderDetailHistory.ds);
				
				RequestDispatcher rd = request.getRequestDispatcher("views/order_detail.jsp");
				rd.forward(request, response);
				return;
			}
			request.setAttribute("isSignin", false);
			RequestDispatcher rd = request.getRequestDispatcher("home");
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
