package model.bean;

public class LoaiBean {
	private int maLoai;
	private String tenLoai;

	public int getMaLoai() {
		return maLoai;
	}

	public void setMaLoai(int maloai) {
		this.maLoai = maloai;
	}

	public String getTenLoai() {
		return tenLoai;
	}

	public void setTenLoai(String tenLoai) {
		this.tenLoai = tenLoai;
	}

	public LoaiBean(int maloai, String tenLoai) {
		super();
		this.maLoai = maloai;
		this.tenLoai = tenLoai;
	}

	public LoaiBean() {
		super();
		// TODO Auto-generated constructor stub
	}

}
