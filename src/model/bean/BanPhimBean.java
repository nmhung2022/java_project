package model.bean;

import java.sql.Date;

public class BanPhimBean {
	private String maBanPhim;
	private String tenBanPhim;
	private int maLoai;
	private int maSwitch;
	private Long giaBan;
	private String anh;
	private long soLuong;
	private String ngayNhap;
	private String moTa;


	public BanPhimBean(String maBanPhim, String tenBanPhim, int maLoai, int maSwitch, Long giaBan, String anh,
			long soLuong, String ngayNhap, String moTa) {
		super();
		this.maBanPhim = maBanPhim;
		this.tenBanPhim = tenBanPhim;
		this.maLoai = maLoai;
		this.maSwitch = maSwitch;
		this.giaBan = giaBan;
		this.anh = anh;
		this.soLuong = soLuong;
		this.ngayNhap = ngayNhap;
		this.moTa = moTa;
	}

	public String getMaBanPhim() {
		return maBanPhim;
	}

	public void setMaBanPhim(String maBanPhim) {
		this.maBanPhim = maBanPhim;
	}

	public String getTenBanPhim() {
		return tenBanPhim;
	}

	public void setTenBanPhim(String tenBanPhim) {
		this.tenBanPhim = tenBanPhim;
	}

	public long getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(long soLuong) {
		this.soLuong = soLuong;
	}

	public Long getGiaBan() {
		return giaBan;
	}

	public void setGiaBan(Long giaBan) {
		this.giaBan = giaBan;
	}

	public int getMaLoai() {
		return maLoai;
	}

	public void setMaLoai(int maLoai) {
		this.maLoai = maLoai;
	}

	public String getAnh() {
		return anh;
	}

	public void setAnh(String anh) {
		this.anh = anh;
	}

	public String getNgayNhap() {
		return ngayNhap;
	}

	public void setNgayNhap(String ngayNhap) {
		this.ngayNhap = ngayNhap;
	}

	public int getMaSwitch() {
		return maSwitch;
	}

	public void setMaSwitch(int maSwitch) {
		this.maSwitch = maSwitch;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

}
