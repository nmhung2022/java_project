package model.bean;


public class GioHangBean {
	private String maBanPhim;
    private String tenBanPhim;    
    private String anh;
	private long soLuong;
	private Long giaBan;
	private Long thanhTien;
	
	
    public GioHangBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GioHangBean(String maBanPhim, String tenBanPhim, String anh, long soLuong, Long giaBan) {
		super();
		this.maBanPhim = maBanPhim;
		this.tenBanPhim = tenBanPhim;
		this.anh = anh;
		this.soLuong = soLuong;
		this.giaBan = giaBan;
		this.thanhTien = soLuong * giaBan;
	}
	public String getMaBanPhim() {
		return maBanPhim;
	}
	public void setMaBanPhim(String maBanPhim) {
		this.maBanPhim = maBanPhim;
	}
	public String getTenBanPhim() {
		return tenBanPhim;
	}
	public void setTenBanPhim(String tenBanPhim) {
		this.tenBanPhim = tenBanPhim;
	}
	public String getAnh() {
		return anh;
	}
	public void setAnh(String anh) {
		this.anh = anh;
	}
	public long getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(long soLuong) {
		this.soLuong = soLuong;
	}
	public Long getGiaBan() {
		return giaBan;
	}
	public void setGiaBan(Long giaBan) {
		this.giaBan = giaBan;
	}
	public Long getThanhTien() {
		return soLuong * giaBan;
	}
	public void setThanhTien(Long thanhTien) {
		this.thanhTien = thanhTien;
	}
	
    
      
}
