package model.bean;

public class SwitchBean {
	private int maSwitch;
	private String tenSwitch;
	
	public SwitchBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SwitchBean(int maSwitch, String tenSwitch) {
		super();
		this.maSwitch = maSwitch;
		this.tenSwitch = tenSwitch;
	}
	public int getMaSwitch() {
		return maSwitch;
	}
	public void setMaSwitch(int maSwitch) {
		this.maSwitch = maSwitch;
	}
	public String getTenSwitch() {
		return tenSwitch;
	}
	public void setTenSwitch(String tenSwitch) {
		this.tenSwitch = tenSwitch;
	}
}
