package model.bean;

public class HoaDonChiTietBean {
    private String maHoaDon;
    private String tenBanPhim;
    private String anh;
    private long soLuong;
    private Long gia;
    private Long thanhTien;
    
	public HoaDonChiTietBean(String maHoaDon, String tenBanPhim, String anh, long soLuong, Long gia) {
		super();
		this.maHoaDon = maHoaDon;
		this.tenBanPhim = tenBanPhim;
		this.anh = anh;
		this.soLuong = soLuong;
		this.gia = gia;
		this.thanhTien = soLuong * gia;
	}
	public HoaDonChiTietBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMaHoaDon() {
		return maHoaDon;
	}
	public void setMaHoaDon(String maHoaDon) {
		this.maHoaDon = maHoaDon;
	}
	public String getTenBanPhim() {
		return tenBanPhim;
	}
	public void setTenBanPhim(String tenBanPhim) {
		this.tenBanPhim = tenBanPhim;
	}
	public String getAnh() {
		return anh;
	}
	public void setAnh(String anh) {
		this.anh = anh;
	}
	public long getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(long soLuong) {
		this.soLuong = soLuong;
	}
	public Long getGia() {
		return gia;
	}
	public void setGia(Long gia) {
		this.gia = gia;
	}
	public Long getThanhTien() {
		return this.soLuong * this.gia;
	}
	public void setThanhTien(Long thanhTien) {
		this.thanhTien = thanhTien;
	}
    
    
  
    
    
}
