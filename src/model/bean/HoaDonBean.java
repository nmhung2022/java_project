package model.bean;

import java.util.Date;

public class HoaDonBean {
    private String maHoaDon;
    private Date thoiGianMua;
    private int soLuongMua;
    private int trangThai;
    private int maKhachHang;
    
    
    public HoaDonBean(String maHoaDon, Date thoiGianMua, int soLuongMua, int trangThai) {
	super();
	this.maHoaDon = maHoaDon;
	this.thoiGianMua = thoiGianMua;
	this.soLuongMua = soLuongMua;
	this.trangThai = trangThai;
    }
    
    
    public HoaDonBean(String maHoaDon, Date thoiGianMua, int soLuongMua, int trangThai, int maKhachHang) {
   	super();
   	this.maHoaDon = maHoaDon;
   	this.thoiGianMua = thoiGianMua;
   	this.soLuongMua = soLuongMua;
   	this.trangThai = trangThai;
   	this.maKhachHang = maKhachHang;
       }
    

    public HoaDonBean() {
   	super();
   	
       }
    
    public String getMaHoaDon() {
        return maHoaDon;
    }
    public void setMaHoaDon(String maHoaDon) {
        this.maHoaDon = maHoaDon;
    }
    public Date getThoiGianMua() {
        return thoiGianMua;
    }
    public void setThoiGianMua(Date thoiGianMua) {
        this.thoiGianMua = thoiGianMua;
    }
    public int getSoLuongMua() {
        return soLuongMua;
    }
    public void setSoLuongMua(int soLuongMua) {
        this.soLuongMua = soLuongMua;
    }
    public int getTrangThai() {
        return trangThai;
    }
    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }
    public int getMaKhachHang() {
        return maKhachHang;
    }
    public void setMaKhachHang(int maKhachHang) {
        this.maKhachHang = maKhachHang;
    }
    
   
    
}
