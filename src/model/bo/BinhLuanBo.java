package model.bo;

import java.util.ArrayList;

import model.bean.BinhLuanBean;
import model.dao.BinhLuanDao;


public class BinhLuanBo {
    public ArrayList<BinhLuanBean> ds = new ArrayList<BinhLuanBean>();
    
    public BinhLuanBo() {
	super();
    }
    
    public ArrayList<BinhLuanBean> getCommentOfBook(String maSach) {
	BinhLuanDao bl = new BinhLuanDao();
	
	return bl.getCommentOfBook(maSach);
	
    }

}
