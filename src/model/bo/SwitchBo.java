package model.bo;

import java.util.ArrayList;
import model.bean.SwitchBean;
import model.dao.SwitchDao;

public class SwitchBo {
	SwitchDao swd = new SwitchDao();

	public SwitchBean getSwitchById(int idSwitch) throws Exception {
		return swd.getSwitchById(idSwitch);
		
	}

	public ArrayList<SwitchBean> getSwitch() throws Exception {
		return swd.getLoaiSach();
	}
}
