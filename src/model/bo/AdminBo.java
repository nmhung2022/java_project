package model.bo;

import java.util.Date;

import model.bean.AdminBean;
import model.bean.KhachHangBean;
import model.dao.AdminDao;

public class AdminBo {
	AdminDao admin = new AdminDao();

	public AdminBean kiemTraDN(String tendn, String pass) throws Exception {
		return admin.kiemTraDangNhap(tendn, pass);
	}

	public int kiemTraDK(String tendn, String pass) throws Exception {
		return admin.kiemTraDangKy(tendn, pass);
	}

	public boolean kiemTraCN(String maBanPhim, String tenBanPhim, int maLoai, int maSwitch, Long giaBan, String anh,
			long soLuong, String ngayNhap, String moTa) throws Exception {
		return admin.kiemTraCapNhat(maBanPhim, tenBanPhim, maLoai, maSwitch, giaBan, anh, soLuong, ngayNhap, moTa);
	}

	public boolean kiemTraXoa(String maSach) throws Exception {
		return admin.kiemTraXoa(maSach);
	}

	public int kiemTraThem(String maBanPhim, String tenBanPhim, int maLoai, int maSwitch, Long giaBan, String anh,
			long soLuong, String ngayNhap, String moTa) throws Exception {
		return admin.kiemTraThemSach(maBanPhim, tenBanPhim, maLoai, maSwitch, giaBan, anh, soLuong, ngayNhap, moTa);
	}

	public int kiemTraTL(int maLoai, String tenLoai) throws Exception {
		return admin.kiemTraThemLoai(maLoai, tenLoai);
	}

	public boolean kiemTraCNL(int maLoai, int maLoaiOld, String tenLoai) throws Exception {
		return admin.kiemTraCapNhatLoai(maLoai, maLoaiOld, tenLoai);
	}

	public boolean kiemTraXL(int maLoai) throws Exception {
		return admin.kiemTraXoaLoai(maLoai);
	}

	public boolean xoaHoaDon(String orderId) throws Exception {
		return admin.xoaHoaDon(orderId);
	}

	public int chuyenTrangThai(String orderId, String ttOrder) throws Exception {
		return admin.chuyenTrangThaiDonHang(orderId, ttOrder);
	}

}
