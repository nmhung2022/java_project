package model.bo;

import java.util.ArrayList;

import model.bean.BanPhimBean;
import model.dao.BanPhimDao;

public class BanPhimBo {
	BanPhimDao sdao = new BanPhimDao();

	public ArrayList<BanPhimBean> getProductByQuantity(int quantity) throws Exception {
		return sdao.getProductByQuantity(quantity);
	}

	public ArrayList<BanPhimBean> getAllSach() throws Exception {
		return sdao.getAllBanPhim();
	}

	public BanPhimBean getKeyBoardById(String keyBoardId) throws Exception {
		return sdao.getKeyBoardById(keyBoardId);
	}

	public ArrayList<BanPhimBean> timMaLoai(int maLoai) throws Exception {
		ArrayList<BanPhimBean> ds = sdao.getAllBanPhim();
		ArrayList<BanPhimBean> tam = new ArrayList<BanPhimBean>();
		for (BanPhimBean sb : ds) {
			if (sb.getMaLoai() == maLoai)
				tam.add(sb);
		}
		return tam;
	}

	public ArrayList<BanPhimBean> getMoreProduct(int amount) throws Exception {
		return sdao.getNextProductByAmout(amount);
	}

	public Long demBanPhim(int maloai) throws Exception {
		long dem = 0;
		ArrayList<BanPhimBean> sbean = sdao.getAllBanPhim();
		int n = sbean.size();
		for (int i = 0; i < n; i++) {
			if (sbean.get(i).getMaLoai() == maloai)
				dem = dem + 1;
		}
		return dem;
	}

	public ArrayList<BanPhimBean> filterNewProduct(int quantityProduct) throws Exception {
		return sdao.filterNewProduct(quantityProduct);
	}

	public ArrayList<BanPhimBean> filterPriceASC(int quantityProduct) throws Exception {
		return sdao.filterPriceASC(quantityProduct);
	}

	public ArrayList<BanPhimBean> filterPriceDESC(int quantityProduct) throws Exception {
		return sdao.filterPriceDESC(quantityProduct);
	}

	public ArrayList<BanPhimBean> searchByNameWithQuantity(String txtSearch, int quantityProduct) throws Exception {
		return sdao.searchByNameWithQuantity(txtSearch, quantityProduct);
	}

	public ArrayList<BanPhimBean> timChung(String key) {
		return timChung(key);
	}
}
