package model.bo;

import java.util.ArrayList;

import model.bean.HoaDonChiTietBean;
import model.dao.HoaDonChiTietDao;

public class HoaDonChiTietBo {
    public ArrayList<HoaDonChiTietBean> ds = new ArrayList<HoaDonChiTietBean>();

    public HoaDonChiTietBo() {
	super();
    }

    HoaDonChiTietDao hdao = new HoaDonChiTietDao();

    public HoaDonChiTietBo getOrderDetail(int customerId, String orderId) throws Exception {
	return hdao.getOrderDetail(customerId, orderId);
    }

    public void Them(HoaDonChiTietBean hdct) {
	ds.add(hdct);
    }

    public long tongTien() {
	long total = 0;
	for (int i = 0; i < ds.size(); i++) {
	    total += ds.get(i).getThanhTien();
	}
	return total;
    }



}
