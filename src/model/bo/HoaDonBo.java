package model.bo;

import java.util.ArrayList;

import model.bean.HoaDonBean;
import model.dao.HoaDonDao;

public class HoaDonBo {
	HoaDonDao hdao = new HoaDonDao();

	public ArrayList<HoaDonBean> getOrderByQuantity(int customerId, int quantity) throws Exception {
		return hdao.getOrderHistoryByQuantity(customerId, quantity);
	}

	public ArrayList<HoaDonBean> getAllOrderHistory() throws Exception {
		return hdao.getAllOrderHistory();
	}

	public ArrayList<HoaDonBean> searchByNameWithQuantity(String txtSearch, int quantity) throws Exception {
		return hdao.searchByNameWithQuantity(txtSearch, quantity);
	}

	
	
	public ArrayList<HoaDonBean> getOrder(int maKH) throws Exception {
		return hdao.getOrderHistory(maKH);
	}

	public ArrayList<HoaDonBean> getOrderAllByQuantity(int quantity) throws Exception {
		return hdao.getOrderAllByQuantity(quantity);
	}

}
