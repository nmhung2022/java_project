package model.bo;

import java.util.ArrayList;

import model.bean.GioHangBean;

public class GioHangBo {
	public ArrayList<GioHangBean> ds = new ArrayList<GioHangBean>();

	public GioHangBo() {
		super();
	}

	public void addToCart(String maBanPhim, String tenBanPhim, String anh, Long giaBan, int soLuong) {
		for (GioHangBean g : ds) {
			if (g.getMaBanPhim().equals(maBanPhim)) {
				g.setSoLuong(g.getSoLuong() + soLuong);
				return;
			}
		}
		GioHangBean h = new GioHangBean(maBanPhim, tenBanPhim, anh, soLuong, giaBan);
		ds.add(h);

	}

	public void removeById(String maBanPhim) {
		for (GioHangBean g : ds)
			if (g.getMaBanPhim().equals(maBanPhim)) {
				ds.remove(g);
				return;
			}
	}

	public long getTongTien() {
		long total = 0;
		for (int i = 0; i < ds.size(); i++) {
			total += ds.get(i).getThanhTien();
		}
		return total;
	}

	public void updateQuantity(String maBanPhim, int soluong) {
		for (int i = 0; i < ds.size(); i++) {
			if (maBanPhim.equals(ds.get(i).getMaBanPhim())) {
				ds.get(i).setSoLuong(soluong);
				return;
			}
		}
	}

	public int totalQuanity() {
		int total = 0;
		for (GioHangBean g : ds) {
			total += g.getSoLuong();
		}
		return total;
	}

	public int getSize() {
		return ds.size();
	}

}
