package model.bo;

import java.util.ArrayList;

import model.bean.LoaiBean;
import model.bean.SwitchBean;
import model.bean.BanPhimBean;
import model.dao.LoaiDao;

public class LoaiBo {
	LoaiDao ldao = new LoaiDao();

	public ArrayList<LoaiBean> getLoai() throws Exception {
		return ldao.getLoaiBanPhim();
	}

	public ArrayList<LoaiBean> timMaLoaiById(int maLoai) throws Exception {
		ArrayList<LoaiBean> ds = ldao.getLoaiBanPhim();
		ArrayList<LoaiBean> tam = new ArrayList<LoaiBean>();
		for (LoaiBean l : ds) {
			if (l.getMaLoai() == maLoai)
				tam.add(l);
		}
		return tam;
	}

	public LoaiBean getLoaiById(int maLoai) throws Exception {
		return ldao.getLoaiById(maLoai);
	}

}
