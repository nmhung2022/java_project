package model.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import model.bean.GioHangBean;
import model.bean.KhachHangBean;
import model.dao.KhachHangDao;

public class KhachHangBo {
	KhachHangDao khdao = new KhachHangDao();

	public KhachHangBean kiemTraDN(String tendn, String pass) throws Exception {
		return khdao.kiemTraDangNhap(tendn, pass);
	}

	public int kiemTraDK(String username, String fullname, String email, String password, String address,
			String phoneNumber) throws Exception {
		return khdao.kiemTraDangKy(username, fullname, email, password, address, phoneNumber);
	}

	public KhachHangBean getInfoCustomer(String customerId) throws Exception {
		return khdao.getInfoCustomer(customerId);

	}

	public int order(int customerId, Date orderDate, int isBought) throws SQLException, Exception {
		return khdao.order(customerId, orderDate, isBought);
	}

	public boolean orderDetail(int orderId, String keyboardId, long quantity) throws SQLException, Exception {
		return khdao.orderDetail(orderId, keyboardId, quantity);
	}

	public int chuyenTrangThai(String orderId, String ttOrder) throws Exception {
		return khdao.chuyenTrangThaiDatHang(orderId, ttOrder);

	}

	public boolean huyDatHang(String orderId) throws Exception {
		return khdao.huyDatHang(orderId);
	}

}
