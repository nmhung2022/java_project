package model.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import model.bean.HoaDonChiTietBean;
import model.bo.HoaDonChiTietBo;

public class HoaDonChiTietDao {
    public HoaDonChiTietBo getOrderDetail(int customerId, String orderId) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_HoaDon_OrderDetail(?, ?)}");

	cs.setInt(1, customerId);
	cs.setString(2, orderId);

	ResultSet rs = cs.executeQuery();

	HoaDonChiTietBo ds = new HoaDonChiTietBo();

	while (rs.next()) {

	    HoaDonChiTietBean hdctbean = new HoaDonChiTietBean();
	    hdctbean.setMaHoaDon(rs.getString("MaHoaDon"));
	    hdctbean.setTenBanPhim(rs.getString("TenBanPhim"));
	    hdctbean.setAnh(rs.getString("Anh"));
	    hdctbean.setSoLuong(rs.getInt("SoLuongMua"));
	    hdctbean.setGia(rs.getLong("GiaBan"));
	    ds.Them(hdctbean);
	}
	rs.close();
	dc.cn.close();
	return ds;
    }
}
