package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.bean.LoaiBean;
import model.dao.LoaiDao;

public class LoaiDao {
	public ArrayList<LoaiBean> getLoaiBanPhim() throws Exception {
		ArrayList<LoaiBean> ds = new ArrayList<LoaiBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String sql = "select * from Loai";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();
		while (rs.next()) {
			int maloai = rs.getInt("MaLoai");
			String tenloai = rs.getString("TenLoai");
			ds.add(new LoaiBean(maloai, tenloai));
		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public LoaiBean getLoaiById(int maLoai) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();
		String sql = "select * from Loai where MaLoai = ?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setInt(1, maLoai);
		ResultSet rs = cmd.executeQuery();

		LoaiBean lb = null;
		while (rs.next()) {
			int maloai = rs.getInt("MaLoai");
			String tenLoai = rs.getString("TenLoai");
			lb = new LoaiBean(maloai, tenLoai);
		}
		rs.close();
		dc.cn.close();
		return lb;

	}

}
