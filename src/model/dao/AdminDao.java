package model.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.bean.AdminBean;
import model.bean.KhachHangBean;
import model.bean.BanPhimBean;

public class AdminDao {
	public AdminBean kiemTraDangNhap(String username, String password) throws Exception, SQLException {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_Authenticate(?, ?)}");

		cs.setString(1, username);
		cs.setString(2, password);

		ResultSet rs = cs.executeQuery();

		if (rs.next() == false) {
			return null;
		} else {
			AdminBean admin = new AdminBean();
			admin.setTenDangNhap(rs.getString("TenDangNhap"));
			admin.setMatKhau(rs.getString("MatKhau"));
			admin.setQuyen(rs.getBoolean("Quyen"));
			return admin;
		}
	}

	public int kiemTraDangKy(String username, String password) throws Exception, SQLException {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_Register(?, ?, ?)}");

		cs.setString(1, username);
		cs.setString(2, password);
		cs.registerOutParameter(3, Types.INTEGER);

		cs.execute();

		int result = cs.getInt(3);

		return result;
	}

	public boolean kiemTraCapNhat(String maBanPhim, String tenBanPhim, int maLoai, int maSwitch, Long giaBan,
			String anh, long soLuong, String ngayNhap, String moTa) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();
		CallableStatement cs = dc.cn.prepareCall("{call proc_Product_Update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

		cs.setString(1, maBanPhim);
		cs.setString(2, tenBanPhim);
		cs.setInt(3, maLoai);
		cs.setInt(4, maSwitch);
		cs.setLong(5, giaBan);
		cs.setString(6, anh);
		cs.setLong(7, soLuong);
		cs.setString(8, ngayNhap);
		cs.setString(9, moTa);

		cs.registerOutParameter(10, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(10);

		return result;
	}

	public boolean kiemTraXoa(String maSach) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Product_Delete(?, ?)}");

		cs.setString(1, maSach);

		cs.registerOutParameter(2, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(2);

		return result;
	}

	public int kiemTraThemSach(String maBanPhim, String tenBanPhim, int maLoai, int maSwitch, Long giaBan, String anh,
			long soLuong, String ngayNhap, String moTa) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Product_Insert(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
		cs.setString(1, maBanPhim);
		cs.setString(2, tenBanPhim);
		cs.setInt(3, maLoai);
		cs.setInt(4, maSwitch);
		cs.setLong(5, giaBan);
		cs.setString(6, anh);
		cs.setLong(7, soLuong);
		cs.setString(8, ngayNhap);
		cs.setString(9, moTa);

		cs.registerOutParameter(10, Types.BIGINT);

		cs.execute();

		int result = cs.getInt(10);

		return result;
	}

	public int kiemTraThemLoai(int maLoai, String tenLoai) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfProducts_Insert(?, ?, ?)}");
		cs.setInt(1, maLoai);
		cs.setString(2, tenLoai);

		cs.registerOutParameter(3, Types.BIGINT);

		cs.execute();

		int result = cs.getInt(3);

		return result;
	}

	public boolean kiemTraCapNhatLoai(int maLoai, int maLoaiOld, String tenLoai) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfProducts_Update(?, ?, ?, ?)}");
		cs.setInt(1, maLoai);
		cs.setInt(2, maLoaiOld);
		cs.setString(3, tenLoai);

		cs.registerOutParameter(4, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(4);
		return result;

	}

	public boolean kiemTraXoaLoai(int maLoai) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_TypesOfProducts_Delete(?, ?)}");

		cs.setInt(1, maLoai);

		cs.registerOutParameter(2, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(2);

		return result;
	}

//    public LichSuMuaHangBo layLichSuDathang() throws Exception {
//	DungChung dc = new DungChung();
//	dc.ketNoi();
//
//	CallableStatement cs = dc.cn.prepareCall("{call proc_Admin_GetAllOrderHistory}");
//
//	ResultSet rs = cs.executeQuery();
//
//	LichSuMuaHangBo list = new LichSuMuaHangBo();
//
//	while (rs.next()) {
//
//	    LichSuMuaHangBean ls = new LichSuMuaHangBean();
//
//	    ls.setTenSach(rs.getString("tensach"));
//	    ls.setAnh(rs.getString("anh"));
//	    ls.setTongTien(rs.getLong("SoLuongMua") * rs.getLong("gia"));
//	    ls.setTrangThai(rs.getInt("TrangThai"));
//	    ls.setThoiGianMua(rs.getDate("NgayMua"));
//	    ls.setMaSach(rs.getString("MaSach"));
//	    ls.setMaHoaDon(rs.getString("MaHoaDon"));
//
//	    list.ThemObject(ls);
//	}
//	return list;
//
//    }

	public boolean xoaHoaDon(String orderId) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Delete(?, ?)}");

		cs.setString(1, orderId);

		cs.registerOutParameter(2, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(2);

		return result;
	}

	public int chuyenTrangThaiDonHang(String orderId, String ttOrder) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Transforms(?, ?, ?)}");

		cs.setString(1, orderId);
		cs.setString(2, ttOrder);

		cs.registerOutParameter(3, Types.BIGINT);

		cs.execute();

		int result = cs.getInt(3);

		return result;
	}
}
