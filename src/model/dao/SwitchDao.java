package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.bean.LoaiBean;
import model.bean.SwitchBean;

public class SwitchDao {

	public SwitchBean getSwitchById(int idSwitch) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();
		String sql = "select * from Switch where maSwitch = ?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setInt(1, idSwitch);
		ResultSet rs = cmd.executeQuery();

		SwitchBean sb = null;
		while (rs.next()) {
			int maSwitch = rs.getInt("maSwitch");
			String tenSwitch = rs.getString("tenSwitch");
			sb = new SwitchBean(maSwitch, tenSwitch);
		}
		rs.close();
		dc.cn.close();
		return sb;

	}

	public ArrayList<SwitchBean> getLoaiSach() throws Exception {
		ArrayList<SwitchBean> ds = new ArrayList<SwitchBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String sql = "select * from Switch";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();
		while (rs.next()) {
			int maSwitch = rs.getInt("MaSwitch");
			String tenSwitch = rs.getString("TenSwitch");
			ds.add(new SwitchBean(maSwitch, tenSwitch));
		}
		rs.close();
		dc.cn.close();
		return ds;
	}

}
