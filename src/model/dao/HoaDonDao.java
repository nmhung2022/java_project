package model.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.bean.BanPhimBean;
import model.bean.HoaDonBean;

public class HoaDonDao {

	public ArrayList<HoaDonBean> getOrderHistoryByQuantity(int customerId, int quantity) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_HoaDon_Order(?, ?)}");

		cs.setInt(1, customerId);
		cs.setInt(2, quantity);

		ResultSet rs = cs.executeQuery();

		ArrayList<HoaDonBean> ds = new ArrayList<HoaDonBean>();

		while (rs.next()) {
			HoaDonBean hdbean = new HoaDonBean();
			hdbean.setMaHoaDon(rs.getString("MaHoaDon"));
			hdbean.setThoiGianMua(rs.getDate("NgayDatMua"));
			hdbean.setSoLuongMua(rs.getInt("SoLuongMua"));
			hdbean.setTrangThai(rs.getInt("TrangThai"));
			ds.add(hdbean);
		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<HoaDonBean> getAllOrderHistory() throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		PreparedStatement stmt = dc.cn.prepareStatement("select * from view_OrderHisoty");

		ResultSet rs = stmt.executeQuery();

		ArrayList<HoaDonBean> ds = new ArrayList<HoaDonBean>();

		while (rs.next()) {

			HoaDonBean hdbean = new HoaDonBean();
			hdbean.setMaHoaDon(rs.getString("MaHoaDon"));
			hdbean.setThoiGianMua(rs.getDate("NgayDatMua"));
			hdbean.setSoLuongMua(rs.getInt("SoLuongMua"));
			hdbean.setTrangThai(rs.getInt("TrangThai"));
			hdbean.setMaKhachHang(rs.getInt("MaKhachHang"));
			ds.add(hdbean);
		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<HoaDonBean> searchByNameWithQuantity(String txtSearch, int quantity) {

		return null;
	}

	public ArrayList<HoaDonBean> getOrderHistory(int customerId) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_HoaDon_Order(?)}");

		cs.setInt(1, customerId);

		ResultSet rs = cs.executeQuery();

		ArrayList<HoaDonBean> ds = new ArrayList<HoaDonBean>();

		while (rs.next()) {

			HoaDonBean hdbean = new HoaDonBean();
			hdbean.setMaHoaDon(rs.getString("MaHoaDon"));
			hdbean.setThoiGianMua(rs.getDate("NgayDatMua"));
			hdbean.setSoLuongMua(rs.getInt("SoLuongMua"));
			hdbean.setTrangThai(rs.getInt("TrangThai"));
			ds.add(hdbean);
		}
		rs.close();
		dc.cn.close();
		return ds;

	}

	public ArrayList<HoaDonBean> getOrderAllByQuantity(int quantity) throws Exception {

		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "select h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai, count(c.MaHoaDon) as SoLuongMua\r\n"
				+ "from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim\r\n"
				+ "group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai\r\n"
				+ "ORDER BY h.NgayDatMua desc OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY";

		PreparedStatement ps = dc.cn.prepareStatement(query);

		ps.setInt(1, quantity);
		ResultSet rs = ps.executeQuery();

		ArrayList<HoaDonBean> ds = new ArrayList<HoaDonBean>();
		while (rs.next()) {
			HoaDonBean hdbean = new HoaDonBean();
			hdbean.setMaHoaDon(rs.getString("MaHoaDon"));
			hdbean.setThoiGianMua(rs.getDate("NgayDatMua"));
			hdbean.setSoLuongMua(rs.getInt("SoLuongMua"));
			hdbean.setTrangThai(rs.getInt("TrangThai"));
			hdbean.setMaKhachHang(rs.getInt("MaKhachHang"));
			ds.add(hdbean);

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

}
