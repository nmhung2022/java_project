package model.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.bean.BanPhimBean;

public class BanPhimDao {

	public ArrayList<BanPhimBean> getAllBanPhim() throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "select * from BanPhim";
		PreparedStatement cmd = dc.cn.prepareStatement(query);
		ResultSet rs = cmd.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<BanPhimBean> getProductByQuantity(int quantity) throws Exception {

		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "select top " + quantity + " * from BanPhim";
		PreparedStatement cmd = dc.cn.prepareStatement(query);
		ResultSet rs = cmd.executeQuery();
		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<BanPhimBean> getNextProductByAmout(int amount) throws Exception {

		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "SELECT * FROM BanPhim ORDER BY maBanPhim OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
		PreparedStatement ps = dc.cn.prepareStatement(query);

		ps.setInt(1, amount);
		ps.setInt(2, amount);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;

	}

	public ArrayList<BanPhimBean> timChung(String key) throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "select * from BanPhim where tenBanPhim like ?";

		PreparedStatement ps = dc.cn.prepareStatement(query);

		ps.setString(1, "%" + key + "%");
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public BanPhimBean getKeyBoardById(String keyBoardId) throws Exception {

		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "select top 1 * from BanPhim where maBanPhim = ?";

		PreparedStatement ps = dc.cn.prepareStatement(query);

		ps.setString(1, keyBoardId);
		ResultSet rs = ps.executeQuery();

		BanPhimBean sb = null;

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			sb = new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa);

		}

		rs.close();
		dc.cn.close();
		return sb;
	}

	public ArrayList<BanPhimBean> searchByNameWithQuantity(String txtSearch, int quantityProduct) throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "SELECT * FROM BanPhim Where TenBanPhim Like ? ORDER BY NgayNhap ASC OFFSET 0 ROWS FETCH FIRST ? ROWS ONLY";
		PreparedStatement ps = dc.cn.prepareStatement(query);
		ps.setString(1, "%" + txtSearch + "%");
		ps.setInt(2, quantityProduct);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<BanPhimBean> filterPriceDESC(int quantityProduct) throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "SELECT * FROM BanPhim ORDER BY GiaBan DESC OFFSET 0 ROWS FETCH FIRST ? ROWS ONLY";
		PreparedStatement ps = dc.cn.prepareStatement(query);
		ps.setInt(1, quantityProduct);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<BanPhimBean> filterPriceASC(int quantityProduct) throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "SELECT * FROM BanPhim ORDER BY GiaBan ASC OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY";
		PreparedStatement ps = dc.cn.prepareStatement(query);
		ps.setInt(1, quantityProduct);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public ArrayList<BanPhimBean> filterNewProduct(int quantityProduct) throws Exception {
		ArrayList<BanPhimBean> ds = new ArrayList<BanPhimBean>();
		DungChung dc = new DungChung();
		dc.ketNoi();
		String query = "SELECT * FROM BanPhim ORDER BY NgayNhap ASC OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY";
		PreparedStatement ps = dc.cn.prepareStatement(query);
		ps.setInt(1, quantityProduct);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String maBanPhim = rs.getString("maBanPhim");
			String tenBanPhim = rs.getString("TenBanPhim");
			Long soLuong = rs.getLong("SoLuong");
			Long gia = rs.getLong("GiaBan");
			int maLoai = rs.getInt("MaLoai");
			String anh = rs.getString("Anh");
			String ngayNhap = rs.getString("NgayNhap");
			int maSwitch = rs.getInt("MaSwitch");
			String moTa = rs.getString("MoTa");

			ds.add(new BanPhimBean(maBanPhim, tenBanPhim, maLoai, maSwitch, gia, anh, soLuong, ngayNhap, moTa));

		}
		rs.close();
		dc.cn.close();
		return ds;
	}

}
