package model.dao;

import java.sql.CallableStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import model.bean.KhachHangBean;

public class KhachHangDao {
	public KhachHangBean kiemTraDangNhap(String username, String password) throws Exception, SQLException {
		DungChung dc = new DungChung();
		dc.ketNoi();
		CallableStatement cs = dc.cn.prepareCall("{call proc_KhachHang_Authenticate(?, ?)}");

		cs.setString(1, username);
		cs.setString(2, password);

		ResultSet rs = cs.executeQuery();

		KhachHangBean kh = null;

		if (rs.next()) {
			kh = new KhachHangBean();
			kh.setMaKH(rs.getInt("MaKhachHang"));
			kh.setHoTen(rs.getString("HoTen"));
			kh.setTenDangNhap(rs.getString("TenDangNhap"));
			kh.setDiaChi(rs.getString("DiaChi"));
			kh.setEmail(rs.getString("Email"));
			kh.setSoDienThoai(rs.getString("SoDienThoai"));

		}
		cs.close();
		dc.cn.close();
		return kh;
	}

	public int kiemTraDangKy(String username, String fullname, String email, String password, String address,
			String phoneNumber) throws Exception, SQLException {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_KhachHang_Register(?, ?, ?, ?, ?, ?, ?)}");

		cs.setString(1, fullname);
		cs.setString(2, username);
		cs.setString(3, password);
		cs.setString(4, address);
		cs.setString(5, email);
		cs.setString(6, phoneNumber);

		cs.registerOutParameter(7, Types.INTEGER);

		cs.execute();

		int newCustomerId = cs.getInt(7);

		cs.close();
		dc.cn.close();
		return newCustomerId;

	}

	public KhachHangBean getInfoCustomer(String customerId) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_KhachHang_GetInfo(?)}");

		cs.setString(1, customerId);

		ResultSet rs = cs.executeQuery();

		KhachHangBean kh = null;
		if (rs.next()) {
			kh = new KhachHangBean();
			kh.setMaKH(rs.getInt("MaKhachHang"));
			kh.setHoTen(rs.getString("HoTen"));
			kh.setTenDangNhap(rs.getString("TenDangNhap"));
			kh.setDiaChi(rs.getString("DiaChi"));
			kh.setEmail(rs.getString("Email"));
			kh.setSoDienThoai(rs.getString("SoDienThoai"));
		}
		cs.close();
		dc.cn.close();
		return kh;
	}

	public int order(int customerId, Date orderDate, int isBought) throws SQLException, Exception {

		DungChung dc = new DungChung();
		dc.ketNoi();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");

		CallableStatement cstmt = dc.cn.prepareCall("{call proc_HoaDon_Create(?, ?, ?, ?)}");

		cstmt.setInt(1, customerId);
		cstmt.setString(2, sdf.format(orderDate));
		cstmt.setInt(3, isBought);
		cstmt.registerOutParameter(4, Types.INTEGER);

		cstmt.execute();
		int orderId = cstmt.getInt(4);

		cstmt.close();
		dc.cn.close();
		return orderId;
	}

	public boolean orderDetail(int orderId, String keyboardId, long quantity) throws SQLException, Exception {

		DungChung dc = new DungChung();
		dc.ketNoi();
		CallableStatement cstmt = dc.cn.prepareCall("{call proc_ChiTietHoaDon_Create(?, ?, ?, ?, ?)}");

		cstmt.setString(1, keyboardId);
		cstmt.setLong(2, quantity);
		cstmt.setInt(3, orderId);
		cstmt.setInt(4, 0);
		cstmt.registerOutParameter(5, Types.INTEGER);

		cstmt.execute();

		int statusCode = cstmt.getInt(5);
		cstmt.close();
		dc.cn.close();

		if (statusCode > 0) {
			return true;
		}
		return false;
	}

	public int chuyenTrangThaiDatHang(String orderId, String ttOrder) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Tranforms_Cus(?, ?, ?)}");

		cs.setString(1, orderId);
		cs.setString(2, ttOrder);

		cs.registerOutParameter(3, Types.BIGINT);

		cs.execute();

		int result = cs.getInt(3);

		cs.close();
		dc.cn.close();
		return result;
	}

	public boolean huyDatHang(String orderId) throws Exception {
		DungChung dc = new DungChung();
		dc.ketNoi();

		CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Cancel_Cus(?, ?)}");

		cs.setString(1, orderId);

		cs.registerOutParameter(2, Types.BIT);

		cs.execute();

		boolean result = cs.getBoolean(2);

		cs.close();
		dc.cn.close();
		return result;
	}
}
