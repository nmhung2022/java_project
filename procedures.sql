﻿use QLBanPhim
go

------------------------------------------CUSTOMER--------------------------------------------------

-------------------------------------------------------------------------------
--  view_OrderHisoty: View get all order history
-------------------------------------------------------------------------------
if exists(select * from sys.views  where name = 'view_OrderHisoty' and type='v')
	drop view view_OrderHisoty
go
CREATE VIEW view_OrderHisoty
as
select h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai, count(c.MaHoaDon) as SoLuongMua
from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim
group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai

go



select h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai, count(c.MaHoaDon) as SoLuongMua
from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim
group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai
ORDER BY h.NgayDatMua desc OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY



-------------------------------------------------------------------------------
--  fn_IsValidEmail: Function validate email
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'fn_IsValidEmail'))
	drop function fn_IsValidEmail
go

CREATE FUNCTION fn_IsValidEmail(@EMAIL varchar(100))RETURNS bit as
BEGIN     
  DECLARE @bitEmailVal as Bit
  DECLARE @EmailText varchar(100)

  SET @EmailText=ltrim(rtrim(isnull(@EMAIL,'')))

  SET @bitEmailVal = case when @EmailText = '' then 0
                          when @EmailText like '% %' then 0
                          when @EmailText like ('%["(),:;<>\]%') then 0
                          when substring(@EmailText,charindex('@',@EmailText),len(@EmailText)) like ('%[!#$%&*+/=?^`_{|]%') then 0
                          when (left(@EmailText,1) like ('[-_.+]') or right(@EmailText,1) like ('[-_.+]')) then 0                                                                                    
                          when (@EmailText like '%[%' or @EmailText like '%]%') then 0
                          when @EmailText LIKE '%@%@%' then 0
                          when @EmailText NOT LIKE '_%@_%._%' then 0
                          else 1 
                      end
  RETURN @bitEmailVal
END 
GO


-------------------------------------------------------------------------------
--  proc_KhachHang_Register: Đăng ký một tài khoản khách hàng mới
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_KhachHang_Register'))
	drop procedure proc_KhachHang_Register
go
create procedure proc_KhachHang_Register
@hoten nvarchar(255),
@tendn varchar(50),
@pass varchar(50),
@diachi nvarchar(455),
@email nvarchar(50),
@sodt nvarchar(15),
@makh int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @makh = -3
				return;
			end
		if(exists(select * from KhachHang as t where t.TenDangNhap = @tendn))
			begin
				set @makh = -5
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @makh = -2
				return;
			end
		if ([dbo].[fn_IsValidEmail](@email) = 0)
			begin
				set @makh = -1
				return;
			end
		if(@sodt = NULL or LEN(@sodt) < 1)
			begin
				set @makh = -4
				return;
			end

		insert into KhachHang(HoTen, TenDangNhap, MatKhau, Email, DiaChi, SoDienThoai)
		values (@hoten, @tendn, @pass,Lower( @email), @diachi, @sodt);
		set @makh = SCOPE_IDENTITY();
end
go

-- Test thủ tục: proc_KhachHang_Register
--declare @makh int 
--exec  proc_KhachHang_Register		
--@hoten = 'a',
--@tendn = '1fasd232',
--@pass = '12323',
--@diachi = '32 a',
--@email = 'kjda@gmail.com',
--@sodt = '09302',
--@makh = @makh output

select * from KhachHang

-------------------------------------------------------------------------------
--  proc_KhachHang_Authenticate: Kiểm tra đang nhập khách hàng
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_KhachHang_Authenticate'))
	drop procedure proc_KhachHang_Authenticate
go
create procedure proc_KhachHang_Authenticate
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from KhachHang as t where t.TenDangNhap = @tendn and t.MatKhau = @pass

end
go

-- Test thủ tục: proc_KhachHang_Authenticate
--exec  proc_KhachHang_Authenticate
--		@tendn = 'kh1',
--		@pass = 123
		


-------------------------------------------------------------------------------
--  proc_KhachHang_GetInfo: Lấy thông tin khách hàng dựa vào @customerId
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_KhachHang_GetInfo'))
	drop procedure proc_KhachHang_GetInfo
go
create procedure proc_KhachHang_GetInfo
@customerId int
as
begin
	set nocount on;
	select * from KhachHang where MaKhachHang = @customerId

end
go

 --Test thủ tục: proc_KhachHang_GetInfo
--exec  proc_KhachHang_GetInfo
--@customerId = 1
		


-------------------------------------------------------------------------------
--  proc_HoaDon_Create: Tạo mới một hoá đơn
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_HoaDon_Create'))
	drop procedure proc_HoaDon_Create
go
create procedure proc_HoaDon_Create
@maKh bigint,
@ngayMua date,
@damua bit,
@maHoaDon int output
as
begin
	set nocount on;
	if(not exists(select * from KhachHang as t where t.MaKhachHang = @maKh))
		begin
			set @maHoaDon = -1
			return;
		end
	insert into HoaDon (MaKhachHang, NgayDatMua, DaMua) values(@maKh, @ngayMua, @damua)
	set @maHoaDon = @@IDENTITY;
end
go

-- Test thủ tục: proc_HoaDon_Create
--declare @maHoaDon bigint;
--exec  proc_HoaDon_Create
--		@maKh = 52,
--		@ngayMua = '2021-02-02',
--		@daMua = 1,
--		@maHoaDon = @maHoaDon output

-------------------------------------------------------------------------------
--  proc_ChiTietHoaDon_Create: Tạo mới chi tiết hoá đơn
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_ChiTietHoaDon_Create'))
	drop procedure proc_ChiTietHoaDon_Create
go
create procedure proc_ChiTietHoaDon_Create
@maSach varchar(50),
@soLuong int,
@maHoaDon bigint,
@trangThai int,
@StatusCode int = 0 output
as
begin

	if(not exists(select * from HoaDon as t where t.MaHoaDon = @maHoaDon))
		begin
			set @StatusCode = 0
			return;
		end

	set nocount on;
	insert into ChiTietHoaDon(MaBanPhim, SoLuongMua, MaHoaDon, TrangThai)
	select @maSach, @soLuong, @maHoaDon, @trangThai
	if(@@ROWCOUNT > 0)
		set @StatusCode = 1
		return;
end
go

-- Test thủ tục: proc_ChiTietHoaDon_Create
--declare @StatusCode int;
--exec  proc_ChiTietHoaDon_Create
--		@maSach = 'b21',
--		@soLuong = 99,
--		@maHoaDon = 10048,
--		@trangthai = 0,
--		@StatusCode = @StatusCode output

--select @StatusCode



-------------------------------------------------------------------------------
--  proc_HoaDon_Order: Lấy thông tin đơn đặt hàng của @customerId
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_HoaDon_Order'))
	drop procedure proc_HoaDon_Order
go
create procedure proc_HoaDon_Order
@customerId int,
@quantity int
as
begin	
	set nocount on
	
	select h.MaHoaDon, h.NgayDatMua, count(c.SoLuongMua) as SoLuongMua, c.TrangThai
	from HoaDon as h INNER JOIN ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon 
		INNER JOIN BanPhim as s ON c.MaBanPhim = s.MaBanPhim
	where h.MaKhachHang = @customerId and c.MaHoaDon = h.MaHoaDon
	group by h.MaHoaDon, MaKhachHang, NgayDatMua, c.TrangThai
	ORDER BY h.NgayDatMua DESC OFFSET 0 ROWS FETCH NEXT @quantity ROWS ONLY

end
go

--Test thủ tục
exec proc_HoaDon_Order
@customerId = 1,
@quantity = 6






------------------------------------------------------------------------------------
--  proc_HoaDon_OrderHistory: Lấy thông tin chi tiết đơn đặt hàng của @orderId và @customerId
------------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_HoaDon_OrderDetail'))
	drop procedure proc_HoaDon_OrderDetail
go
create procedure proc_HoaDon_OrderDetail
@customerId int,
@orderId int
as
begin	
	set nocount on

	select c.MaHoaDon, s.TenBanPhim, Anh, GiaBan, c.SoLuongMua
	from ChiTietHoaDon as c join BanPhim as s on c.MaBanPhim = s.MaBanPhim join hoadon as h
		on h.MaHoaDon = c.MaHoaDon
	where h.MaKhachHang = @customerId and h.MaHoaDon = @orderId and c.MaHoaDon = @orderId

end
go

--Test thủ tục
--exec proc_HoaDon_OrderDetail
--@customerId = 21,
--@orderId = 10024



-------------------------------------------------------------------------------
--- proc_Order_Tranforms_Cus: Thực hiện chức năng chuyển đổi trạng thái đơn hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Tranforms_Cus')
	drop procedure proc_Order_Tranforms_Cus
go 
create procedure proc_Order_Tranforms_Cus
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int

	UPDATE hoadon
	SET damua = 1
	WHERE MaHoaDon = @maHoaDon and damua = 0

	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon


	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Tranforms_Cus
--	@maHoaDon = '47',
--	@trangThai = 1,
--	@Result = @Result output;

--select @Result


-----------------------------------------------------------------------------
--- proc_Order_Cancel_Cus: Thực hiện chức năng huỷ đặt hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Cancel_Cus')
	drop procedure proc_Order_Cancel_Cus
go 
create procedure proc_Order_Cancel_Cus
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Cancel_Cus
--	@maHoaDon = '47',
--	@Result = @Result output;

--select @Resul



------------------------------------------ADMIN--------------------------------------------------


-------------------------------------------------------------------------------
--  proc_HoaDon_OrderDetail: Lấy thông tin đơn đặt hàng chi tiết
-------------------------------------------------------------------------------

if exists(select * from sys.views  where name = 'view_OrderHisoty' and type='v')
	drop view view_OrderHisoty
go
CREATE VIEW view_OrderHisoty
as
select h.MaHoaDon, h.NgayDatMua, count(c.SoLuongMua) as SoLuongMua, c.TrangThai, h.MaKhachHang
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = c.MaHoaDon
	where c.MaHoaDon = h.MaHoaDon
	group by h.MaHoaDon, h.NgayDatMua, c.SoLuongMua, c.TrangThai, h.MaKhachHang
go

--Test khung nhìn
select * from view_OrderHisoty


-------------------------------------------------------------------------------
--  proc_Admin_Register: Đăng ký một tài khoản admin mới
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_Register'))
	drop procedure proc_Admin_Register
go
create procedure proc_Admin_Register
@tendn varchar(50),
@pass varchar(50),
@Result int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @Result = -3
				return;
			end
		if(exists(select * from DangNhap as t where t.TenDangNhap = @tendn))
			begin
				set @Result = -2
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @Result = -1
				return;
			end

		insert into DangNhap(TenDangNhap, MatKhau, Quyen)
		values (@tendn, @pass, 1);
		set @Result = 1;
end
go

-- Test thủ tục: proc_Admin_Register
--declare @Result int 
--exec  proc_Admin_Register		
--@tendn = 'admin',
--@pass = '12323',
--@Result = @Result output


-------------------------------------------------------------------------------
--  proc_Admin_Authenticate: Kiểm tra đang nhập Admin
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_Authenticate'))
	drop procedure proc_Admin_Authenticate
go
create procedure proc_Admin_Authenticate
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from DangNhap as t where t.TenDangNhap = @tendn and t.MatKhau = @pass and t.Quyen = 1

end
go

 --Test thủ tục: proc_Admin_Authenticate
--exec  proc_Admin_Authenticate
--@tendn = 'admin',
--@pass = 'admin'
		

-------------------------------------------------------------------------------
--- proc_Product_Insert: thực hiện chức năng thêm mới sản phẩm
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Product_Insert')
	drop procedure proc_Product_Insert
go
create procedure proc_Product_Insert
@MaBanPhim nvarchar(50),
@TenBanPhim nvarchar(50),
@MaLoai int, 
@MaSwitch int, 
@GiaBan bigint,
@Anh nvarchar(50),
@Soluong bigint,
@NgayNhap date,
@MoTa ntext,
@Result bit output

as
begin
	set nocount on
	
	if(@MaBanPhim is null or @MaBanPhim = N'' or @MaLoai is null or @TenBanPhim is null or @TenBanPhim = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from BanPhim where MaBanPhim  = @MaBanPhim)
		begin
			set @Result = -1
			return;
		end

	Insert into BanPhim(MaBanPhim, TenBanPhim, MaLoai, MaSwitch, GiaBan, Anh, SoLuong, NgayNhap, Mota)
			values(@MaBanPhim, @TenBanPhim, @MaLoai,@MaSwitch, @GiaBan, @Anh, @Soluong, @NgayNhap,  @moTa)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
go

-- Test thủ tục:
--declare @Result int;
--exec proc_Product_Insert
--@masach = 'b20',
--@tensach = N'Hạt Giống Tâm Hồn dành riêng cho phụ nữ',
--@soluong = 30,
--@gia = 22000,
--@maloai = N'Bi quyet cuoc song',
--@sotap = '2',
--@anh = 'image_sach/b20.jpg',
--@NgayNhap = '2021.11.15 10:04:06',
--@tacgia = 'Jack Canield-Mark Victor Hansen',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_Product_Update: thực hiện chức năng cập nhật thông tin sản phẩm
-------------------------------------------------------------------------------
if exists(select * from sys.objects where name = 'proc_Product_Update')
	drop procedure proc_Product_Update
go
create procedure proc_Product_Update
@MaBanPhim nvarchar(50),
@TenBanPhim nvarchar(250),
@MaLoai int, 
@MaSwitch int, 
@GiaBan money,
@Anh nvarchar(50),
@Soluong bigint,
@NgayNhap date,
@MoTa ntext,
@Result bit output

as
begin
	set nocount on
	
	if(@MaBanPhim is null or @MaBanPhim = N'' or @MaLoai is null or @TenBanPhim is null or @TenBanPhim = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from BanPhim where MaBanPhim  <> @MaBanPhim)
		begin
			set @Result = 0
			return;
		end

	Update BanPhim
	set 
		TenBanPhim = @TenBanPhim,
		MaLoai = @maloai,
		MaSwitch = @MaSwitch,
		SoLuong = @Soluong,
		GiaBan = @GiaBan,
		Anh = @Anh,
		NgayNhap = @NgayNhap,
		MoTa = @MoTa
	where MaBanPhim = @MaBanPhim

	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
go


-- Test thủ tục:
--declare @Result bit;
--exec proc_Product_Update
--@MaBanPhim  = '',
--@TenBanPhim = '',
--@MaLoai  = '',
--@MaSwitch = '',
--@GiaBan  = '',
--@Anh = '',
--@Soluong = 2,
--@NgayNhap  = '',
--@MoTa  = '',
--@Result = @Result output

--select @Result

-------------------------------------------------------------------------------
--- proc_Product_Delete: thực hiện chức năng xoá sản phẩm
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Product_Delete')
	drop procedure proc_Product_Delete
go 
create procedure proc_Product_Delete
@maSach nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete BanPhim 
	where(MaBanPhim = @maSach) or (exists(select * from loai where MaBanPhim = @maSach ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go
--Test thủ tục
--declare @Result bit;
--exec proc_Product_Delete
--	@maSach = '2',
--	@Result = @Result output;

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfProducts_Insert: thực hiện chức năng thêm mới loại sản phẩm
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfProducts_Insert')
	drop procedure proc_TypesOfProducts_Insert
go
create procedure proc_TypesOfProducts_Insert
@maLoai int,
@tenLoai nvarchar(250),
@Result int output

as
begin
	set nocount on
	
	if exists(select * from loai where maloai  = @maLoai)
		begin
			set @Result = -1
			return;
		end

	Insert into loai (maloai, tenloai) values(@maLoai, @tenLoai)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
go

-- Test thủ tục:
--declare @Result int;
--exec proc_TypesOfProducts_Insert
--@maLoai = 'test',
--@tenLoai = N'Test',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfProducts_Update: thực hiện chức năng cập nhật thông tin loại sản phẩm
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfProducts_Update')
	drop procedure proc_TypesOfProducts_Update
go
create procedure proc_TypesOfProducts_Update
@maLoaiNew int,
@maLoaiOld int,
@tenLoai nvarchar(250),
@Result bit output

as
begin
	set nocount on
	
	if not exists(select * from BanPhim where MaLoai  <> @maLoaiOld)
		begin
			set @Result = 0
			return;
		end

	Update loai
	set maloai = @maLoaiNew,
		tenloai = @tenLoai
	where maloai = @maLoaiOld


	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
go

-- Test thủ tục:
--declare @Result bit;
--exec proc_TypesOfProducts_Update
--@maLoaiNew = 'Bi quyet cuoc song2',
--@maLoaiOld = 'Bi quyet cuoc song',
--@tenLoai = N'Bí quyết cuộc sống',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfProducts_Delete: thực hiện chức năng xoá loại sản phẩm
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfProducts_Delete')
	drop procedure proc_TypesOfProducts_Delete
go 
create procedure proc_TypesOfProducts_Delete
@maLoai int,
@Result bit output
as
begin
	set nocount on
	
	Delete loai 
	where(maloai = @maLoai) or (exists(select * from BanPhim where MaLoai = @maLoai ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_TypesOfProducts_Delete
--	@maLoai = 'test',
--	@Result = @Result output;

--select @Result


-------------------------------------------------------------------------------
--  proc_Admin_GetOrderHistory: Lấy tất cả lịch sử đặt hàng
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_GetAllOrderHistory'))
	drop procedure proc_Admin_GetAllOrderHistory
go
create procedure proc_Admin_GetAllOrderHistory
as
begin	
	set nocount on

	select s.Anh, TenBanPhim, c.SoLuongMua, h.NgayDatMua, c.TrangThai, c.MaHoaDon, c.MaBanPhim
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = h.MaHoaDon 
		join BanPhim as s on s.MaBanPhim = c.MaBanPhim
	where h.MaHoaDon = c.MaHoaDon
	order by h.NgayDatMua desc
	
end
go

--Test thủ tục
--exec proc_Admin_GetAllOrderHistory



-------------------------------------------------------------------------------
--- proc_Order_Delete: thực hiện chức năng xoá hoá đơn
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Delete')
	drop procedure proc_Order_Delete
go 
create procedure proc_Order_Delete
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go
--Test thủ tục
--declare @Result bit;
--exec proc_Order_Delete
--	@maHoaDon = '46',
--	@Result = @Result output;

--select @Result



-------------------------------------------------------------------------------
--- proc_Order_Tranforms: Thực hiện chức năng chuyển đổi trạng thái đơn hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Transforms')
	drop procedure proc_Order_Transforms
go 
create procedure proc_Order_Transforms
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int
	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Transforms
--	@maHoaDon = '38',
--	@trangThai = 0,
--	@Result = @Result output;

--select @Result


